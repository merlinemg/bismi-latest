<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Super Admin
        $super_admin = new Role();
        $super_admin->name = 'super_administrator';
        $super_admin->display_name = 'Super Administrator';
        $super_admin->description = 'Super Administrator';
        $super_admin->save();

        $super_admin_permission = Permission::where('name','super_permission')->get();
        $super_admin->permissions()->attach($super_admin_permission);

        // Sub Admin
        $admin = new Role();
        $admin->name = 'administrator';
        $admin->display_name = 'Administrator';
        $admin->description = 'Administrator';
        $admin->save();

        $permissionsArr = array('create_user', 'edit_user', 'delete_user', 'view_user', 'list_user');

        foreach ($permissionsArr as $key => $single_permission) {
        	$new_permissions = Permission::where('name', $single_permission)->get();
        	$admin->permissions()->attach($new_permissions);
        }
    }
}
