<?php

use Illuminate\Database\Seeder;
use App\Models\Designation;

class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designations = [

        	[
        	    'name' => 'Salesman',
        	],
        	[
        	    'name' => 'Sales Trainee',
        	],

            [
                'name' => 'Manager',
            ],

        ];

        foreach ($designations as $key => $value) {
            Designation::create($value);
        }
    }
}
