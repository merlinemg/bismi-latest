<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

        	[
        	    'name' => 'super_permission',
        	    'display_name' => 'Super Permission',
        	    'description' => 'Privilege to manage all records'
        	],
        	[
        	    'name' => 'role_management',
        	    'display_name' => 'Role Management',
        	    'description' => 'Privilege to create, edit, delete view and list User Roles'
        	],

            [
                'name' => 'create_user',
                'display_name' => 'Create User',
                'description' => 'Privilege to create a new user record'
            ],
            [
                'name' => 'edit_user',
                'display_name' => 'Edit User',
                'description' => 'Privilege to edit a user record'
            ],
            [
                'name' => 'delete_user',
                'display_name' => 'Delete User',
                'description' => 'Privilege to delete a user record'
            ],
            [
                'name' => 'view_user',
                'display_name' => 'View User',
                'description' => 'Privilege to view a user record'
            ],
            [
                'name' => 'list_user',
                'display_name' => 'List Users',
                'description' => 'Privilege to list all user records'
            ]

        ];

        foreach ($permissions as $key => $value) {
            Permission::create($value);
        }
    }
}
