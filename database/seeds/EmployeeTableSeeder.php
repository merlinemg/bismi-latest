<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Outlet;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = new Employee();
        $employee->name = 'Employee Demo';
        $employee->emp_id = 'BIS001';
        $employee->email = 'emp@demo.com';
        $employee->password = bcrypt('123456');
        $employee->phone_number = '9219592195';
        $employee->gender = 'male';
        $employee->dob = '2010-01-02';
        $employee->designation_id = 1;  //Dummy
        $employee->save();

        $outletsArr = array('Supreme Market', 'Pothys');

        foreach ($outletsArr as $key => $single_outlet) {
            $new_outlet = Outlet::where('name', $single_outlet)->get();
            $employee->outlets()->attach($new_outlet);
        }
    }
}
