<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_user = new User();
        $super_user->name = 'Super Admin Demo';
        $super_user->email = 'admin@demo.com';
        $super_user->password = bcrypt('123456');
        $super_user->save();

        $super_user_role = Role::where('name','super_administrator')->get();
        $super_user->roles()->attach($super_user_role);
    }
}
