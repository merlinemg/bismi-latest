<?php

use Illuminate\Database\Seeder;
use App\Models\Outlet;

class OutletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $outlets = [
        	[
        	    'name' => 'Supreme Market',
        	],

        	[
        	    'name' => 'Reliance More',
        	],

        	[
        	    'name' => 'Big Baazar',
        	],

        	[
        	    'name' => 'Pothys',
        	],

        ];

        foreach ($outlets as $key => $value) {
            Outlet::create($value);
        }
    }
}
