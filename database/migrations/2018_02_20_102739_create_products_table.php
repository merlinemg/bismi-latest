<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->string('thumbnail_image',225)->nullable();
            $table->text('description')->nullable();
            $table->string('product_price',225)->nullable();
            $table->string('product_code',225)->nullable();
            $table->string('barcode',225)->nullable();
            $table->string('department',225)->nullable();
            $table->string('package',225)->nullable();
            $table->string('brand',225)->nullable();
            $table->string('shelf_life',225)->nullable();
            $table->string('country_of_orgin',225)->nullable();
            $table->string('selling_price',100)->nullable();
            $table->string('altered_price',100)->nullable();
            $table->string('overhide',225)->nullable();
            $table->integer('category_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
