<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id',225);
            $table->string('name',225);
            $table->string('emailid',225)->nullable();
            $table->string('phone',50)->nullable();
            $table->text('address')->nullable();
            $table->string('customer_type')->nullable();
            $table->integer('customer_outlet_id')->nullable();
            $table->string('customer_currency')->nullable();
            $table->string('customer_vat_trn')->nullable();
            $table->integer('status',11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
