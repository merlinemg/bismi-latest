<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id',100);
            $table->string('order_unique_code',225);
            $table->integer('customer_id');
            $table->integer('salesman_id');
            $table->integer('outlet_id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->string('price',150);
            $table->string('commision_price',150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
