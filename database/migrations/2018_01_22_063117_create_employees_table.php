<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('emp_id');
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('phone_number');
            $table->string('gender', 10);
            $table->date('dob')->nullable();
            $table->integer('designation_id');
            $table->string('image', 100)->nullable();
            $table->timestamps();
        });


        // Create table for associating roles to employees (Many-to-Many)
        Schema::create('employee_outlet', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned();
            $table->integer('outlet_id')->unsigned();

            $table->foreign('employee_id')->references('id')->on('employees')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('outlet_id')->references('id')->on('outlets')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['employee_id', 'outlet_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_outlet');
        Schema::dropIfExists('employees');
    }
}
