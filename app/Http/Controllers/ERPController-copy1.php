<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Designation;
use App\Models\Outlet;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Product_image;
use App\Models\Category;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\Department;
use App\Models\PaymentMode;
use App\Models\ProductUnit;
use App\Models\ProductPrice;
use App\Models\ProductBarcode;

use DB;
use Mail;

class ERPController extends Controller
{
    protected $api;  
    public function __construct(){
        $this->api = new \GuzzleHttp\Client(['http_errors' => false]);
        $this->factapikey = "koocebqa04b27ar301l4";

       // $client = new \GuzzleHttp\Client(['http_errors' => false]);
    }

    //login to erp to get token value
    public function erpLogin(){

        $url = "http://bismiho.dyndns.org:4000/api/authenticate";
      
        $request =  $this->api->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => $this->factapikey,
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'user_loginId' => 'admin',
                'user_email' => 'admin',
                'deviceID' => 'DEV43',
                'cur_location' => '25.2350393,55.2762502'
            ]
        ]);

        $response = $request->getBody();
        $result = json_decode($response);

        return $result->token;

    }

    //get all outlets

    public function getAllOutlets(){

        $login_token = $this->erpLogin();
        $url = "http://bismiho.dyndns.org:4000/api/getAllOutletlist";
      
        $request =  $this->api->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => $this->factapikey,
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'authentication_token' => $login_token
            ]
        ]);

        $response = $request->getBody();

        $json_decode = json_decode($response,true);
       
        foreach($json_decode['data'] as $key=>$row){

            $outlet_check = Outlet::where('name',$row['name'])->where('outlet_code',$row['id'])->count();

            if($outlet_check>0){
                $outlet = Outlet::where('name',$row['name'])->first(); 
            }
            else{
                $outlet = new Outlet();
            }

            $outlet->outlet_code = $row['id'];
            $outlet->name = $row['name'];
            $outlet->location = $row['location'];
            $outlet->otherDetails = $row['otherDetails'];

            $outlet->save();
         
        }
        return response()->json(["status" => "success", 'message' => 'Outlet details saved']);


    }

    //get all payment mode

    public function getAllPayment(){

        $login_token = $this->erpLogin();
        $url = "http://bismiho.dyndns.org:4000/api/getMasters";
      
        $request =  $this->api->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => $this->factapikey,
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'authentication_token' => $login_token
            ]
        ]);

        $response = $request->getBody();

        $json_decode = json_decode($response,true);
       
        foreach($json_decode['paymodes'] as $key=>$row){

            $payment_check = PaymentMode::where('payment_name',$row['Name'])->count();

            if($payment_check>0){
                $payment = PaymentMode::where('payment_name',$row['Name'])->first(); 
            }
            else{
                $payment = new PaymentMode();
            }

            $payment->payment_code = $row['Code'];
            $payment->payment_name = $row['Name'];

            $payment->save();
         
        }
        return response()->json(["status" => "success", 'message' => 'Payment mode details saved']);


    }

     //get all departments

    public function getAllDepartments(){

        $login_token = $this->erpLogin();
        $url = "http://bismiho.dyndns.org:4000/api/getAllDepartments";
      
        $request =  $this->api->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => $this->factapikey,
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'authentication_token' => $login_token
            ]
        ]);

        $response = $request->getBody();

        $json_decode = json_decode($response,true);
       
        foreach($json_decode['data'] as $key=>$row){

            $department_check = Department::where('department_name',$row['department_name'])->count();

            if($department_check>0){
                $department = Department::where('department_name',$row['department_name'])->first(); 
            }
            else{
                $department = new Department();
            }

            $department->department_id = $row['department_id'];
            $department->department_name = $row['department_name'];
            $department->save();
         
        }
        return response()->json(["status" => "success", 'message' => 'Department details saved']);


    }

    //get all salesman list and save to db

    public function getAllEmployee(){

        $login_token = $this->erpLogin();
        $url = "http://bismiho.dyndns.org:4000/api/getAllSalesman";
      
        $request =  $this->api->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => $this->factapikey,
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'authentication_token' => $login_token
            ]
        ]);

        $response = $request->getBody();

        $json_decode = json_decode($response,true);

        $count = count($json_decode['data']);

        foreach($json_decode['data'] as $key=>$row){
           
            $employee_check = Employee::where('emp_id',$row['salesman_id'])->count();//check emp already exist

            if($employee_check>0){
                $employee = Employee::where('emp_id',$row['salesman_id'])->first(); //update existing emp
            }
            else{

                $password = bcrypt(rand());
                $employee = new Employee(); //add new emp
                $employee->password = $password;
            }

                
                $employee->name = $row['salesman_name'];
                $employee->emp_id = $row['salesman_id'];
                //$employee->email = $row['email'];
                if( isset( $row['phone_number'] ) ){
                    $employee->phone_number = $row['phone_number'];
                }
                $employee->gender = $row['gender'];
                if( isset( $row['DOB'] ) ){
                
                    $dob_date = str_replace('/', '-', $row['DOB']);
                    $dob = date('Y-m-d', strtotime($dob_date));
                    $employee->dob = $dob;

                }
                
                //$employee->designation_id = $designationId;
                $employee->save();

                if( isset( $row['outlets'] ) ){

                    $outletArr= array();
                    foreach($row['outlets'] as $key1=>$row_outlets){

                        $outlet_count = Outlet::where('name',$row_outlets['name'])->count();
                        //get outlet count based on name

                        if($outlet_count>0){
                            //get id based on outlet name
                            $outlet = Outlet::where('name',$row_outlets['name'])->first();
                            $outletId = $outlet->id;
                        }
                        else{
                            //add new outlets
                           $outlet = new Outlet(); 
                           $outlet->outlet_code = $row_outlets['id'];
                           $outlet->name = $row_outlets['name'];
                           $outlet->location = $row_outlets['location'];
                           $outlet->save();
                           $outletId = $outlet->id;
                        }

                        $outlet = Outlet::where('name',$row_outlets['name'])->first();
                        //to use outlet sync
                        array_push($outletArr, $outletId);
                        //save new outlets
                        if($employee_check==0){
                           $employee->outlets()->attach($outletId);
                        }
                        
                    }

                    //to update new outlets to employee
                    if($employee_check>0){
                        $employee->outlets()->sync($outletArr);
                    }

                }

                //send email for first time saved in db
                // if($employee_check==0){

                //     $emailData = array(
                //     'to'        => $row['email'],
                //     'from'      => 'bismi@gmail.com',
                //     'subject'   => 'Bismi - Employee Login Credentials',
                //     'view'      => 'auth.employeeEmail'
                //      );

                //     $data = [
                //        'employee_code' => $row['employee_id'],
                //        'username' => $row['email'],
                //        'password' => $password
                //     ];

                //     Mail::send($emailData['view'], ["details"=>$data], function ($message) use ($emailData) {
                //         $message
                //             ->to($emailData['to'])
                //             ->from($emailData['from'])
                //             ->subject($emailData['subject']);
                //     });
                // }
        }

        return response()->json(["status" => "success", 'message' => 'Salesman Details Saved']);

    }

    //get customer list from erp and saved to db

     public function getAllCustomer(){

        $login_token = $this->erpLogin();
        $url = "http://bismiho.dyndns.org:4000/api/getAllCustomers";

        $outlet = Outlet::all();

        foreach($outlet as $key3=>$rowOutlet){
      
            $request =  $this->api->post(
                 $url,[
                'headers' => ['FACTS_API_KEY' => $this->factapikey,
                'Content-Type' => 'application/x-www-form-urlencoded'],
                'form_params' => [
                    'authentication_token' => $login_token,
                    'outlet_id'=>$rowOutlet['outlet_code']
                ]
            ]);

            $response = $request->getBody();

            $json_decode = json_decode($response,true);
           
            foreach($json_decode['data'] as $key=>$row){

                $customer_check = Customer::where('customer_id',$row['customer_id'])->count();

                if($customer_check>0){
                    $customer = Customer::where('customer_id',$row['customer_id'])->first(); 
                }
                else{
                    $customer = new Customer();
                    $customer->flag = 'E';
                }

                $customer->customer_id = $row['customer_id'];
                $customer->name = $row['customer_name'];
                $customer->emailid = $row['customer_email'];
                $customer->phone = $row['customer_phone'];
                $customer->customer_type = $row['customer_type'];

                if( isset( $row['customer_outlet_id'] ) ){
                    $outlet = Outlet::where('outlet_code',$row['customer_outlet_id'])->first();
                    $customer->outlet_id = $outlet['id'];
                }

                $customer->customer_currency = $row['customer_currency'];
                $customer->save();
         
            }
        }
        return response()->json(["status" => "success", 'message' => 'Customer details saved']);
    }
   

    public function getAllProduct(){

        $url = "http://bismiho.dyndns.org:4000/api/getProducts";

        $department = Department::orderBy(DB::raw('RAND()'))->get();
        
        $login_token = $this->erpLogin();
        
        foreach($department as $rowDepart){

            // $rowDepart['department_id']

            //   dd($rowDepart['department_id']);
      
            $request =  $this->api->post(
                     $url,[
                    'headers' => ['FACTS_API_KEY' => $this->factapikey,
                    'Content-Type' => 'application/x-www-form-urlencoded'],
                    'form_params' => [
                        'department_code' => $rowDepart['department_id'],
                        'product_code' => '',
                        'category_code' => '',
                        'subcategory_code' => '',
                        'outlet_code' =>0,
                        'authentication_token' => $login_token
                    ]
                ]);

            $response = $request->getBody();

            $json_decode = json_decode($response,true);

           
            foreach($json_decode['data'] as $key=>$row){

                $sales_category = Category::orderBy('id','ASC')->get();
                foreach($sales_category as $rowSalesCat){

                    $product_check = Product::where('product_code',$row['product_code'])->where('sales_category_id',$rowSalesCat->id)->count();

                    if($product_check>0){
                        $product = Product::where('product_code',$row['product_code'])->where('sales_category_id',$rowSalesCat->id)->first();

                        $product_image = Product_image::where('product_id',$product->id)->get();
                        if($product_image){

                             DB::table('product_images')->whereIn('id', $product_image)->delete();
                        }

                      
                        // $product_barcode = ProductBarcode::select('id')->where('product_id',$product->id)->get();
                        // if($product_barcode){

                        //     DB::table('product_barcodes')->whereIn('id', $product_barcode)->delete();
                           
                        // }
                    }
                    else{
                        $product = new Product();
                        
                    }
                
                    $category = Product_category::where('category_name',$row['category_name'])->where('category_code',$row['category_code'])->first();
                    $categoryId = $category->id;

                    $subcategory = Product_subcategory::where('category_id',$categoryId)->where('subcategory_name',$row['subcategory_name'])->where('subcategory_code',$row['subcategory_code'])->first();
                    $subcategoryId = $subcategory->id;

                    $product->product_name = $row['product_name'];
                    $product->sales_category_id = $rowSalesCat->id;
                    $product->category_id = $categoryId;
                    $product->subcategory_id = $subcategoryId;
                    if( isset( $row['thumbnail_image'] ) ){
                        $product->thumbnail_image = $row['thumbnail_image'];
                    }
                    $product->description = $row['description'];
                   
                    $product->product_code = $row['product_code'];
                    
                    if( isset( $row['package'] ) ){
                        $product->package = $row['package'];
                    }

                    if( isset( $row['brand'] ) ){
                        $product->brand = $row['brand'];
                    }

                    if( isset( $row['shelf_life'] ) ){
                        $product->shelf_life = $row['shelf_life'];
                    }

                    if( isset( $row['country_of_origin'] ) ){
                        $product->country_of_orgin = $row['country_of_origin'];
                    }

                    $departmentId = Department::where('department_id',$row['department_code'])->first();
                    $product->department_id = $departmentId['id'];

                    $product->base_unit = $row['base_unit'];

                    if( isset( $row['overhide'] ) ){
                        $product->overhide = $row['overhide'];
                    }

                    $product->available_quantity = $row['available_qty'];
                    $product->save();

                    $productid = $product->id;

                    if( isset( $row['images'] ) ){

                        foreach($row['images'] as $key1=>$row_images){
                        
                             $product_image = new Product_image();
                             $product_image->product_id = $productid;
                             $product_image->image = $row_images['image_name'];
                             $product_image->save();
                        }
                    }

                    //insert /update  barcode code start

                    if( isset( $row['barcodes'] ) ){

                        foreach($row['barcodes'] as $key2=>$row_barcodes){

                            $product_barcode_check = ProductBarcode::select('id')->where('product_id',$product->id)->where('barcode_unit',$row_barcodes['barcode_unit'])->get();


                            if(count($product_barcode_check)>0){
                        
                                 $product_barcode = ProductBarcode::where('product_id',$product->id)->where('barcode_unit',$row_barcodes['barcode_unit'])->first();

                                 
                             }
                             else{
                                    $product_barcode = new ProductBarcode();
                                    $product_barcode->product_id = $product->id;
                             }
                        
                             $product_barcode->barcode_unit = $row_barcodes['barcode_unit'];
                             $product_barcode->barcode_name = $row_barcodes['barcode_name'];
                             $product_barcode->barcode = $row_barcodes['barcode'];
                             $product_barcode->save();
                        }
                    }

                    //insert /update  barcode code end

                    //insert /update  price code start

                    if( isset( $row['prices'] ) ){

                        foreach($row['prices'] as $key3=>$row_prices){

                            $product_prices_check = ProductPrice::select('id')->where('product_id',$product->id)->where('pricetype_unit',$row_prices['pricetype_unit'])->get();


                            if(count($product_prices_check)>0){
                        
                                 $product_prices = ProductPrice::where('product_id',$product->id)->where('pricetype_unit',$row_prices['pricetype_unit'])->first();

                                 if($product_prices->status==0){
                                    $product_prices->selling_price = ($row_prices['pricetype_msp']) * (1 +($rowSalesCat->fixed_percentage/100));
                                 }

                                 
                             }
                             else{
                                    $product_prices = new ProductPrice();
                                    $product_prices->product_id = $product->id;
                                    $product_prices->selling_price = ($row_prices['pricetype_msp']) * (1 +($rowSalesCat->fixed_percentage/100));
                             }
                        
                             $product_prices->pricetype_code = $row_prices['pricetype_code'];
                             $product_prices->pricetype_name = $row_prices['pricetype_name'];
                             $product_prices->pricetype_unit = $row_prices['pricetype_unit'];
                             $product_prices->pricetype_msp = $row_prices['pricetype_msp'];
                             

                             $product_prices->save();
                        }
                    }

                    //insert /update  price code end


                    //insert /update unit price code start

                    if( isset( $row['units'] ) ){

                         

                        foreach($row['units'] as $key4=>$row_units){

                            $product_unit = ProductUnit::select('id')->where('product_id',$product->id)->where('conversion_unit',$row_units['conversion_unit'])->get();

                            if(count($product_unit)>0){
                        
                                 $product_units = ProductUnit::where('product_id',$product->id)->where('conversion_unit',$row_units['conversion_unit'])->first();

                                 
                             }
                             else{
                                    $product_units = new ProductUnit();
                                    $product_units->product_id = $product->id;
                             }

                                 $product_units->base_unit = $row_units['base_unit'];
                                 $product_units->base_qty = $row_units['base_qty'];
                                 $product_units->conversion_unit = $row_units['conversion_unit'];
                                 $product_units->conversion_qty = $row_units['conversion_qty'];
                                 if( isset( $row_units['conversion_string'] ) ){
                                    $product_units->conversion_string = $row_units['conversion_string'];
                                 }
                                 if( isset( $row_units['packing_string'] ) ){
                                    $product_units->packing_string = $row_units['packing_string'];
                                 }


                                 $product_units->available_quantity = $row_units['unit_available_qty'];
                                 $product_units->save();
                        }
                    }

                    //insert /update unit price code end



                    // if($key=='2'){

                    //     dd("insert");
                    //      exit();  
                    //  }
                }
                
            }
           
                   
        }

            return response()->json(["status" => "success", 'message' => 'Product saved']);
        
    }

    //save category and subcategory from erp to db

    public function getAllCategory(){
       
        
        $url = "http://bismiho.dyndns.org:4000/api/getProducts";

        $department = Department::all();
        $login_token = $this->erpLogin();
        
        foreach($department as $rowDepart){
            
            $request =  $this->api->post(
                 $url,[
                'headers' => ['FACTS_API_KEY' => $this->factapikey,
                'Content-Type' => 'application/x-www-form-urlencoded'],
                'form_params' => [
                    'department_code' => $rowDepart['department_id'],
                    'product_code' => '',
                    'category_code' => '',
                    'subcategory_code' => '',
                    'outlet_code' =>0,
                    'authentication_token' => $login_token
                ]
            ]);

            $response = $request->getBody();

            $json_decode = json_decode($response,true);

            foreach($json_decode['data'] as $key=>$row){

                //saving category

                 if( isset( $row['category_code'] ) ){

                    $category_check = Product_category::where('category_name',$row['category_name'])->where('category_code',$row['category_code'])->count();

                    if($category_check==0){
                        $category = new Product_category();
                        $category->department_id = $rowDepart['id'];
                        $category->category_name = $row['category_name'];
                        $category->category_code = $row['category_code'];
                        $category->save();
                    }

                    $category_id = Product_category::where('category_name',$row['category_name'])->where('category_code',$row['category_code'])->first();
                    $categoryId = $category_id->id;

                   // saving subcategory

                    if( isset( $row['subcategory_code'] ) ){

                        $subcategory_check = Product_subcategory::where('subcategory_code',$row['subcategory_code'])->where('subcategory_name',$row['subcategory_name'])->where('category_id',$categoryId)->count();

                        if($subcategory_check==0){
                            $subcategory = new Product_subcategory();
                            $subcategory->category_id = $categoryId;
                            $subcategory->subcategory_code = $row['subcategory_code'];
                            $subcategory->subcategory_name = $row['subcategory_name'];
                            $subcategory->save();

                        }   
                    }  
               
                }

            // echo   $row['subcategory_name']."\n";
            }
           
        }
         return response()->json(["status" => "success", 'message' => 'Categories saved']);
    }

    // ************************************ getAllErpData *********************************************

    public function getAllErpData(){

       $x1 = $this->getAllDepartments(); 
       $x2 =$this->getAllCategory(); 
       $x3 = $this->getAllOutlets(); 
       $x4 =$this->getAllCustomer(); 
       $x5 = $this->getAllEmployee(); 
       $x6 = $this->getAllPayment(); 
       $x7 = $this->getAllProduct(); 


       $emailData = array(
                'to'        =>'reeja@spericorn.com',
                'from'      => 'bismi@gmail.com',
                'subject'   => 'Bismi - Cron Completed',
                'view'      => 'auth.cronMail'
                 );

                $data = array('Department'=>$x1->status(),'Category'=>$x2->status(),'Outlet'=>$x3->status(),'Customer'=>$x4->status(),'Employee'=>$x5->status(),'Payment'=>$x6->status(),'Product'=>$x7);

                Mail::send($emailData['view'], ["details"=>$data], function ($message) use ($emailData) {
                    $message
                        ->to($emailData['to'])
                        ->from($emailData['from'])
                        ->subject($emailData['subject']);
                });

    }

}
