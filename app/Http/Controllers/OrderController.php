<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\Outlet;
use DB;


class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(['permission:*_order|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        //$order = Order::groupBy('order_unique_code')->orderBy('id','DESC')->get();

        $order =  \DB::table('orders')
                    ->select('orders.*','customers.id as custid','customers.name as customer_name','employees.id as salesid','employees.name as employee_name','outlets.id as outletid','outlets.name as outlet_name','categories.id as catid','categories.name as cat_name')
                    ->join('customers','orders.customer_id','=','customers.id')
                    ->join('employees','orders.salesman_id','=','employees.id')
                    ->join('outlets','orders.outlet_id','=','outlets.id')
                    ->join('categories','orders.sales_category_id','=','categories.id')
                    ->orderBy('orders.id','DESC')
                    ->get();

        $customer = Customer::get();
        $employee = Employee::get();
        $outlet = Outlet::get();

        $customer_id = '';
        $salesman_id = '';
        $outlet_id = '';
        $from_date = '';
        $to_date = '';
        return view('order.list',compact('order','customer','employee','outlet','customer_id','salesman_id','outlet_id','from_date','to_date'));
    }


    public function orderDetails($order_unique_code){
         
         //$order = Order::find($id);

         $order =  \DB::table('orders')
                    ->select('orders.*','customers.id as custid','customers.name as customer_name','employees.id as salesid','employees.name as employee_name','outlets.id as outletid','outlets.name as outlet_name','categories.id as catid','categories.name as cat_name')
                    ->join('customers','orders.customer_id','=','customers.id')
                    ->join('employees','orders.salesman_id','=','employees.id')
                    ->join('outlets','orders.outlet_id','=','outlets.id')
                    ->join('categories','orders.sales_category_id','=','categories.id')
                    ->where('orders.order_unique_code',$order_unique_code)
                    ->first();

                   // dd($order);

        $product =\DB::table('order_products')
                    ->select('order_products.*','products.id as productid','products.product_name as product_name')
                    ->join('products','order_products.product_id','=','products.id')
                    ->where('order_products.order_id',$order->id)
                    ->get();

        
         return view('order.order-details',compact('order','product'));
    }

    /*Export Data*/
    public function export(Request $request){  

            
        $format = 'application/csv';
        $filename = 'order.csv';

        $customer_id = $request->input('customer_id');
        $salesman_id = $request->input('salesman_id');
        $outlet_id = $request->input('outlet_id');
        $from_date = $request->input('from_date_order');
        $to_date = $request->input('to_date_order');

        //dd($customer_id);
       
        
        $order =  \DB::table('orders')
                    ->select('orders.*','orders.order_id as fact_order_id','customers.id as custid','customers.name as customer_name','employees.id as salesid','employees.name as employee_name','outlets.id as outletid','outlets.name as outlet_name','categories.id as catid','categories.name as cat_name','order_products.*','order_products.order_id as order_product_id','products.id as productid','products.product_name as product_name')
                    ->join('customers','orders.customer_id','=','customers.id')
                    ->join('employees','orders.salesman_id','=','employees.id')
                    ->join('outlets','orders.outlet_id','=','outlets.id')
                    ->join('categories','orders.sales_category_id','=','categories.id')
                    ->join('order_products','orders.id','=','order_products.order_id')
                    ->join('products','order_products.product_id','=','products.id')
                     ->where(function($query) use ($customer_id, $salesman_id, $outlet_id, $from_date, $to_date)
                    {
                        if($customer_id){

                         $query->where('orders.customer_id', '=',$customer_id);
                        }

                        if($salesman_id){
                            $query->where('orders.salesman_id', '=',$salesman_id);
                        }

                        if($outlet_id){
                            $query->where('orders.outlet_id', '=',$outlet_id);
                        }

                        if($from_date && $to_date==''){
                            $query->where('orders.created_at', '>',$from_date);
                        }

                        if($from_date && $to_date){

                            //$required_date = DB::raw('CAST(orders.created_at AS order_date)');

                            $query->whereBetween('orders.created_at' , array($from_date,$to_date));
                        }


                    })
                    ->get();
        $tot_record_found=0;
        if(count($order)>0){

            $tot_record_found=1;
                    
            $export_data="SlNo,Fact Order ID,Spericorn Order ID,Customer,Salesman,Outlet,Sales Category,Product Name,Package,Quantity,Rate,Total Price,Payment Mode,Discount Amount,Vat Amount,Order Total Amount,Order date\n";
            foreach($order as $key=>$value){
                $slno = $key+1;
                $export_data.=$slno.','.$value->fact_order_id.','.$value->order_unique_code.','.$value->customer_name.','.$value->employee_name.','.$value->outlet_name.','.$value->cat_name.','.$value->product_name.','.$value->package.','.$value->quantity.','.$value->rate.','.$value->total_amount.','.$value->payment_mode.','.$value->discount_amount.','.$value->vat_amount.','.$value->order_total_amount.','.date('d-M-Y', strtotime($value->created_at))."\n";
            }

            return response($export_data)
            ->header('Content-Type',$format)               
            ->header('Content-Disposition', 'attachment; filename="'.$filename)
            ->header('Pragma','no-cache')
            ->header('Expires','0');                     
        }
         print_r(json_encode(array('status' => 'success', 'msg' => 'Download Succesfully')));
       // return view('download',['record_found' =>$tot_record_found]);    
    }

    public function sort(Request $request){
        $customer_id = $request->input('customer');
        $salesman_id = $request->input('salesman');
        $outlet_id = $request->input('outlet');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $customer = Customer::get();
        $employee = Employee::get();
        $outlet = Outlet::get();

        $order =  \DB::table('orders')
                    ->select('orders.*','customers.id as custid','customers.name as customer_name','employees.id as salesid','employees.name as employee_name','outlets.id as outletid','outlets.name as outlet_name','categories.id as catid','categories.name as cat_name')
                    ->join('customers','orders.customer_id','=','customers.id')
                    ->join('employees','orders.salesman_id','=','employees.id')
                    ->join('outlets','orders.outlet_id','=','outlets.id')
                    ->join('categories','orders.sales_category_id','=','categories.id')
                    ->where(function($query) use ($customer_id, $salesman_id, $outlet_id, $from_date, $to_date)
                    {
                        if($customer_id){

                         $query->where('orders.customer_id', '=',$customer_id);
                        }

                        if($salesman_id){
                            $query->where('orders.salesman_id', '=',$salesman_id);
                        }

                        if($outlet_id){
                            $query->where('orders.outlet_id', '=',$outlet_id);
                        }

                        if($from_date && $to_date==''){
                            $query->where('orders.created_at', '>',$from_date);
                        }

                        if($from_date && $to_date){

                            //$required_date = DB::raw('CAST(orders.created_at AS order_date)');

                            $query->whereBetween('orders.created_at' , array($from_date,$to_date));
                        }


                    })
                    ->get();

                   // dd($order);


         return view('order.list',compact('order','customer','employee','outlet','customer_id','salesman_id','outlet_id','from_date','to_date'));
    }
     

    
}
