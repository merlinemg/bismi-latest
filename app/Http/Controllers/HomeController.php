<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    /**
     * check 404 in admin/frontend side
     *
     * @return \Illuminate\Http\Response
     */
    public function pagenotfound(){
        if(Auth::check()){
            return view('errors.notfound-admin');
        }
        else{
            return view('errors.notfound');
        }

    }

}
