<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;

use DB;


class FaqController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(['permission:*_faq|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $faq = Faq::orderBy('id','DESC')->get();

        return view('faq.list', compact('faq'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'question' => 'required|unique:faqs,question'
        ]);

        $faq = new Faq();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        $status = "New Faq Added Successfully";
        return redirect()->back()->with('status', $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $input = $request->all();
        $id = $input['id'];
        $faq = Faq::where('id', $id)->first();

        echo json_encode($faq);
    }

    /**
     * Editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'edit_question' => 'required',
            'edit_answer' => 'required'
        ]);
        $id = $request->edit_faq_id;

        $faq = Faq::find($id);
        $faq->question = $request->edit_question;
        $faq->answer = $request->edit_answer;
        $faq->save();

        $status = " Faq Updated Successfully";
        return redirect()->back()->with('status', $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
        $id = $request->id;
        $faq = Faq::find($id);
        $faq->delete();

        print_r(json_encode(array('status' => 'success', 'msg' => 'Faq Delete Succesfully')));
    }
}
