<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use DB;



class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(['permission:*_category|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $category = Category::orderBy('id','DESC')->get();

        return view('category.list', compact('category'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:categories,name'
        ]);


        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->fixed_percentage = $request->percentage;

        $category->save();

        $status = "New Category Added Successfully";
        return redirect()->back()->with('status', $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $input = $request->all();
        $id = $input['id'];
        $user = Category::where('id', $id)->first();

        echo json_encode($user);
    }

    /**
     * Editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'edit_name' => 'required'
        ]);
        $id = $request->edit_category_id;

        $category = Category::find($id);
        $category->name = $request->edit_name;
        $category->description = $request->edit_description;
        $category->fixed_percentage = $request->edit_percentage;

        $category->save();

        $productlist = Product::where('sales_category_id',$id)->get();

        foreach($productlist as $rowProduct){
        
            $price =ProductPrice::where('product_id',$rowProduct->id)->where('status','0')->get();

            foreach($price as $rowPrice){

                $priceSet =ProductPrice::where('id',$rowPrice->id)->where('status','0')->first();
                //$priceSet->selling_price =$rowPrice->pricetype_msp +  $request->edit_percentage;
                $priceSet->selling_price =($rowPrice->pricetype_msp) * (1 +($request->edit_percentage/100));
                $priceSet->save();
            }
        }


        $status = " Category Updated Successfully";
        return redirect()->back()->with('status', $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->id;
        $category = Category::find($id);
        $category->delete();

        print_r(json_encode(array('status' => 'success', 'msg' => 'Category Delete Succesfully')));
    }
}
