<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Category;
use App\Models\Outlet;

use Validator;

class SalesCategoryController extends Controller
{


    /**
     * Display array of faq  to logged user
     *
     * @return \Illuminate\Http\Response
     */
    public function salesCategoryList(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'outlet_id' => 'required'
        ]);

        $data = array();
        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $outlet_id = request('outlet_id');

            $employee = Employee::find($user_id);
            if($employee){

                $outlet = Outlet::find($outlet_id);
                if($outlet){
               
                    $category = Category::orderBy('id','ASC')->get();
                    $count = $category->count();
                    if($count>0){

                        $message = "Sales Category list";
                        $data['outlet_name'] = $outlet->name;
                        $data['category'] = $category;
                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);
                    }
                    else{
                        $message ="No Sales Category Added";
                        return response()->json(["status" => "error", 'message'=>$message, 'data'=> $data]);
                    }
                }
                else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Outlet ID', 'data' => $data]);
                }
                
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }   

    }

}
