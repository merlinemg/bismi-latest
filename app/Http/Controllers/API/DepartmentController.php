<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Department;
use Validator;

class DepartmentController extends Controller
{


    /**
     * Display array of faq  to logged user
     *
     * @return \Illuminate\Http\Response
     */
    public function departmentList(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        $user =auth()->guard('employee')->user();

        if($user){

            $data = array();
            if ($validator->fails()) {
                return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
            }
            else {

                $user_id = request('user_id');

                $employee = Employee::find($user_id);
                if($employee){
                   
                    $department = Department::orderBy('id','DESC')->get();
                    $count = $department->count();
                    if($count>0){

                        $message = "Department list";
                        foreach($department as $key=>$row){
                            $data[$key]['id'] = $row['id'];
                            $data[$key]['name'] = $row['department_name'];

                        }
                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);
                    }
                    else{
                        $message ="No Department Added";
                        return response()->json(["status" => "error", 'message'=>$message, 'data'=> $data]);
                    }
                    
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
                }

            } 
        }  

    }

}
