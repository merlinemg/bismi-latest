<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Customer;
use App\Models\Selling_price_salesman;
use App\Models\Outlet;
use App\Models\Order;
use App\Models\Department;
use App\Models\Setting;

use App\Models\Category;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\Product_image;
use App\Models\ProductUnit;
use App\Models\ProductPrice;
use App\Models\ProductBarcode;

use Validator;
use DB;

class CategoryApiController extends Controller
{

    public function getProductByMultipleCategory(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'sales_category_id' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $sales_category_id = request('sales_category_id');
            $customer_id = request('customer_id');
            $category_id = request('category_id');

            $department_req = request('department');
            $subcategory_req = request('subcategory');
            $minprice_req = request('minprice');
            $maxprice_req = request('maxprice');
            $brand_req = request('brand');
            $product_keyword = request('product');
           // $index = request('index');
            $upperlimit= (request('index') + 1)*15 ;
            $lowerlimit = $upperlimit- 15;


            $employee = Employee::find($user_id);
            if($employee){
               
                $sales_category = Category::find($sales_category_id);
                if($sales_category){

                    $customer = Customer::find($customer_id);
                    if($customer){

                        $category = Product_category::find($category_id);
                        if($category){
                            $category_name = $category->category_name;
                        }
                        else{
                            $category_name="";
                        }
                        // if($category){

                        if($category_id){
                            $department = Department::find($category->department_id);

                        }
                        else{
                            $department = Department::find($department_req);

                        }

                        
                        if($department){
                            $department_name = $department->department_name;
                        }
                        else{
                            $department_name="";
                        }

                       
                            $message = "Product list";
                            $data['sales_category_name'] = "".$sales_category->name;
                            $data['customer_name'] = $customer->name;

                            $data['selected_department'] = "".$department_req;
                            $data['selected_category'] = "".$category_id;
                            $data['selected_subcategory'] = "".$subcategory_req;
                            $data['selected_brand'] = "".$brand_req;

                            // $data['category_name'] = $category_name;
                            // $data['department_name'] = $department_name;

                            $settings_prefix = Setting::where('name','PREFIX')->first();
                       
                            $rate_condition = array($minprice_req,$maxprice_req);

                            $product = \DB::table('products')
                               ->join('product_prices','product_prices.product_id','=','products.id')
                               ->join('product_subcategories','products.subcategory_id','=','product_subcategories.id')
                               ->join('departments','products.department_id','=','departments.id')
                               ->select('products.*','product_subcategories.subcategory_name as subcategory_name','departments.department_name',DB::raw("
                                   group_concat(DISTINCT product_prices.pricetype_unit SEPARATOR ',') as conversion_unit, 
                                   group_concat(DISTINCT product_prices.selling_price SEPARATOR ',') as selling_price "))

                               ->where('products.sales_category_id',$sales_category_id)
                               ->where('product_prices.pricetype_msp','!=',0)
                              
                               ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id,$product_keyword)
                                    {
                                        if ($brand_req) {

                                            $brand_array = explode(',',$brand_req);
                                            $countBrand = count($brand_array);
                                            if($countBrand>1){
                                                foreach($brand_array as $rowBrands){
                                                    $query->orWhere('products.brand', 'LIKE','%'.$rowBrands.'%');
                                                }
                                            }
                                            else{
                                                $query->where('products.brand', 'LIKE','%'.$brand_req.'%');
                                            }
                                            
                                        }

                                        if($department_req){

                                            $department_array = explode(',',$department_req);
                                            $query->whereIn('products.department_id', $department_array);

                                        }

                                        if($category_id){

                                            $category_array = explode(',',$category_id);
                                            $query->whereIn('products.category_id', $category_array);
                                        }

                                        if ($subcategory_req) {
                                            $subcategory_array = explode(',',$subcategory_req);
                                            $query->whereIn('products.subcategory_id', $subcategory_array);
                                        }
                                        
                                        if ($minprice_req && $maxprice_req) {
                                           $query->whereBetween('product_prices.selling_price', $rate_condition);
                                           // $query->where('product_prices.selling_price', '>=',$minprice_req);
                                           // $query->where('product_prices.selling_price', '<=',$maxprice_req);
                                        }

                                        if($product_keyword){

                                            $query->where('products.product_name','LIKE','%'.$product_keyword.'%');
                                           
                                        }

                                    })
                               ->groupBy('products.id')
                               ->limit(15)->offset($lowerlimit)
                               ->orderBy('products.featured_status','DESC')->orderBy('products.id','DESC')
                               ->get();

                               
                              //dd($product);
                        

                            $data['product_count']=$product->count();

                            $maxprice =  \DB::table('product_prices')
                            ->select('product_prices.*','products.*','product_units.*')
                            ->join('product_units','product_units.conversion_unit','=','product_prices.pricetype_unit')
                            ->join('products','product_prices.product_id','=','products.id')
                            ->where('products.sales_category_id',$sales_category_id)
                            ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id,$product_keyword)
                            {
                                if($category_id){
                                    $category_array = explode(',',$category_id);
                                    $query->whereIn('products.category_id',$category_array);
                                }

                                if($department_req){

                                    $department_array = explode(',',$department_req);
                                    $query->whereIn('products.department_id', $department_array);
                                }

                                if ($brand_req) {

                                            $brand_array = explode(',',$brand_req);
                                            $countBrand = count($brand_array);
                                            if($countBrand>1){
                                                foreach($brand_array as $rowBrands){
                                                    $query->orWhere('products.brand', 'LIKE','%'.$rowBrands.'%');
                                                }
                                            }
                                            else{
                                                $query->where('products.brand', 'LIKE','%'.$brand_req.'%');
                                            }
                                            
                                }

                                if ($subcategory_req) {
                                    $subcategory_array = explode(',',$subcategory_req);
                                    $query->whereIn('products.subcategory_id', $subcategory_array);
                                }

                                if($product_keyword){

                                    $query->where('products.product_name','LIKE','%'.$product_keyword.'%');
                                   
                                }

                                
                            })
                            ->max('product_prices.selling_price');


                            $minprice =  \DB::table('product_prices')
                            ->select('product_prices.*','products.*','product_units.*')
                            ->join('product_units','product_units.conversion_unit','=','product_prices.pricetype_unit')
                            ->join('products','product_prices.product_id','=','products.id')
                            ->where('products.sales_category_id',$sales_category_id)
                            ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id,$product_keyword)
                            {
                                if($category_id){

                                    $category_array = explode(',',$category_id);
                                    $query->whereIn('products.category_id',$category_array);
                                }

                                if($department_req){
                                    $department_array = explode(',',$department_req);
                                    $query->whereIn('products.department_id', $department_array);
                                }

                                if ($brand_req) {

                                            $brand_array = explode(',',$brand_req);
                                            $countBrand = count($brand_array);
                                            if($countBrand>1){
                                                foreach($brand_array as $rowBrands){
                                                    $query->orWhere('products.brand', 'LIKE','%'.$rowBrands.'%');
                                                }
                                            }
                                            else{
                                                $query->where('products.brand', 'LIKE','%'.$brand_req.'%');
                                            }
                                            
                                 }

                                if ($subcategory_req) {
                                    $subcategory_array = explode(',',$subcategory_req);
                                    $query->whereIn('products.subcategory_id', $subcategory_array);
                                }

                                 if($product_keyword){

                                    $query->where('products.product_name','LIKE','%'.$product_keyword.'%');
                                   
                                }

                                
                            })
                            ->min('product_prices.selling_price');


                             // filter data

                            $data['filter'] = array();

                            $data['filter']['max_price']=(int)round($maxprice)+1;
                            $data['filter']['min_price']=(int)round($minprice)-1;

                    
                            //category based on department code start

                            $department_array = explode(',',$department_req);

                            $data['filter']['category'] = array();

                            $categoryArr = Product_category::select('id','category_name as name')->whereIn('department_id',$department_array)->orderBy('category_name','ASC')->get();

                            $data['filter']['category'] = $categoryArr;

                            //category based on department code end

                            //subcategory based on category code start

                            $category_array = explode(',',$category_id);

                            $data['filter']['subcategory'] = array();

                            $subcategoryArr = Product_subcategory::select('id','subcategory_name as name')->whereIn('category_id',$category_array)->orderBy('subcategory_name','ASC')->get();

                            $data['filter']['subcategory'] = $subcategoryArr;

                            //subcategory based on category code end

                            //all departments code start

                            $data['filter']['department'] = array();

                            $department = Department::select('id','department_name as name')->orderBy('department_name','ASC')->get();

                            $data['filter']['department'] = $department;
                            
                            //all departments code end

                            //brand based on department, category , subcategory code start

                            $data['filter']['brand'] = array();

                            $brandProduct = Product::distinct()
                            ->select('products.brand as name')
                            ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id)
                            {
                                if($category_id){

                                    $category_array = explode(',',$category_id);
                                    $query->whereIn('products.category_id',$category_array);
                                }

                                if($department_req){

                                    $department_array = explode(',',$department_req);
                                    $query->whereIn('products.department_id',$department_array);
                                }


                                if ($subcategory_req) {

                                    $subcategory_array = explode(',',$subcategory_req);
                                    $query->whereIn('products.subcategory_id', $subcategory_array);
                                }

                                
                            })
                            ->orderBy('products.brand','ASC')
                            ->get(['brand']);

                            $data['filter']['brand'] = $brandProduct;

                            //brand based on department, category , subcategory code end

                            // filter data

                            $data['product']=array();

                            foreach($product as $key2=>$row_product){

                                $data['product'][$key2]['product_id'] = (int)$row_product->id;
                                $data['product'][$key2]['product_code'] = "".$row_product->product_code;
                                $data['product'][$key2]['product_name'] = $row_product->product_name;

                                $data['product'][$key2]['product_subcategory'] = "".$row_product->subcategory_name;

                                $data['product'][$key2]['thumbnail_image'] = "".$row_product->thumbnail_image;
                                $data['product'][$key2]['product_description'] = "".$row_product->description;
                               
                                $data['product'][$key2]['product_department'] = "".$row_product->department_name;
                                $data['product'][$key2]['product_package'] = "".$row_product->package;
                                $data['product'][$key2]['product_brand'] = "".$row_product->brand;
                                $data['product'][$key2]['product_shelf_life'] = "".$row_product->shelf_life;
                                $data['product'][$key2]['product_country_of_orgin'] = "".$row_product->country_of_orgin;
                                $data['product'][$key2]['product_overhide'] = (int)$row_product->overhide;
                                
                                $data['product'][$key2]['featured_status'] = (int)$row_product->featured_status;

                                $data['product'][$key2]['prefix'] = $settings_prefix['value'];

                                $product_image = Product_image::where('product_id',$row_product->id)->orderBy('id','DESC')->get();

                                $data['product'][$key2]['images'] = array();
                                
                                foreach($product_image as $key3=>$row_image){
                                    $data['product'][$key2]['images'][$key3] = "".$row_image->image;
                                }

                                $conversion_unit =$row_product->conversion_unit;
                                $array_conversion_unit =explode(',',$conversion_unit);
                                $count_array_conversion_unit = count($array_conversion_unit);


                                $data['product'][$key2]['units'] = array();


                                //$getUnitCount = ProductUnit::where('product_id',$row_product->id)->whereIn('conversion_unit', $array_conversion_unit)->count();


                                $getUnitPrice = ProductUnit::where('product_id',$row_product->id)->whereIn('conversion_unit', $array_conversion_unit)->get();

                                $getUnitCount = $getUnitPrice->count();

                                if($getUnitCount>0){

                                    foreach($getUnitPrice as $key4=>$row_unit){

                                            $price = ProductPrice::where('product_id',$row_product->id)->where('pricetype_unit',$row_unit['conversion_unit'])->first();

                                            $data['product'][$key2]['units'][$key4]['base_quantity'] = "".$row_unit['available_quantity'];
                                       
                                            $data['product'][$key2]['units'][$key4]['conversion_unit'] = "".$row_unit['conversion_unit'];

                                            $data['product'][$key2]['units'][$key4]['conversion_string'] = "".$row_unit['packing_string'];

                                           $data['product'][$key2]['units'][$key4]['base_price'] = (float)$price['pricetype_msp'];

                                            $data['product'][$key2]['units'][$key4]['commision_price'] = (float)$price['selling_price'];
                                        }

                                }

                            }
       
                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);
                        
                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data]);
                    }
                }
                else{
                   return response()->json(["status" => "error", 'message' => 'Invalid Category ID', 'data' => $data]);
                }
                
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }   

    }



}
