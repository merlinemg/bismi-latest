<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Customer;
use App\Models\Outlet;

use Validator;

class CustomerController extends Controller
{

    /**
     * Add new customer
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'customer_id' => '']);
        }
        else {

            $user_id = request('user_id');
            $customer_name = request('customer_name');
            $customer_emailid = request('customer_emailid');
            $customer_phone = request('customer_phone');
            $customer_address = request('customer_address');
            $customer_type = request('customer_type');
            $customer_outlet_id = request('outlet_id');
            $customer_currency = request('customer_currency');
            $customer_vat_trn = request('customer_vat_trn');

            $employee = Employee::find($user_id);
            if($employee){

                if($customer_type=="QPCUSTOMER"){
                    $customer_check=0;
                    $customer_qp  = Customer::where('customer_type','QPCUSTOMER')->count();
                    if($customer_name==""){
                       $customer_name = "QPCUSTOMER ".($customer_qp+1); 
                    }
                    else{
                        $customer_name = request('customer_name');
                    }
                    
                }
                else{
                   $customer_check  = Customer::where('name',request('customer_name'))->where('phone',request('customer_phone'))->where('status',0)->where('customer_type','!=','QPCUSTOMER')->count(); 
                }
                
                

                if($customer_check>0){
                    $customer  = Customer::where('name',request('customer_name'))->where('phone',request('customer_phone'))->where('status',0)->where('customer_type','!=','QPCUSTOMER')->first();
                    return response()->json(["status" => "error", 'message'=>"Customer Already Existed", 'customer_id'=> $customer->id]);
                }
                else{

                    
                    $erpClient = new \GuzzleHttp\Client(['http_errors' => false]);
                     $login_token = $this->erpLogin();
                     $url = "http://bismiho.dyndns.org:4000/api/AddCustomer";

                    $customer_json = array();
                    $customer_json['customer_id'] = '';
                    $customer_json['customer_name'] = $customer_name;
                    $customer_json['customer_email'] = $customer_emailid;
                    $customer_json['customer_phone'] = $customer_phone;
                    $customer_json['customer_address'] = $customer_address;
                    $customer_json['authentication_token'] = $login_token;
                    $customer_json['customer_currency'] = $customer_currency;
                    $customer_json['customer_vat_trn'] = $customer_vat_trn;
                    $customer_json['customer_type'] = $customer_type;

                    $outlet = Outlet::where('id',$customer_outlet_id)->first();
                    $customer_json['customer_outlet_id'] = $outlet['outlet_code'];

                   
                    $erp_request =  $erpClient->post(
                         $url,[
                        'headers' => ['FACTS_API_KEY' => 'koocebqa04b27ar301l4',
                        'Content-Type' => 'application/json'],
                        'json' => $customer_json
                    ]);

                    $erp_response = $erp_request->getBody();

                    $json_decode_erp = json_decode($erp_response,true);

                    if($json_decode_erp['status']=='true'){

                        $customer = new Customer();
                        $customer->name = $customer_name;
                        $customer->customer_id = $json_decode_erp['customer_id'];
                        $customer->emailid = request('customer_emailid');
                        $customer->phone = request('customer_phone');
                        $customer->address = request('customer_address');
                        $customer->customer_type = $customer_type;
                        $customer->outlet_id = $customer_outlet_id;
                        $customer->customer_currency= $customer_currency;
                        $customer->customer_vat_trn= $customer_vat_trn;
                        $customer->flag= 'A';
                        $customer->save();

                        $customerid = $customer->id;

                        return response()->json(["status" => "success", 'message'=>"New Customer Added", 'customer_id'=> $customerid]);
                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Customer Not Added', 'customer_id' => '']);
                    }

                     
                }
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'customer_id' => '']);
            }

        }   

    }

    /**
     * show all customer records
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCustomer(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'outlet_id'=>'required'
        ]);

        $data = array();

        $user =auth()->guard('employee')->user();

        if($user){

            if ($validator->fails()) {
                return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
            }
            else {

                $user_id = request('user_id');
                $outlet_id = request('outlet_id');

                $employee = Employee::find($user_id);
                if($employee){
                
                    $customer = Customer::where('status','0')->where('customer_type','!=','QPCUSTOMER')->where('outlet_id',$outlet_id)->orWhere('customer_type','CREDIT')->orderBy('id','DESC')->get();

                    $count = $customer->count();

                    if($count>0){

                        $message = "Customer list";

                        foreach($customer as $key=>$row){
                            $data[$key]['id']=$row['id'];
                            $data[$key]['name']="".$row['name'];
                            $data[$key]['emailid']="".$row['emailid'];
                            $data[$key]['phone']="".$row['phone'];
                            $data[$key]['address']="".$row['address'];
                            $data[$key]['customer_type']="".$row['customer_type'];
                            $data[$key]['customer_currency']="".$row['customer_currency'];
                            $data[$key]['outlet_id']="".$row['outlet_id'];
                            $data[$key]['customer_vat_trn']="".$row['customer_vat_trn'];
                        }

                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);
                    }
                    else{
                        $message ="No Customer Added";
                        return response()->json(["status" => "error", 'message'=>$message, 'data'=> $data]);
                    }
                    
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
                }

            }  
        } 

    }

    /**
     * show all customer records serach by name / phone
     *
     * @return \Illuminate\Http\Response
     */
    public function customerSearch(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'outlet_id'=>'required',
            'keyword' => 'required'
        ]);

        $data = array();

        $user =auth()->guard('employee')->user();

        if($user){

            if ($validator->fails()) {
                return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
            }
            else {

                $user_id = request('user_id');
                $keyword = request('keyword');
                $outlet_id = request('outlet_id');

                $employee = Employee::find($user_id);
                if($employee){
                
                    $customer = Customer::where('status','0')->where('customer_type','!=','QPCUSTOMER')->where('outlet_id',$outlet_id)->where('name','LIKE','%'.$keyword.'%')->orWhere('phone','LIKE','%'.$keyword.'%')->orderBy('id','DESC')->get();

                    $count = $customer->count();

                    if($count>0){

                        $message = "Customer list";

                        foreach($customer as $key=>$row){
                            $data[$key]['id']=$row['id'];
                            $data[$key]['name']="".$row['name'];
                            $data[$key]['emailid']="".$row['emailid'];
                            $data[$key]['phone']="".$row['phone'];
                            $data[$key]['address']="".$row['address'];
                            $data[$key]['customer_type']="".$row['customer_type'];
                            $data[$key]['customer_currency']="".$row['customer_currency'];
                            $data[$key]['outlet_id']="".$row['outlet_id'];
                            $data[$key]['customer_vat_trn']="".$row['customer_vat_trn'];
                        }

                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);
                    }
                    else{
                        $message ="No Customer Found";
                        return response()->json(["status" => "error", 'message'=>$message, 'data'=> $data]);
                    }
                    
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
                }

            }   
        }

    }

     //login to erp to get token value
    public function erpLogin(){
        $erpClient = new \GuzzleHttp\Client(['http_errors' => false]);
        $url = "http://bismiho.dyndns.org:4000/api/authenticate";
      
        $request =  $erpClient->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => 'koocebqa04b27ar301l4',
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'user_loginId' => 'admin',
                'user_email' => 'admin',
                'deviceID' => 'DEV43',
                'cur_location' => '25.2350393,55.2762502'
            ]
        ]);

        $response = $request->getBody();
        $result = json_decode($response);

        return $result->token;

    }



}
