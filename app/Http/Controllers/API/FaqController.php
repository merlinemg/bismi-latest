<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Faq;
use Validator;

class FaqController extends Controller
{


    /**
     * Display array of faq  to logged user
     *
     * @return \Illuminate\Http\Response
     */
    public function faq(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        $user =auth()->guard('employee')->user();

        if($user){

            $data = array();
            if ($validator->fails()) {
                return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
            }
            else {

                $user_id = request('user_id');

                $employee = Employee::find($user_id);
                if($employee){
                   
                    $faq = Faq::orderBy('id','DESC')->get();
                    $count = $faq->count();
                    if($count>0){

                        $message = "Faq list";
                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $faq]);
                    }
                    else{
                        $message ="No Faq Added";
                        return response()->json(["status" => "error", 'message'=>$message, 'data'=> $data]);
                    }
                    
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
                }

            } 
        }  

    }

}
