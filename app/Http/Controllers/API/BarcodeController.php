<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\ProductUnit;
use App\Models\ProductPrice;
use App\Models\ProductBarcode;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\Product_image;
use App\Models\Department;
use App\Models\Setting;
use Validator;

use DB;

class BarcodeController extends Controller
{


    /**
     * Display array of faq  to logged user
     *
     * @return \Illuminate\Http\Response
     */
    public function barcode(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'sales_category_id' => 'required',
            'barcode' => 'required'
            
        ]);

       // dd($request->all());

        $user =auth()->guard('employee')->user();

        if($user){

            $data = array();
            if ($validator->fails()) {
                return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
            }
            else {

                $user_id = request('user_id');
                $barcode = request('barcode');
                $sales_category_id = request('sales_category_id');

                $employee = Employee::find($user_id);
                if($employee){
                   
                        $category = Category::find($sales_category_id);

                       
                        $settings_prefix = Setting::where('name','PREFIX')->first();
                   

                        $product = \DB::table('products')
                               ->join('product_prices','product_prices.product_id','=','products.id')
                               ->join('product_subcategories','products.subcategory_id','=','product_subcategories.id')
                               ->join('departments','products.department_id','=','departments.id')
                               ->join('product_barcodes','products.id','=','product_barcodes.product_id')
                               ->select('products.*','product_subcategories.subcategory_name as subcategory_name','departments.department_name','product_barcodes.barcode_unit','product_barcodes.barcode')

                               ->where('products.sales_category_id',$sales_category_id)
                               //->where('product_units.conversion_unit','=','product_prices.pricetype_unit')
                               ->where(function($query) use ($barcode)
                                    {
                                        if ($barcode) {
                                            $query->where('product_barcodes.barcode',$barcode);
                                        }

                                    })
                               ->groupBy('products.id')
                               ->orderBy('products.featured_status','DESC')->orderBy('products.id','DESC')
                               ->get();


                               

                               //dd($product);

                               $count = $product->count();

                               if($count>0){

                                    $data['sales_category_name'] = "".$category->name;

                                   foreach($product as $key2=>$row_product){


                                        $data['product'][$key2]['product_id'] = (int)$row_product->id;
                                        $data['product'][$key2]['product_code'] = "".$row_product->product_code;
                                        $data['product'][$key2]['product_name'] = $row_product->product_name;

                                        $data['product'][$key2]['product_subcategory'] = "".$row_product->subcategory_name;

                                        $data['product'][$key2]['thumbnail_image'] = "".$row_product->thumbnail_image;
                                        $data['product'][$key2]['product_description'] = "".$row_product->description;
                                        
                                        
                                        $data['product'][$key2]['product_department'] = "".$row_product->department_name;

                                        $data['product'][$key2]['product_package'] = "".$row_product->package;
                                        $data['product'][$key2]['product_brand'] = "".$row_product->brand;
                                        $data['product'][$key2]['product_shelf_life'] = "".$row_product->shelf_life;
                                        $data['product'][$key2]['product_country_of_orgin'] = "".$row_product->country_of_orgin;
                                        $data['product'][$key2]['product_overhide'] = (int)$row_product->overhide;
                                   

                                    
                                        $data['product'][$key2]['featured_status'] = (int)$row_product->featured_status;

                                        $data['product'][$key2]['prefix'] = $settings_prefix['value'];

                                        $product_image = Product_image::where('product_id',$row_product->id)->orderBy('id','DESC')->get();

                                        $data['product'][$key2]['images'] = array();
                                        
                                        foreach($product_image as $key3=>$row_image){
                                            $data['product'][$key2]['images'][$key3] = "".$row_image->image;
                                        }

                                        $priceProduct = ProductPrice::where('product_id',$row_product->id)->get();

                                     
                                        $data['product'][$key2]['units'] = array();
                                        
                                        foreach($priceProduct as $key4=>$row_unit){

                                            $units = ProductUnit::where('product_id',$row_product->id)->where('conversion_unit',$row_unit['pricetype_unit'])->first();

                                            //$price = ProductPrice::where('product_id',$row_product['id'])->where('pricetype_unit',$row_unit->conversion_unit)->first();

                                               
                                                $data['product'][$key2]['units'][$key4]['conversion_unit'] = "".$units->conversion_unit;
                                                $data['product'][$key2]['units'][$key4]['conversion_string'] = "".$units->packing_string;
                                                $data['product'][$key2]['units'][$key4]['base_quantity'] = "".$units->available_quantity;

                                                $data['product'][$key2]['units'][$key4]['commision_price'] = (float)$row_unit['selling_price'];
                                                $data['product'][$key2]['units'][$key4]['base_price'] = (float)$row_unit['pricetype_msp'];
                                            }
                                    }
                                    return response()->json(["status" => "success", 'message'=>"Product Details", 'data'=> $data]);

                                }
                                else{
                                    return response()->json(["status" => "error", 'message'=>"Product Not Found", 'data'=> $data]);
                                }

                            
                        

                    
                    
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
                }

            } 
        }  

    }

}
