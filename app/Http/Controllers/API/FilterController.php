<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Customer;

use App\Models\Category;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\Product_image;
use App\Models\ProductUnit;
use App\Models\ProductPrice;
use App\Models\ProductBarcode;
use App\Models\Outlet;
use App\Models\Department;
use App\Models\Setting;

use Validator;
use DB;


class FilterController extends Controller
{

     public function getProductFilter(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'sales_category_id' => 'required',
            'keyword' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $category_id = request('sales_category_id');
            $customer_id = request('customer_id');
            $keyword = request('keyword');

            $employee = Employee::find($user_id);
            if($employee){
               
                $category = Category::find($category_id);
                if($category){

                    $customer = Customer::find($customer_id);
                    if($customer){

                       
                        $message = "Product list";
                        $data['sales_category_name'] = "".$category->name;
                        $data['customer_name'] = $customer->name;

                        //brand

                        $product_brand = Product::
                                        join('product_prices','product_prices.product_id','=','products.id')
                                        ->join('product_categories','products.category_id','=','product_categories.id')
                                        ->select('products.*','product_prices.pricetype_unit','product_categories.category_name as category_name')
                                        ->where('products.sales_category_id',$category_id)
                                        ->where('products.brand','LIKE','%'.$keyword.'%')
                                        ->groupBy('products.category_id')
                                        ->limit(5)
                                        ->get();

                        $product_brand_count = $product_brand->count();

                        $data['category'] =array();
                        $data['brand'] =array();
                        $data['subcategory'] =array();
                        $data['product'] =array();

                        if($product_brand_count>0){

                            $search_type ="brand";
                            
                            foreach($product_brand as $key1=>$row_brand){

                                $product_counts = Product::
                                                join('product_prices','product_prices.product_id','=','products.id')
                                                ->where('products.sales_category_id',$category_id)
                                                ->where('products.category_id',$row_brand['category_id'])
                                                ->where('brand','LIKE','%'.$keyword.'%')
                                                ->groupBy('products.id')
                                                ->get();

                                $countProduct = $product_counts->count();
                                
                                $mainArr = array();

                                if($countProduct>0){

                                    $mainArr[$key1]['category_id'] = (int)$row_brand['category_id'];
                                    $mainArr[$key1]['category_name'] = $row_brand['category_name'];

                                    $department = Department::find($row_brand['department_id']);

                                    $mainArr[$key1]['department_id'] = $department['id'];
                                    $mainArr[$key1]['department_name'] = $department['department_name'];

                                    $subcategory = Product_subcategory::where('id',$row_brand['subcategory_id'])->first();

                                    $mainArr[$key1]['subcategory_id'] = (int)$row_brand['subcategory_id'];
                                    $mainArr[$key1]['subcategory_name'] = $subcategory['subcategory_name'];

                                    $mainArr[$key1]['search_key'] = $row_brand['brand'];

                                    $mainArr[$key1]['product_count'] = $countProduct;

                                    $mainArr[$key1]['search_type'] = $search_type;
                                    

                                    if($mainArr){
                                        $data['brand'] = array_merge($data['brand'],$mainArr );
                                       
                                    }

                                }
             
                            }

                        }

                        //category

                        $product_category = Product_category::where('category_name','LIKE','%'.$keyword.'%')->limit(5)->orderBy('featured_status','DESC')->orderBy('id','ASC')->get();
                        
                        $product_category_count = $product_category->count();
                       
                        if($product_category_count>0){
                        

                            $x = 0 ;
                            $search_type ="category";
                            
                            
                            foreach($product_category as $key2=>$row_category){

                                $product_counts = Product::
                                            join('product_prices','product_prices.product_id','=','products.id')
                                            ->select('products.*','product_prices.pricetype_unit')
                                            ->where('products.sales_category_id',$category_id)
                                            ->where('products.category_id',$row_category['id'])
                                            ->groupBy('products.id')
                                            ->get();

                                $countProduct = $product_counts->count();
                                
                                $mainArr = array();

                                if($countProduct>0){

                                    $mainArr[$key2]['category_id'] = (int)$row_category['id'];
                                    $mainArr[$key2]['category_name'] = $row_category['category_name'];

                                    $department = Department::find($row_category['department_id']);

                                    $mainArr[$key2]['department_id'] = $department['id'];
                                    $mainArr[$key2]['department_name'] = $department['department_name'];

                                    $mainArr[$key2]['subcategory_id'] = 0;
                                    $mainArr[$key2]['subcategory_name'] = "";

                                    $mainArr[$key2]['search_key'] = $row_category['category_name'];

                                    $mainArr[$key2]['product_count'] = $countProduct;

                                    $mainArr[$key2]['search_type'] = $search_type;
                                    

                                    if($mainArr){
                                        $data['category'] = array_merge($data['category'],$mainArr );
                                       
                                    }
                                    $x++;

                                }
             
                            }

                        }

                        //sub category

                        $product_subcategory = Product_subcategory::where('subcategory_name','LIKE','%'.$keyword.'%')->limit(5)->orderBy('id','ASC')->get();
                        
                        $product_subcategory_count = $product_subcategory->count();
                       
                        if($product_subcategory_count>0){
                        

                            $x = 0 ;
                            $search_type ="subcategory";
                            
                            
                            foreach($product_subcategory as $key3=>$row_subcategory){

                                $product_counts = Product::
                                            join('product_prices','product_prices.product_id','=','products.id')
                                            ->select('products.*','product_prices.pricetype_unit')
                                            ->where('products.sales_category_id',$category_id)
                                            ->where('products.subcategory_id',$row_subcategory['id'])
                                            ->groupBy('products.id')
                                            ->get();

                                $countProduct = $product_counts->count();
                                
                                $mainArrSub = array();

                                if($countProduct>0){

                                    $category = Product_category::where('id',$row_subcategory['category_id'])->first();

                                    $mainArrSub[$key3]['category_id'] = (int)$row_subcategory['category_id'];
                                    $mainArrSub[$key3]['category_name'] = $category['category_name'];

                                    $department = Department::find($category['department_id']);

                                    $mainArrSub[$key3]['department_id'] = $department['id'];
                                    $mainArrSub[$key3]['department_name'] = $department['department_name'];

                                    $mainArrSub[$key3]['subcategory_id'] = (int)$row_subcategory['id'];
                                    $mainArrSub[$key3]['subcategory_name'] = $row_subcategory['subcategory_name'];

                                    $mainArrSub[$key3]['search_key'] = $row_subcategory['subcategory_name'];

                                    $mainArrSub[$key3]['product_count'] = $countProduct;

                                    $mainArrSub[$key3]['search_type'] = $search_type;
                                    

                                    if($mainArrSub){
                                        $data['subcategory'] = array_merge($data['subcategory'],$mainArrSub );
                                       
                                    }
                                    $x++;

                                }
             
                            }

                        }


                        //product

                        $product_product = Product::where('product_name','LIKE','%'.$keyword.'%')->where('sales_category_id',$category_id)->orderBy('featured_status','DESC')->limit(5)->orderBy('id','ASC')->get();
                        
                        $product_product_count = $product_product->count();

                        //dd($product_product_count);
                       
                        if($product_product_count>0){
                        

                            $x = 0 ;
                            $search_type ="product";
                            
                            
                            foreach($product_product as $key4=>$row_product){

                                $product_counts = Product::
                                            join('product_prices','product_prices.product_id','=','products.id')
                                            ->select('products.*','product_prices.pricetype_unit')
                                            ->where('products.sales_category_id',$category_id)
                                            ->where('products.product_name','LIKE','%'.$keyword.'%')
                                            ->groupBy('products.id')
                                            ->get();

                                $countProduct = $product_counts->count();
                                
                                $mainArrProduct = array();

                                if($countProduct>0){

                                    $mainArrProduct[$key4]['category_id'] = (int)$row_product['category_id'];

                                    $category = Product_category::where('id',$row_product['category_id'])->first();

                                    $mainArrProduct[$key4]['category_name'] = $category['category_name'];

                                    $department = Department::find($row_product['department_id']);

                                    $mainArrProduct[$key4]['department_id'] = $department['id'];
                                    $mainArrProduct[$key4]['department_name'] = $department['department_name'];

                                    $mainArrProduct[$key4]['subcategory_id'] = (int)$row_product['subcategory_id'];

                                    $subcategory = Product_subcategory::where('id',$row_product['subcategory_id'])->first();

                                    $mainArrProduct[$key4]['subcategory_name'] = $subcategory['subcategory_name'];

                                    $mainArrProduct[$key4]['search_key'] = $row_product['product_name'];

                                    $mainArrProduct[$key4]['product_count'] = $countProduct;

                                    $mainArrProduct[$key4]['search_type'] = $search_type;
                                    

                                    if($mainArrProduct){
                                        $data['product'] = array_merge($data['product'],$mainArrProduct );
                                       
                                    }
                                    $x++;

                                }
             
                            }

                        }

                        $data_org['filter'] = array();
                        $data_org['filter'] = array_merge($data['category'],$data['brand'],$data['subcategory'],$data['product']);

                        if(count($data['category'])>0 || count($data['brand'])>0 || count($data['subcategory'])>0 || count($data['product'])>0){
                            print_r(json_encode(array("status" => "success", 'message'=>$message, 'data'=> $data_org)));
                        }
                        else{
                            return response()->json(["status" => "error", 'message' => 'Result Not found', 'data' => array()]);
                        }


                        
                        
                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data_org]);
                    }
                }
                else{
                   return response()->json(["status" => "error", 'message' => 'Invalid Category ID', 'data' => $data_org]);
                }
                
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }   

    }

    public function getVAT(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            
            $employee = Employee::find($user_id);
            if($employee){
                $setting =Setting::where('name','VAT')->first();
                $data['vat'] = (int)$setting['percentage'];

                return response()->json(["status" => "success", 'message' => 'Vat Percentage', 'data' => $data]); 

            }
            else{
               return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]); 
            }
         }
     }


     public function getThreshold(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            
            $employee = Employee::find($user_id);
            if($employee){
                $setting =Setting::where('name','THRESHOLD')->first();
                $data['percentage'] = (int)$setting['percentage'];
                $data['amount'] = (int)$setting['amount'];

                return response()->json(["status" => "success", 'message' => 'Threshold Value', 'data' => $data]); 

            }
            else{
               return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]); 
            }
         }
     }

     

}
