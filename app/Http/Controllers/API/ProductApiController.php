<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Customer;
use App\Models\Selling_price_salesman;
use App\Models\Outlet;
use App\Models\Order;
use App\Models\Department;
use App\Models\Setting;

use App\Models\Category;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\Product_image;
use App\Models\ProductUnit;
use App\Models\ProductPrice;
use App\Models\ProductBarcode;

use Validator;
use DB;

class ProductApiController extends Controller
{

    /**
     * Display array of product category wise  to logged user
     *
     * @return \Illuminate\Http\Response
     */

    public function getProductByCategory(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'sales_category_id' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $category_id = request('sales_category_id');
            $customer_id = request('customer_id');
            $upperlimit= (request('index') + 1)*5 ;
            $lowerlimit = $upperlimit- 5;

            $employee = Employee::find($user_id);
            if($employee){
               
                $category = Category::find($category_id);
                if($category){

                    $customer = Customer::find($customer_id);
                    if($customer){

                       
                        $message = "Product list";
                        $data['sales_category_name'] = "".$category->name;
                        $data['customer_name'] = $customer->name;
                       
                        $product_category = Product_category::limit(5)->offset($lowerlimit)->orderBy('featured_status','DESC')->orderBy('id','ASC')->get();

                        
                        $product_category_count = $product_category->count();
                        $data['product_category_count'] = $product_category_count;

                        $settings_prefix = Setting::where('name','PREFIX')->first();
                       

                       $x = 0 ;
                       
                       $data['category'] =array();
                        
                        foreach($product_category as $key1=>$row_category){

                            $product_counts = Product::where('sales_category_id',$category_id)->where('category_id',$row_category['id'])->count();
                            
                            $mainArr = array();

                            if($product_counts>0){

                                $mainArr[$key1]['category_id'] = $row_category['id'];
                                $mainArr[$key1]['category_name'] = $row_category['category_name'];

                                $department = Department::find($row_category['department_id']);

                                $mainArr[$key1]['department_id'] = $department['id'];
                                $mainArr[$key1]['department_name'] = $department['department_name'];

                                $product = \DB::table('products')
                                    ->join('product_prices','product_prices.product_id','=','products.id')
                                    ->select('products.*','product_prices.pricetype_unit',DB::raw("
                                     group_concat(DISTINCT product_prices.pricetype_unit SEPARATOR ',') as pricetype_unit"))
                                    ->where('products.sales_category_id',$category_id)
                                    ->where('products.category_id',$row_category['id'])
                                    ->where('product_prices.pricetype_msp','!=',0)
                                    ->groupBy('products.id')
                                    ->limit(10)->offset(0)
                                    ->orderBy('products.featured_status','DESC')
                                    ->orderBy('products.id','DESC')
                                    ->get();


                                $mainArr[$key1]['product_count']=$product->count();
                                $mainArr[$key1]['product']=array();

                                foreach($product as $key2=>$row_product){

                                    $product_subcategory = Product_subcategory::where('id',$row_product->subcategory_id)->first();

                                    if($product_subcategory){
                                        $subcat_name = $product_subcategory->subcategory_name;
                                    }
                                    else{
                                        $subcat_name = "";
                                    }
                                    
                                    $mainArr[$key1]['product'][$key2]['product_id'] = (int)$row_product->id;
                                   
                                    $mainArr[$key1]['product'][$key2]['product_code'] = "".$row_product->product_code;
                                    
                                    $mainArr[$key1]['product'][$key2]['product_name'] = $row_product->product_name;

                                    $mainArr[$key1]['product'][$key2]['product_subcategory'] = "".$subcat_name;
                                   
                                    $mainArr[$key1]['product'][$key2]['thumbnail_image'] = "".$row_product->thumbnail_image;
                                    
                                    $mainArr[$key1]['product'][$key2]['product_description'] = "".$row_product->description;

                                    $department = Department::find($row_product->department_id);
                                   
                                    $mainArr[$key1]['product'][$key2]['product_department'] = "".$department->department_name;
                                    
                                    $mainArr[$key1]['product'][$key2]['product_package'] = "".$row_product->package;
                                    
                                    $mainArr[$key1]['product'][$key2]['product_brand'] = "".$row_product->brand;
                                   
                                    $mainArr[$key1]['product'][$key2]['product_shelf_life'] = "".$row_product->shelf_life;
                                   
                                    $mainArr[$key1]['product'][$key2]['product_country_of_orgin'] = "".$row_product->country_of_orgin;
                                    
                                    $mainArr[$key1]['product'][$key2]['product_overhide'] = (int)$row_product->overhide;
                                   

                                    $mainArr[$key1]['product'][$key2]['featured_status'] = $row_product->featured_status;

                                    $mainArr[$key1]['product'][$key2]['prefix'] = $settings_prefix['value'];
                                    
                                    $product_image = Product_image::where('product_id',$row_product->id)->orderBy('id','DESC')->get();

                                    
                                    $mainArr[$key1]['product'][$key2]['images'] = array();
                                    
                                    foreach($product_image as $key3=>$row_image){
                                      
                                        $mainArr[$key1]['product'][$key2]['images'][$key3] = "".$row_image->image;
                                    }

                                    $mainArr[$key1]['product'][$key2]['units']=array();
                            
                                    $conversion_unit =$row_product->pricetype_unit;

                                    $array_conversion_unit =explode(',',$conversion_unit);
                                    $count_array_conversion_unit = count($array_conversion_unit);

                                    $getUnitPrice = ProductUnit::where('product_id',$row_product->id)->whereIn('conversion_unit', $array_conversion_unit)->get();

                                    $getUnitCount = $getUnitPrice->count();

                                    if($getUnitCount>0){

                                        foreach($getUnitPrice as $key4=>$row_unit){

                                            $price = ProductPrice::where('product_id',$row_product->id)->where('pricetype_unit',$row_unit['conversion_unit'])->first();

                                            $mainArr[$key1]['product'][$key2]['units'][$key4]['base_quantity'] = "".$row_unit['available_quantity'];
                                       
                                            $mainArr[$key1]['product'][$key2]['units'][$key4]['conversion_unit'] = "".$row_unit['conversion_unit'];

                                            $mainArr[$key1]['product'][$key2]['units'][$key4]['conversion_string'] = "".$row_unit['packing_string'];

                                           $mainArr[$key1]['product'][$key2]['units'][$key4]['base_price'] = (float)$price['pricetype_msp'];

                                            $mainArr[$key1]['product'][$key2]['units'][$key4]['commision_price'] = (float)$price['selling_price'];
                                        }

                                    }
                                                                      
                                }

                            if($mainArr){
                                $data['category'] = array_merge($data['category'],$mainArr );
                               
                            }
                            $x++;

                        }
         
                        }
                        
                        print_r(json_encode(array("status" => "success", 'message'=>$message, 'data'=> $data)));
                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data]);
                    }
                }
                else{
                   return response()->json(["status" => "error", 'message' => 'Invalid Category ID', 'data' => $data]);
                }
                
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }   

    }
    
      
                        
            
    /**
     * Setting product selling price by salesman
     *
     * @return \Illuminate\Http\Response
     */
    public function setSellingPrice(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'product_id' => 'required',
            'price' => 'required',
            'conversion_unit' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {


            $user_id = request('user_id');
            $product_id = request('product_id');
            $customer_id = request('customer_id');
            $price = request('price');
            $conversion_unit = request('conversion_unit');

            $employee = Employee::find($user_id);
            if($employee){

                $customer = Customer::find($customer_id);
                if($customer){
               
                    $product = Product::find($product_id);
                    if($product){
                            
                            $selling_price_check = Selling_price_salesman::where('customer_id',$customer_id)->where('product_id',$product_id)->where('salesman_id',$user_id)->where('conversion_unit',$conversion_unit)->first();

                            if($selling_price_check){
                                $message = "Price updated successfully";
                                $setting_price = Selling_price_salesman::where('id',$selling_price_check->id)->first();
                            }
                            else{
                                $message = "Price set successfully";
                                $setting_price = new Selling_price_salesman(); 
                                $setting_price->customer_id = $customer_id;
                                $setting_price->product_id = $product_id;
                                $setting_price->salesman_id = $user_id;
                                $setting_price->conversion_unit = $conversion_unit;
                                $setting_price->sales_category_id = $product->sales_category_id;
                            }
                           
                            $setting_price->price = $price;
                            $setting_price->save();
                            
                            return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);

                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Product ID', 'data' => $data]);
                    }
                
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data]);
                }
            }
            else
            {
               return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]); 
            }

        }
    }


    //login to erp to get token value
    public function erpLogin(){
        $erpClient = new \GuzzleHttp\Client(['http_errors' => false]);
        $url = "http://bismiho.dyndns.org:4000/api/authenticate";
      
        $request =  $erpClient->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => 'koocebqa04b27ar301l4',
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'user_name' => 'admin',
                'password' => '123',
                'deviceID' => '985614861475',
                'cur_location' => '25.2596498,55.2977726'
            ]
        ]);

        $response = $request->getBody();
        $result = json_decode($response);

        return $result->token;

    }


    public function getProductBySingleCategory(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'sales_category_id' => 'required',
            'category_id' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $sales_category_id = request('sales_category_id');
            $customer_id = request('customer_id');
            $category_id = request('category_id');

            $department_req = request('department');
            $subcategory_req = request('subcategory');
            $minprice_req = request('minprice');
            $maxprice_req = request('maxprice');
            $brand_req = request('brand');
            $product_keyword = request('product');
           // $index = request('index');
            $upperlimit= (request('index') + 1)*15 ;
            $lowerlimit = $upperlimit- 15;


            $employee = Employee::find($user_id);
            if($employee){
               
                $sales_category = Category::find($sales_category_id);
                if($sales_category){

                    $customer = Customer::find($customer_id);
                    if($customer){

                        $category = Product_category::find($category_id);
                        if($category){
                            $category_name = $category->category_name;
                        }
                        else{
                            $category_name="";
                        }
                        // if($category){

                        if($category_id){
                            $department = Department::find($category->department_id);

                        }
                        else{
                            $department = Department::find($department_req);

                        }

                        
                        if($department){
                            $department_name = $department->department_name;
                        }
                        else{
                            $department_name="";
                        }

                       
                            $message = "Product list";
                            $data['sales_category_name'] = "".$sales_category->name;
                            $data['customer_name'] = $customer->name;
                            $data['category_name'] = $category_name;
                            $data['department_name'] = $department_name;

                            $settings_prefix = Setting::where('name','PREFIX')->first();
                       
                            $rate_condition = array($minprice_req,$maxprice_req);

                            $product = \DB::table('products')
                               ->join('product_prices','product_prices.product_id','=','products.id')
                               ->join('product_subcategories','products.subcategory_id','=','product_subcategories.id')
                               ->join('departments','products.department_id','=','departments.id')
                               ->select('products.*','product_subcategories.subcategory_name as subcategory_name','departments.department_name',DB::raw("
                                   group_concat(DISTINCT product_prices.pricetype_unit SEPARATOR ',') as conversion_unit, 
                                   group_concat(DISTINCT product_prices.selling_price SEPARATOR ',') as selling_price "))

                               ->where('products.sales_category_id',$sales_category_id)
                               ->where('product_prices.pricetype_msp','!=',0)
                              
                               ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id,$product_keyword)
                                    {
                                        if ($brand_req) {

                                            $brand_array = explode(',',$brand_req);
                                            $countBrand = count($brand_array);
                                            if($countBrand>1){
                                                foreach($brand_array as $rowBrands){
                                                    $query->orWhere('products.brand', 'LIKE','%'.$rowBrands.'%');
                                                }
                                            }
                                            else{
                                                $query->where('products.brand', 'LIKE','%'.$brand_req.'%');
                                            }
                                            
                                        }

                                        if ($subcategory_req) {
                                            $query->where('products.subcategory_id', $subcategory_req);
                                        }

                                        if ($minprice_req && $maxprice_req) {
                                            $query->whereBetween('product_prices.selling_price', $rate_condition);
                                        }

                                        if($category_id){
                                            $query->where('products.category_id', $category_id);
                                        }

                                        if($department_req){
                                            $query->where('products.department_id', $department_req);
                                        }

                                        if($product_keyword){

                                            $query->where('products.product_name','LIKE','%'.$product_keyword.'%');
                                           
                                        }

                                    })
                               ->groupBy('products.id')
                               ->limit(15)->offset($lowerlimit)
                               ->orderBy('products.featured_status','DESC')->orderBy('products.id','DESC')
                               ->get();

                               
                              //dd($product);
                        

                            $data['product_count']=$product->count();

                            $maxprice =  \DB::table('product_prices')
                            ->select('product_prices.*','products.*','product_units.*')
                            ->join('product_units','product_units.conversion_unit','=','product_prices.pricetype_unit')
                            ->join('products','product_prices.product_id','=','products.id')
                            ->where('products.sales_category_id',$sales_category_id)
                            ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id,$product_keyword)
                            {
                                if($category_id){
                                    $query->where('products.category_id',$category_id);
                                }

                                if($department_req){
                                    $query->where('products.department_id',$department_req);
                                }

                                if ($brand_req) {

                                            $brand_array = explode(',',$brand_req);
                                            $countBrand = count($brand_array);
                                            if($countBrand>1){
                                                foreach($brand_array as $rowBrands){
                                                    $query->orWhere('products.brand', 'LIKE','%'.$rowBrands.'%');
                                                }
                                            }
                                            else{
                                                $query->where('products.brand', 'LIKE','%'.$brand_req.'%');
                                            }
                                            
                                }

                                if ($subcategory_req) {
                                    $query->where('products.subcategory_id', $subcategory_req);
                                }

                                if($product_keyword){

                                    $query->where('products.product_name','LIKE','%'.$product_keyword.'%');
                                   
                                }

                                
                            })
                            ->max('product_prices.selling_price');


                            $minprice =  \DB::table('product_prices')
                            ->select('product_prices.*','products.*','product_units.*')
                            ->join('product_units','product_units.conversion_unit','=','product_prices.pricetype_unit')
                            ->join('products','product_prices.product_id','=','products.id')
                            ->where('products.sales_category_id',$sales_category_id)
                            ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id,$product_keyword)
                            {
                                if($category_id){
                                    $query->where('products.category_id',$category_id);
                                }

                                if($department_req){
                                    $query->where('products.department_id',$department_req);
                                }

                                if ($brand_req) {

                                            $brand_array = explode(',',$brand_req);
                                            $countBrand = count($brand_array);
                                            if($countBrand>1){
                                                foreach($brand_array as $rowBrands){
                                                    $query->orWhere('products.brand', 'LIKE','%'.$rowBrands.'%');
                                                }
                                            }
                                            else{
                                                $query->where('products.brand', 'LIKE','%'.$brand_req.'%');
                                            }
                                            
                                 }

                                if ($subcategory_req) {
                                    $query->where('products.subcategory_id', $subcategory_req);
                                }

                                 if($product_keyword){

                                    $query->where('products.product_name','LIKE','%'.$product_keyword.'%');
                                   
                                }

                                
                            })
                            ->min('product_prices.selling_price');


                             // filter data

                            $data['filter'] = array();

                            $data['filter']['max_price']=(int)round($maxprice);
                            $data['filter']['min_price']=(int)round($minprice);

                            $categoryArr = Product_category::where('department_id',$department_req)->get();
                            $data['filter']['category'] = array();

                            foreach($categoryArr as $key5=>$rowcategoryArr){

                                $data['filter']['category'][$key5]['category_id'] = $rowcategoryArr['id'];
                                $data['filter']['category'][$key5]['category_name'] = $rowcategoryArr['category_name'];
                            }

                            $subcategoryArr = Product_subcategory::where('category_id',$category_id)->get();
                            $data['filter']['subcategory'] = array();

                            foreach($subcategoryArr as $key6=>$rowsubcategoryArr){

                                $data['filter']['subcategory'][$key6]['subcategory_id'] = $rowsubcategoryArr['id'];
                                $data['filter']['subcategory'][$key6]['subcategory_name'] = $rowsubcategoryArr['subcategory_name'];
                            }

                            $data['filter']['department'] = array();

                            $department = Department::all();
                            foreach($department as $key7=>$rowDepartment){
                               $data['filter']['department'][$key7]['department_id']= $rowDepartment['id'];
                               $data['filter']['department'][$key7]['department_name']= $rowDepartment['department_name'];
                            }

                            $data['filter']['brand'] = array();

                            $brandProduct = Product::distinct()
                            ->select('products.brand as brand_name')
                            ->where(function($query) use ($brand_req, $subcategory_req, $minprice_req, $maxprice_req, $department_req, $rate_condition, $category_id)
                            {
                                if($category_id){
                                    $query->where('products.category_id',$category_id);
                                }

                                if($department_req){
                                    $query->where('products.department_id',$department_req);
                                }


                                if ($subcategory_req) {
                                    $query->where('products.subcategory_id', $subcategory_req);
                                }

                                
                            })
                            ->get(['brand']);

                            $data['filter']['brand'] = $brandProduct;


                            // filter data


                            $data['product']=array();

                            foreach($product as $key2=>$row_product){

                                $data['product'][$key2]['product_id'] = (int)$row_product->id;
                                $data['product'][$key2]['product_code'] = "".$row_product->product_code;
                                $data['product'][$key2]['product_name'] = $row_product->product_name;

                                $data['product'][$key2]['product_subcategory'] = "".$row_product->subcategory_name;

                                $data['product'][$key2]['thumbnail_image'] = "".$row_product->thumbnail_image;
                                $data['product'][$key2]['product_description'] = "".$row_product->description;
                               
                                $data['product'][$key2]['product_department'] = "".$row_product->department_name;
                                $data['product'][$key2]['product_package'] = "".$row_product->package;
                                $data['product'][$key2]['product_brand'] = "".$row_product->brand;
                                $data['product'][$key2]['product_shelf_life'] = "".$row_product->shelf_life;
                                $data['product'][$key2]['product_country_of_orgin'] = "".$row_product->country_of_orgin;
                                $data['product'][$key2]['product_overhide'] = (int)$row_product->overhide;
                                
                                $data['product'][$key2]['featured_status'] = (int)$row_product->featured_status;

                                $data['product'][$key2]['prefix'] = $settings_prefix['value'];

                                $product_image = Product_image::where('product_id',$row_product->id)->orderBy('id','DESC')->get();

                                $data['product'][$key2]['images'] = array();
                                
                                foreach($product_image as $key3=>$row_image){
                                    $data['product'][$key2]['images'][$key3] = "".$row_image->image;
                                }

                                $conversion_unit =$row_product->conversion_unit;
                                $array_conversion_unit =explode(',',$conversion_unit);
                                $count_array_conversion_unit = count($array_conversion_unit);


                                $data['product'][$key2]['units'] = array();


                                //$getUnitCount = ProductUnit::where('product_id',$row_product->id)->whereIn('conversion_unit', $array_conversion_unit)->count();


                                $getUnitPrice = ProductUnit::where('product_id',$row_product->id)->whereIn('conversion_unit', $array_conversion_unit)->get();

                                $getUnitCount = $getUnitPrice->count();

                                if($getUnitCount>0){

                                    foreach($getUnitPrice as $key4=>$row_unit){

                                            $price = ProductPrice::where('product_id',$row_product->id)->where('pricetype_unit',$row_unit['conversion_unit'])->first();

                                            $data['product'][$key2]['units'][$key4]['base_quantity'] = "".$row_unit['available_quantity'];
                                       
                                            $data['product'][$key2]['units'][$key4]['conversion_unit'] = "".$row_unit['conversion_unit'];

                                            $data['product'][$key2]['units'][$key4]['conversion_string'] = "".$row_unit['packing_string'];

                                           $data['product'][$key2]['units'][$key4]['base_price'] = (float)$price['pricetype_msp'];

                                            $data['product'][$key2]['units'][$key4]['commision_price'] = (float)$price['selling_price'];
                                        }

                                }

                            }
       
                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);
                        
                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data]);
                    }
                }
                else{
                   return response()->json(["status" => "error", 'message' => 'Invalid Category ID', 'data' => $data]);
                }
                
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }   

    }


    public function getProductSearch(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'sales_category_id' => 'required',
            'keyword' => 'required',
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $sales_category_id = request('sales_category_id');
            $customer_id = request('customer_id');
            $category_id = request('category_id');
            $keyword = request('keyword');
           // $upperlimit= (request('index') + 1)*20 ;
            //$lowerlimit = $upperlimit- 20;

            $employee = Employee::find($user_id);
            if($employee){
               
                $sales_category = Category::find($sales_category_id);
                if($sales_category){

                    $customer = Customer::find($customer_id);
                    if($customer){


                            $message = "Product list";
                            $data['sales_category_name'] = "".$sales_category->name;
                            $data['customer_name'] = $customer->name;

                            $settings_prefix = Setting::where('name','PREFIX')->first();
                       
                           // $product = Product::where('sales_category_id',$sales_category_id)->where('product_name','LIKE','%'.$keyword.'%')->limit(30)->orderBy('featured_status','DESC')->orderBy('id','DESC')->get();


                            $product = \DB::table('products')
                                    ->join('product_prices','product_prices.product_id','=','products.id')
                                    ->select('products.*','product_prices.pricetype_unit',DB::raw("
                                     group_concat(DISTINCT product_prices.pricetype_unit SEPARATOR ',') as pricetype_unit"))
                                    ->where('products.sales_category_id',$sales_category_id)
                                    ->where('products.product_name','LIKE','%'.$keyword.'%')
                                    ->groupBy('products.id')
                                    ->limit(30)
                                    ->orderBy('products.featured_status','DESC')
                                    ->orderBy('products.id','DESC')
                                    ->get();

                            $data['product_count']=$product->count();
                            $data['product']=array();

                            foreach($product as $key2=>$row_product){                           

                                $product_subcategory = Product_subcategory::where('id',$row_product->subcategory_id)->first();

                                $data['product'][$key2]['product_id'] = (int)$row_product->id;
                                $data['product'][$key2]['product_code'] = "".$row_product->product_code;
                                $data['product'][$key2]['product_name'] = $row_product->product_name;

                                $data['product'][$key2]['product_subcategory'] = "".$product_subcategory->subcategory_name;

                                $data['product'][$key2]['thumbnail_image'] = "".$row_product->thumbnail_image;
                                $data['product'][$key2]['product_description'] = "".$row_product->description;
                                
                                $departmentProduct = Department::find($row_product->department_id);
                               
                                $data['product'][$key2]['product_department'] = "".$departmentProduct->department_name;
                                $data['product'][$key2]['product_package'] = "".$row_product->package;
                                $data['product'][$key2]['product_brand'] = "".$row_product->brand;
                                $data['product'][$key2]['product_shelf_life'] = "".$row_product->shelf_life;
                                $data['product'][$key2]['product_country_of_orgin'] = "".$row_product->country_of_orgin;
                                $data['product'][$key2]['product_overhide'] = (int)$row_product->overhide;
                                
                                $data['product'][$key2]['featured_status'] = (int)$row_product->featured_status;

                                $data['product'][$key2]['prefix'] = $settings_prefix['value'];

                                $product_image = Product_image::where('product_id',$row_product->id)->orderBy('id','DESC')->get();

                                $data['product'][$key2]['images'] = array();
                                
                                foreach($product_image as $key3=>$row_image){
                                    $data['product'][$key2]['images'][$key3] = "".$row_image->image;
                                }

                                $priceProduct = ProductPrice::where('product_id',$row_product->id)->get();

                                 
                                $data['product'][$key2]['units'] = array();

                                $conversion_unit =$row_product->pricetype_unit;

                                $array_conversion_unit =explode(',',$conversion_unit);
                                $count_array_conversion_unit = count($array_conversion_unit);
                                

                                $getUnitPrice = ProductUnit::where('product_id',$row_product->id)->whereIn('conversion_unit', $array_conversion_unit)->get();

                                    $getUnitCount = $getUnitPrice->count();

                                    if($getUnitCount>0){

                                        foreach($getUnitPrice as $key4=>$row_unit){

                                            $price = ProductPrice::where('product_id',$row_product->id)->where('pricetype_unit',$row_unit['conversion_unit'])->first();

                                            $data['product'][$key2]['units'][$key4]['base_quantity'] = "".$row_unit['available_quantity'];
                                       
                                            $data['product'][$key2]['units'][$key4]['conversion_unit'] = "".$row_unit['conversion_unit'];

                                            $data['product'][$key2]['units'][$key4]['conversion_string'] = "".$row_unit['packing_string'];

                                           $data['product'][$key2]['units'][$key4]['base_price'] = (float)$price['pricetype_msp'];

                                            $data['product'][$key2]['units'][$key4]['commision_price'] = (float)$price['selling_price'];
                                        }

                                    }
                                    
                                   
                                
                            }
       

                        return response()->json(["status" => "success", 'message'=>$message, 'data'=> $data]);

                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data]);
                    }
                }
                else{
                   return response()->json(["status" => "error", 'message' => 'Invalid Category ID', 'data' => $data]);
                }
                
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }   
    }


}
