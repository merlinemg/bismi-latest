<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Designation;
use App\Models\Outlet;
use App\Models\EmployeeCategory;
use App\Models\Category;

use Hash;
use Validator;
use Mail;

class EmployeeController extends Controller
{

    /**
     * login API
     *
     * @return \Illuminate\Http\Response
     */
    public function login() {

        $data = array();
        $employee = Employee::where('email', request('username'))->orWhere('emp_id',request('username'))->first();

        if ( !$employee ) { //If no employee was found
            return response()->json(["status" => "error", 'message' => 'User Login Failed.' ,'token'=>'']);
        }
        else{ 
            if (Hash::check(request('password'), $employee->password)) {

                $token = $employee->createToken('BismiApp')->accessToken;

                $emp_details = Employee::find($employee->id);

                // $designation = Designation::where('id','=',$emp_details->designation_id)->first();

                $data = array(
                    'token' => $token,
                    'user_id' =>$employee->id,
                    'name' => "".$emp_details->name,
                    'email' => "".$emp_details->email,
                    'emp_id' => $emp_details->emp_id,
                    'phone_number' => "".$emp_details->phone_number,
                    'gender' => "".$emp_details->gender,
                    'dob' => "".$emp_details->dob,
                    'image' => "".$emp_details->image
                 );
                $salesCategory = EmployeeCategory::where('employee_id',$employee->id)->get();

                $data['sales_category']=array();
                foreach($salesCategory as $key=>$row){

                    $data['sales_category'][$key]['id'] = (int)$row['sales_category_id'];
                    $category = Category::where('id',$row['sales_category_id'])->first();
                    $data['sales_category'][$key]['name'] = $category['name'];

                }
                 return response()->json(["status" => "success", 'message' => 'User Logged in successfully', 'data' => $data]);
    
                
               // return response()->json(["status" => "success", 'message' => 'User Logged in successfully.', 'token' => $token, 'user_id'=> $employee->id]);
            }

            return response()->json(["status" => "error", 'message' => 'User Login Failed.' ,'data'=>$data]);

        }
        
    }

    /**
     * Display array of outlets assigned to logged user
     *
     * @return \Illuminate\Http\Response
     */
    public function outlets(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');

            $emp_details = Employee::find($user_id);

            if($emp_details){

                $outlets = Employee::find($user_id)->outlets()->get();

                $count = $outlets->count();

                if($count>0){
                    $dataArr=array();

                    foreach ($outlets as $outlet) {
                        $tempArr = array("id" => $outlet->id, "name" => "".$outlet->name, "location" => "".$outlet->location,"otherDetails" => "".$outlet->otherDetails);
                        array_push($dataArr, $tempArr);
                    }
                    //$jsn['outlets']=$dataArr;

                    $message = "Outlet List.";
                    return response()->json(["status" => "success", 'message' => $message, 'data' =>$dataArr]);
                }
                else{
                    $message = "No Outlet Assigned.";
                    return response()->json(["status" => "error", 'message' => $message, 'data' =>$data]);
                }
            }
            else{
                    return response()->json(["status" => "error", 'message' => "Invalid User ID", 'data' =>$data]);

            }

        }

    }

    /**
     * Display search  array of outlets assigned to logged user
     *
     * @return \Illuminate\Http\Response
     */
    public function outletsSearch(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'outlet_name' => 'required'
        ]);

        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $outlet_name = request('outlet_name');

            $emp_details = Employee::find($user_id);

            if($emp_details){

                $outlets = Employee::find($user_id)->outlets()
                ->where(function($query) use ($outlet_name)
                                    {
                    $query->where('outlets.location','like','%'.$outlet_name.'%');
                    $query->orWhere('outlets.name','like','%'.$outlet_name.'%');
                })
                ->get();

                //dd($outlets);

                $count = $outlets->count();

                if($count>0){
                    $dataArr=array();

                    foreach ($outlets as $outlet) {

                        $tempArr = array("id" => $outlet->id, "name" => "".$outlet->name, "location" => "".$outlet->location,"otherDetails" => "".$outlet->otherDetails);
                        array_push($dataArr, $tempArr);
                    }
                    //$jsn['outlets']=$dataArr;

                    $message = "Outlet List.";
                    return response()->json(["status" => "success", 'message' => $message, 'data' =>$dataArr]);
                }
                else{
                    $message = "No Outlet Assigned.";
                    return response()->json(["status" => "error", 'message' => $message, 'data' =>$data]);
                }
                
            }
            else{
                    return response()->json(["status" => "error", 'message' => "Invalid User ID", 'data' =>$data]);

            }

        }
    }


    /**
     * Display logged user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        $data = array();
        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the field', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');

            $EmpArr= array();

            $emp_details = Employee::find($user_id);

            if($emp_details){

               // $designation = Designation::where('id','=',$emp_details->designation_id)->first();

                $EmpArr = array(
                    'name' => "".$emp_details->name,
                    'email' => "".$emp_details->email,
                    'emp_id' => $emp_details->emp_id,
                    'phone_number' => "".$emp_details->phone_number,
                    'gender' => "".$emp_details->gender,
                    'dob' => "".$emp_details->dob,
                    'image' => "".$emp_details->image
                 );
                 return response()->json(["status" => "success", 'message' => 'Profile Details', 'data' => $EmpArr]);
            }
            else{
                return response()->json(["status" => "error", 'message' => "Invalid User ID", 'data' =>$EmpArr]);
            }  
        }    

    }

    /**
     * Update logged user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function profileUpdate(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'gender' => 'required',
            'user_id' => 'required'
        ]);
        $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $name = request('name');
            $email = request('email');
            $phone_number = request('phone_number');
            $gender = request('gender');
            $dob = request('dob');
            $image = $request->file('image');

            if ($image != null) {

                $imagename = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/employees');
                $image->move($destinationPath, $imagename);
            }

            $employee = Employee::find($user_id);

            if($employee){

                $checkEmail = Employee::where('email', $email)->where('id','!=',$user_id)->count();

                if($checkEmail>0){
                    return response()->json(["status" => "error", 'message' => 'Email ID already existed', 'data' => $employee]);
                }
                else{

                    $employee->name = $name;
                    $employee->email = $email;
                    $employee->phone_number = $phone_number;
                    $employee->gender = $gender;
                    $employee->dob = $dob;
                    if ($image != null) {
                         if(file_exists("uploads/".$employee->image)){
                            @unlink("uploads/".$employee->image);
                        }
                        $employee->image = "employees/".$imagename;
                    }
                    $employee->save();

                    return response()->json(["status" => "success", 'message' => 'Profile Details Updated', 'data' => $data]);
                }
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }

    }

    /**
     * Change Password logged user 
     *
     * @return \Illuminate\Http\Response
     */

    function changePassword(Request $request) {
         $validator = Validator::make($request->all(), [
            'current_pwd' => 'required',
            'new_pwd' => 'required',
            'user_id' => 'required'
        ]);
         $data = array();
        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $current_pwd = request('current_pwd');
            $new_pwd = request('new_pwd');

            $employee = Employee::find($user_id);
            if($employee){

                if (Hash::check($current_pwd, $employee->password)) {

                    $employee->password = bcrypt($new_pwd);
                    $employee->save();

                    return response()->json(["status" => "success", 'message' => 'Password Changed', 'data' => $data]);

                }
                else{

                    return response()->json(["status" => "error", 'message' => 'Current Password Incorrect', 'data' => $data]);
                }

            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]); 
            }

        }

    }

    /**
     * Forgot Password
     *
     * @return \Illuminate\Http\Response
     */

    function forgotPassword(Request $request) {

        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ]);
        $data = array();
        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $employee = Employee::where('email', request('username'))->orWhere('emp_id',request('username'))->first();

            if($employee){

                   $newpassword = time();
                   $employee->password = bcrypt($newpassword);
                   $employee->save(); 

                   $emailData = array(
                        'to'        => $employee->email,
                        'from'      => 'bismi@gmail.com',
                        'subject'   => 'Bismi - Forgot password',
                        'view'      => 'auth.userForgotPassword'
                     );

                    $data_body = [
                       'username' => $employee->emp_id,
                       'password' => $newpassword
                    ];

                    Mail::send($emailData['view'], ["details"=>$data_body], function ($message) use ($emailData) {
                        $message
                            ->to($emailData['to'])
                            ->from($emailData['from'])
                            ->subject($emailData['subject']);
                    });

                 return response()->json(["status" => "success", 'message' => 'Password Send Successfully', 'data' => $data]);
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid Username', 'data' => $data]);   
            }


        }

    }
    
}
