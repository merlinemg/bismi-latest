<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\Product_image;
use App\Models\Customer;
use App\Models\Selling_price_salesman;
use App\Models\Outlet;
use App\Models\Order;
use App\Models\OrderProduct;
use Validator;
use DB;
use File;
class OrderController extends Controller
{


     public function orderPlace(Request $request){

        
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_id' => 'required',
            'outlet_id' => 'required',
            'product_list' => 'required',
            'sales_category_id' => 'required',
            'payment_mode' =>'required',
            'discount_amount'=>'required',
            'vat_amount' => 'required',
            'total_amount' =>'required'

        ]);

       

        parse_str( request('product_list'), $myData_serial );

        $jsonValue = str_replace("\n", "", request('product_list'));

        $myData_serial = json_decode($jsonValue,true);

       // dd( $myData_serial);
         //exit();

         $datas = array([
            'user_id' => request('user_id'),
            'customer_id' => request('customer_id'),
            'outlet_id' => request('outlet_id'),
            'product_list' =>$myData_serial

         ]);


         $data = array();

         //$request->headers->get('Content-Type');

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => 'Fill the fields', 'data' => $data]);
        }
        else {

            $user_id = request('user_id');
            $customer_id = request('customer_id');
            $outlet_id = request('outlet_id');
            $sales_category_id = request('sales_category_id');
            $payment_mode = request('payment_mode');
            $discount_amount = request('discount_amount');
            $vat_amount = request('vat_amount');
            $order_total_amount = request('total_amount');
            $product_list = $myData_serial;

            $employee = Employee::find($user_id);
            if($employee){

                $customer = Customer::find($customer_id);
                if($customer){

                    $outlet = Outlet::find($outlet_id);
                    if($outlet){

                        $count = count($product_list);

                        $order_unique_code = "ORD".time();


                        $sales_category = Category::find($sales_category_id);
                        //create erp request json
                        $login_token = $this->erpLogin();
                        $order_json = array();
                        $order_json['order_id'] = '';
                        $order_json['customer_id'] = $customer['customer_id'];
                        $order_json['salesman_id'] = $employee['emp_id'];
                        $order_json['outlet_id'] = $outlet['outlet_code'];
                        $order_json['sales_category'] = $sales_category['name'];
                        $order_json['payment_mode'] = $payment_mode;
                        $order_json['discount_amount'] = $discount_amount;
                        $order_json['authentication_token'] = $login_token;
                       // $order_json['order_unique_code'] = $order_unique_code;
                        $order_json['items'] = array();

                         $order = new Order(); 
                         $order->customer_id = $customer_id;
                         $order->order_unique_code = $order_unique_code;
                         $order->salesman_id = $user_id;
                         $order->outlet_id = $outlet_id;
                         $order->sales_category_id = $sales_category_id;
                         $order->payment_mode = $payment_mode;
                         $order->discount_amount = $discount_amount;
                         $order->vat_amount = $vat_amount;
                         $order->order_total_amount = $order_total_amount;
                         $order->save(); 

                         $orderid = $order->id;


                        foreach($product_list as $key=>$row){

                            //create erp request json

                            $product = Product::find($row['product_id']);

                            $order_json['items'][$key]['product_code'] = $product->product_code;
                            $order_json['items'][$key]['qty'] = $row['qty'];
                            $order_json['items'][$key]['rate'] =$row['rate'];
                            $order_json['items'][$key]['unit'] =$row['package'];
                            $order_json['items'][$key]['total_amount'] = $row['total_amount'];

                            //create erp request json

                            //add new order
                             
                             $OrderProduct = new OrderProduct(); 
                             $OrderProduct->order_id= $orderid;
                             $OrderProduct->package= $row['package'];
                             $OrderProduct->product_id = $row['product_id'];
                             $OrderProduct->quantity = $row['qty'];
                             $OrderProduct->rate = $row['rate'];
                             $OrderProduct->total_amount = $row['total_amount'];
                             $OrderProduct->save();  
                             

                        }

                        //update order details to erp and save orderid in db

                        $erpClient = new \GuzzleHttp\Client(['http_errors' => false]);
                        $url = "http://bismiho.dyndns.org:4000/api/placeOrder";
                        $erp_request =  $erpClient->post(
                             $url,[
                            'headers' => ['FACTS_API_KEY' => 'koocebqa04b27ar301l4',
                            'Content-Type' => 'application/json'],
                            'json' => $order_json
                        ]);

                        $erp_response = $erp_request->getBody();

                        $json_decode_erp = json_decode($erp_response,true);

                        if($json_decode_erp['status']=='true'){

                            $order_update_status = Order::where('order_unique_code', '=', $order_unique_code)->get();
                            foreach($order_update_status as $rowOrder){
                                $order_update = Order::find($rowOrder->id);
                                $order_update->order_id = $json_decode_erp['order_id'];
                                $order_update->save();
                            }
                            
                        }

                        return response()->json(["status" => "success", 'message' => 'Order placed Successfully', 'order_id' => $order_unique_code]);

                         //update order details to erp and save orderid in db

                    }
                    else{
                        return response()->json(["status" => "error", 'message' => 'Invalid Outlet ID', 'data' => $data]);
                    }

                }
                else{
                     return response()->json(["status" => "error", 'message' => 'Invalid Customer ID', 'data' => $data]);
                }
            }
            else{
                 return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }

        }
    }

     //login to erp to get token value
    public function erpLogin(){
        $erpClient = new \GuzzleHttp\Client(['http_errors' => false]);
        $url = "http://bismiho.dyndns.org:4000/api/authenticate";
      
        $request =  $erpClient->post(
             $url,[
            'headers' => ['FACTS_API_KEY' => 'koocebqa04b27ar301l4',
            'Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => [
                'user_loginId' => 'admin',
                'user_email' => 'admin',
                'deviceID' => '985614861475',
                'cur_location' => '25.2596498,55.2977726'
            ]
        ]);

        $response = $request->getBody();
        $result = json_decode($response);

        return $result->token;

    }

    public function orderList(Request $request){

           //dd($request->all());

        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);



         
         $data = array();

        if ($validator->fails()) {
            return response()->json(["status" => "error", 'message' => "Fill the field", 'data' => $request->headers->get('Content-Type')]);
        }
        else {

            $user_id = request('user_id');

            $employee = Employee::find($user_id);
            if($employee){
                $order = Order::where('salesman_id',$user_id)->groupBy('order_unique_code')->orderBy('id','DESC')->get();
                
                $count = $order->count();
                if($count>0){

                    $data['order_list']=array();
                    foreach($order as $key =>$rowOrder){
                        $data['order_list'][$key]['order_id'] = $rowOrder['id'];
                        $data['order_list'][$key]['order_unique_code'] = $rowOrder['order_unique_code'];

                        $customer = Customer::find($rowOrder['customer_id']);

                        $data['order_list'][$key]['customer_name'] = "".$customer['name'];
                        $data['order_list'][$key]['phone_number'] = "".$customer['phone'];

                        $outlet = Outlet::find($rowOrder['outlet_id']);
                        $data['order_list'][$key]['outlet_id'] = $outlet['id'];
                        $data['order_list'][$key]['outlet_name'] = "".$outlet['name'];
                        
                        $data['order_list'][$key]['order_date'] = date('d-m-Y', strtotime($rowOrder['created_at']));

                        $data['order_list'][$key]['vat'] = "".$rowOrder['vat_amount'];

                        $orderProducts = OrderProduct::where('order_id',$rowOrder['id'])->get();

                        $countProducts = $orderProducts->count();

                       // $totalPrice = Order::where('salesman_id',$user_id)->where('order_unique_code',$rowOrder['order_unique_code'])->sum('price');

                        $data['order_list'][$key]['total_price'] = "".$rowOrder['order_total_amount'];
                        $data['order_list'][$key]['number_ofItems'] = $countProducts;

                        $data['order_list'][$key]['product_list']=array();

                        foreach($orderProducts as $key1=>$rowProduct){

                            $product = Product::find($rowProduct['product_id']);
                            $product_image = Product_image::where('product_id',$rowProduct['product_id'])->first();

                            $data['order_list'][$key]['product_list'][$key1]['product_id'] = $product['id'];
                            $data['order_list'][$key]['product_list'][$key1]['product_name'] = "".$product['product_name'];
                            $data['order_list'][$key]['product_list'][$key1]['product_image'] = "".$product_image['image'];
                            $data['order_list'][$key]['product_list'][$key1]['package_name'] = $rowProduct['package'];
                            $data['order_list'][$key]['product_list'][$key1]['quantity'] = $rowProduct['quantity'];
                            $data['order_list'][$key]['product_list'][$key1]['product_price'] = "".$rowProduct['total_amount'];
                        }

                        

                        
                    }

                    
                    return response()->json(["status" => "success", 'message' => 'Order List', 'data' => $data]);
                }
                else{
                    return response()->json(["status" => "error", 'message' => 'Empty Order List', 'data' => $data]);
                }
            }
            else{
                return response()->json(["status" => "error", 'message' => 'Invalid User ID', 'data' => $data]);
            }
        }
    }

}
