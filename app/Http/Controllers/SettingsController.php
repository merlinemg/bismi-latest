<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

use DB;


class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(['permission:*_settings|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $settings = Setting::where('name','VAT')->first();

        return view('settings.vat',compact('settings'));
    }


    
    /**
     * Editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'percentage' => 'required'
        ]);
        $id = $request->settings_id;

        $settings = Setting::find($id);
        $settings->percentage = $request->percentage;
       // $settings->amount = $request->price;
        $settings->save();

        $status = " Vat Updated Successfully";
        return redirect()->back()->with('status', $status);
    }

    public function prefix() {
        $settings = Setting::where('name','PREFIX')->first();

        return view('settings.prefix',compact('settings'));
    }

    public function prefixUpdate(Request $request) {
        $this->validate($request, [
            'value' => 'required'
        ]);
        $id = $request->settings_id;

        $settings = Setting::find($id);
        $settings->value = $request->value;
        $settings->save();

        $status = " Prefix Updated Successfully";
        return redirect()->back()->with('status', $status);
    }


    public function threshold() {
        $settings = Setting::where('name','THRESHOLD')->first();

        return view('settings.threshold',compact('settings'));
    }

    public function thresholdUpdate(Request $request) {
        // $this->validate($request, [
        //     'amount' => 'required'
        // ]);
        $id = $request->settings_id;

        $settings = Setting::find($id);
        $settings->percentage = $request->percentage;
        $settings->amount = $request->amount;
        $settings->save();

        $status = " Threshold Updated Successfully";
        return redirect()->back()->with('status', $status);
    }


    
}
