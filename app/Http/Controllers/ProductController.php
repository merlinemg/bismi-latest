<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Product_image;
use App\Models\Product_category;
use App\Models\Product_subcategory;
use App\Models\ProductPrice;
use App\Models\Category;
use App\Models\Department;

use DB;
use View;
use Response;
use Session;

use Illuminate\Support\Facades\Crypt;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(['permission:*_product|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
       

        $product =  \DB::table('products')
                    ->select('products.*','categories.id as catid','categories.name','product_categories.category_name as product_category','product_subcategories.subcategory_name as subcategory_name','departments.department_name')
                    ->join('product_prices','product_prices.product_id','=','products.id')
                    ->join('categories','products.sales_category_id','=','categories.id')
                    ->join('product_categories','products.category_id','=','product_categories.id')
                    ->join('departments','products.department_id','=','departments.id')
                    ->join('product_subcategories','products.subcategory_id','=','product_subcategories.id')
                    ->groupBy('products.product_code')
                    ->orderBy('products.id','DESC')
                    ->paginate(25);

        $department = Department::all();

        $product_edit='';
        $product_price='';
        $department_id='';
        $brand_id='';
        $featured_status='';
        $category_id='';
        $subcategory_id='';
        $name = '';

       // $brandProduct = array();
        $brandProduct = Product::distinct()
                            ->select('products.brand as brand_name')
                            ->get(['brand']);

        $category = array();
        $subcategory = array();


        return view('product.list', compact('product','product_edit','product_price','department','department_id','featured_status','brand_id','brandProduct','category','category_id','subcategory','subcategory_id','name'));
    }

    public function sort(Request $request){

        $department_id = $request->input('department');
        $featured_status = $request->input('featured_status');
        $brand_id= $request->input('brand');
        $category_id = $request->input('category');
        $subcategory_id = $request->input('subcategory');
        $name = $request->input('search_text');

        $product =  \DB::table('products')
                    ->select('products.*','categories.id as catid','categories.name','product_categories.category_name as product_category','product_subcategories.subcategory_name as subcategory_name','departments.department_name')
                    ->join('product_prices','product_prices.product_id','=','products.id')
                    ->join('categories','products.sales_category_id','=','categories.id')
                    ->join('product_categories','products.category_id','=','product_categories.id')
                    ->join('departments','products.department_id','=','departments.id')
                    ->join('product_subcategories','products.subcategory_id','=','product_subcategories.id')
                    ->where(function($query) use ($department_id, $featured_status,$brand_id,$category_id,$subcategory_id,$name)
                    {
                        if($department_id){
                            $query->where('products.department_id',$department_id);
                        }

                        if($name){
                             $query->where('products.product_name', 'like', '%' . $name . '%');
                             $query->orWhere('products.product_code', 'like', '%' . $name . '%');
                        }

                        if($featured_status){

                            if($featured_status=='2')
                            {
                               $featured_status = "0"; 
                            }
                            $query->where('products.featured_status',$featured_status);
                        }

                        if($brand_id){
                            $query->where('products.brand',$brand_id);
                        }
                        if($category_id){
                            $query->where('products.category_id',$category_id);
                        }

                        if($subcategory_id){
                            $query->where('products.subcategory_id',$subcategory_id);
                        }
                        

                    })
                    ->groupBy('products.product_code')
                    ->orderBy('products.id','DESC')
                    ->paginate(25);

         //print_r($department_id);
       //exit();

        $department = Department::all();

        $brandProduct = Product::distinct()
                            ->select('products.brand as brand_name')
                            ->where(function($query) use ($department_id,$category_id,$subcategory_id)
                            {
                                if($department_id){
                                    $query->where('products.department_id',$department_id);
                                }

                                 if($category_id){
                                    $query->where('products.category_id',$category_id);
                                }

                                if($subcategory_id){
                                    $query->where('products.subcategory_id',$subcategory_id);
                                }
                                
                            })
                            ->get(['brand']);

        if($department_id){

        $category = Product_category::
                        select('product_categories.category_name','product_categories.id')
                        ->where(function($query) use ($department_id)
                        {
                            if($department_id){
                                $query->where('product_categories.department_id',$department_id);
                            }
                            
                        })
                        ->get();
        }
        else{
            $category = array();   
        }

        if($category_id){

            $subcategory = Product_subcategory::
                            select('product_subcategories.subcategory_name','product_subcategories.id')
                            ->where(function($query) use ($category_id)
                            {
                                if($category_id){
                                    $query->where('product_subcategories.category_id',$category_id);
                                }
                                
                            })
                            ->get();
        }
        else{
            $subcategory = array();
        }

        $product_edit='';
        $product_price='';


        return view('product.productajax', compact('product','product_edit','product_price','department','department_id','featured_status','brand_id','brandProduct','category','category_id','subcategory','subcategory_id','name'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
      
        $input = $request->all();
        $id = $input['id'];

        $productDetail = \DB::table('products')
                        ->select('products.*','categories.id as catid','categories.name','product_categories.category_name as product_category','product_subcategories.subcategory_name as subcategory_name','departments.department_name')
                        ->join('categories','products.sales_category_id','=','categories.id')
                        ->join('product_categories','products.category_id','=','product_categories.id')
                        ->join('departments','products.department_id','=','departments.id')
                        ->join('product_subcategories','products.subcategory_id','=','product_subcategories.id')
                        ->join('product_prices','product_prices.product_id','=','products.id')
                        ->where('products.product_code', $id)
                        ->first();

                        //print_r($productDetail);


         $product_edit =  \DB::table('products')
                        ->select('products.id','products.product_name','categories.name',
                           DB::raw('group_concat(product_prices.id, ", ", product_prices.pricetype_unit,", ", product_prices.pricetype_msp,", ", product_prices.selling_price, ",", categories.name,"@@") AS bismiPrice'))
                        ->join('categories','products.sales_category_id','=','categories.id')
                        ->join('product_prices','product_prices.product_id','=','products.id')
                        ->where('products.product_code', $id)
                        ->groupBy('categories.name')
                        ->get();

       // print_r($product_edit);

     

       $product_price = ProductPrice::where('product_id','1')->get();

       
       
        $html = View::make('includes.editProductModal', compact('productDetail','product_edit','product_price'))->render();
        return Response::json(['html' => $html]);
    }

    /**
     * Editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
       
       
       // $id = $request->edit_product_id;
       // $x = unset($request['formData'][0]);
        $x =  array_slice($request['formData'], 2);

        // print_r($x);
        // exit();

        foreach($x as $single){
            $id = substr($single['name'], 1, -1);
            $value = $single['value'];


            $product = ProductPrice::where('id',$id)->first();
            $product->selling_price =$value;
            $product->save();
            // dd($product);
            // exit();

        }

        
        $status = " Product Updated Successfully";
        //Session::flash('status', $status); 

        print_r(json_encode(array('status' => 'success', 'msg' => $status,'id'=>$request->selling_price_CTN)));


        //return redirect()->back()->with('status', $status);
    }


    public function setFeatures(Request $request){
        $id = $request->id;
        $status = $request->status;

        $productCode = Product::where('product_code',$id)->get();

       foreach($productCode as $rows){
            $product = product::find($rows->id);
            $product->featured_status = $status;
            $product->save();
       }
        

        if($status=='1'){
            $msg = "Featured Product Set Succesfully..!";
        }
        else{
            $msg = "Non Featured Product Set Succesfully..!";
        }

        Session::flash('status', $msg); 

        print_r(json_encode(array('status' => 'success', 'msg' => 'Featured Product Set Succesfully')));
    }

    public function categoryList(){
        $category = Product_category::all();
        return view('product.category', compact('category'));
    }

    public function setFeaturesCategory(Request $request){
        $id = $request->id;
        $status = $request->status;

        $category = Product_category::find($id);
        $category->featured_status = $status;
        $category->save();

        if($status=='1'){
            $msg = "Featured Category Set Succesfully..!";
        }
        else{
            $msg = "Non Featured Category Set Succesfully..!";
        }

        Session::flash('status', $msg); 

        print_r(json_encode(array('status' => 'success', 'msg' => 'Featured Category Set Succesfully')));
    }

    public function setFeaturesCategoryBulk(Request $request){
        $category_id = $request->id;
        $status = $request->status;

      //  $category_id = explode(",",$id);

        foreach($category_id as $row){
           $category = Product_category::find($row);
            $category->featured_status = $status;
            $category->save(); 
        }

        if($status=='1'){
            $msg = "Featured Category Set Succesfully..!";
        }
        else{
            $msg = "Non Featured Category Set Succesfully..!";
        }

        Session::flash('status', $msg); 

        print_r(json_encode(array('status' => 'success', 'msg' => 'Featured Category Set Succesfully')));
    }

    public function setFeaturesBulk(Request $request){
        $product_id = $request->id;
        $status = $request->status;

        foreach($product_id as $row){

            $pCode = Product::where('product_code',$row)->get();
            foreach($pCode as $pRow){
                $product = Product::find($pRow->id);
                $product->featured_status = $status;
                $product->save();  
            }
            
        }

        if($status=='1'){
            $msg = "Featured Product Set Succesfully..!";
        }
        else{
            $msg = "Non Featured Product Set Succesfully..!";
        }

        Session::flash('status', $msg); 

        print_r(json_encode(array('status' => 'success', 'msg' => 'Featured Product Set Succesfully')));
    }


    function alterPrice(){

        $product =  Product::
                    select('products.*')
                    ->join('product_prices','product_prices.product_id','=','products.id')
                    ->where('product_prices.selling_price','=','999.99999')
                    ->groupBy('products.id')
                    ->get();
        foreach($product as $key=>$rowProduct){
            $category = Category::find($rowProduct['sales_category_id']);

            $price = ProductPrice::where('product_id',$rowProduct['id'])->get();
            foreach($price as $rowPrice){

                $priceUpdate = ProductPrice::where('product_id',$rowProduct['id'])->where('id',$rowPrice['id'])->first();
                $priceUpdate->selling_price = $rowPrice['pricetype_msp']+$category['fixed_percentage'];
                $priceUpdate->save();

              
            }

            
            // if($key=='6'){
            //     exit();
            // }

        }
     }

}
