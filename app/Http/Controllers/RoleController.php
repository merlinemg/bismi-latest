<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;

use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::where('name','!=','super_administrator')->orderBy('id','DESC')->get();
        $permissions = Permission::where([  ['name', '!=', 'super_permission'], ['name', '!=', 'role_management'] ])->get();
        return view('roles.list', compact('roles', 'permissions'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'role_name'  => 'required|unique:roles,name',
           'role_shortname' => 'required',
           'role_desc' => 'required'
        ]);

        $role = new Role();
        $role->name = $request->role_name;
        $role->display_name = $request->role_shortname;
        $role->description = $request->role_desc;
        $role->save();

        $role->permissions()->attach($request->add_permisssion);

        $status = "New Role Added Successfully";
        return redirect()->back()->with('status', $status);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $input = $request->all();
        $id = $input['id'];
        $role = Role::find($id);

        $permission = $role->permissions()->get();

        $data = array('role'=>$role,'permission'=>$permission);

        echo json_encode($data);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'edit_role_name' => 'required',
            'edit_role_shortname' => 'required',
            'edit_role_desc' => 'required'
        ]);


        $id = $request->edit_role_id;

        $role = Role::find($id);
        $role->name = $request->edit_role_name;
        $role->display_name = $request->edit_role_shortname;
        $role->description = $request->edit_role_desc;

        $role->save();

        $role->permissions()->sync($request->edit_add_permisssion);


        $status = " Role Updated Successfully";
        return redirect()->back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        $user = Role::find($id);
        $user->delete();

        print_r(json_encode(array('status' => 'success', 'msg' => 'Role Delete Succesfully')));
    }

    public function destroyBulk(Request $request)
    {
        $id = $request->id;

        foreach($id as $row){
            $user = Role::find($row);
            $user->delete();
        }
        

        print_r(json_encode(array('status' => 'success', 'msg' => 'Role Delete Succesfully')));
    }
}
