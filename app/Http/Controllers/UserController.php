<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Outlet;

use DB;
use Mail;
use Hash;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['permission:*_user|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin_role = Role::where('name', 'super_administrator')->first()->users()->first();

        $users = User::where('id', '!=', $admin_role->id)->orderBy('id','DESC')->get();
        $roles = Role::where('name', '!=','super_administrator')->get();

        return view('users.list', compact('users','roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_name' => 'required',
            'user_email' => 'required|unique:users,email',
            'password' => 'required',
            'role' => 'required'
        ]);


        $user = new User();
        $user->name = $request->user_name;
        $user->email = $request->user_email;
        $user->password = bcrypt($request->password);

        $user->save();
        $user->roles()->sync($request->role);

        $emailData = array(
            'to'        => $request->user_email,
            'from'      => 'bismi@gmail.com',
            'subject'   => 'Bismi - Backend User Login Credentials',
            'view'      => 'auth.userEmail'
         );

        $data = [
           'username' => $request->user_email,
           'password' => $request->password
        ];

        Mail::send($emailData['view'], ["details"=>$data], function ($message) use ($emailData) {
            $message
                ->to($emailData['to'])
                ->from($emailData['from'])
                ->subject($emailData['subject']);
        });

        $status = "New User Added Successfully";
        return redirect()->back()->with('status', $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $id = $request->id;
        $user = User::find($id);
        $role = $user->roles()->get();

        echo json_encode(array("user" => $user, "role" => $role));
    }

    /**
     * Editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'edit_user_name' => 'required',
            'edit_user_email' => 'required',
            'edit_role' => 'required'
        ]);
        $id = $request->edit_user_id;


        $user = User::find($id);
        $user->name = $request->edit_user_name;
        $user->email = $request->edit_user_email;

        $user->save();
        $user->roles()->sync($request->edit_role);

        $status = " User Updated Successfully";
        return redirect()->back()->with('status', $status);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $id = $request->id;

        $user =  User::findOrFail($id);
        $user->delete();

        print_r(json_encode(array('status' => 'success', 'msg' => 'User Delete Succesfully','id'=>$id)));
    }

    // Profile
    public function profile(){
        $auth_id = Auth::user()->id;
        $user = User::find($auth_id);

        return view('users.profile',compact('user'));
    }

    // Update Profile
    public function updateProfile(Request $request){

        $auth_id = Auth::user()->id;
        $user = User::find($auth_id);
        $user->name = $request->name;
        $user->save();

        $status = "Profile Updated Successfully";
        return redirect()->back()->with('status', $status);
    }

    // Change Password
    public function changePassword(Request $request){

        $user = Auth::User();

        $curPassword = $request->curPassword;
        $newPassword = $request->newPassword;

        if (Hash::check($curPassword, $user->password)) {
            $user_id = $user->id;
            $obj_user = User::find($user_id)->first();
            $obj_user->password = bcrypt($newPassword);
            $obj_user->save();

            $status = " Password Updated Successfully";
            return redirect('/home')->with('status', $status);
        }
        else
        {
            $failed = "Current Password Incorrect..Try Again..!";
            return redirect('/home')->with('failed', $failed);
        }
    }
}
