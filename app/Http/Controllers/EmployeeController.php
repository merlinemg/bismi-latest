<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Designation;
use App\Models\Outlet;
use App\Models\Category;
use App\Models\EmployeeCategory;

use DB;
use Mail;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(['permission:*_employee|super_permission']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $employees = Employee::orderBy('id','DESC')->get();
        $designations = Designation::all();
        $outlets = Outlet::all();
        $category = Category::all();

        return view('employees.list', compact('employees','designations', 'outlets','category'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'employee_name' => 'required',
            'employee_email' => 'required|unique:employees,email',
            'password' => 'required',
            'phone_number' => 'required',
            'gender' => 'required',
            'outlet' => 'required'
        ]);


        $employee = new Employee();
        $emp_code = "BISMI".time();
        $employee->name = $request->employee_name;
        $employee->emp_id = $emp_code;
        $employee->email = $request->employee_email;
        $employee->password = bcrypt($request->password);
        $employee->phone_number = $request->phone_number;
        $employee->gender = $request->gender;
        $employee->dob = $request->dob;
       // $employee->designation_id = $request->designation;

        $employee->save();
        $employee->outlets()->attach($request->outlet);

        $emailData = array(
        'to'        => $request->employee_email,
        'from'      => 'bismi@gmail.com',
        'subject'   => 'Bismi - Employee Login Credentials',
        'view'      => 'auth.employeeEmail'
         );

        $data = [
           'employee_code' => $emp_code,
           'username' => $request->employee_email,
           'password' => $request->password
        ];

        Mail::send($emailData['view'], ["details"=>$data], function ($message) use ($emailData) {
            $message
                ->to($emailData['to'])
                ->from($emailData['from'])
                ->subject($emailData['subject']);
        });

        $status = "New User Added Successfully";
        return redirect()->back()->with('status', $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request) {
        $id = $request->id;
        $employee = Employee::find($id);
        $outlets = $employee->outlets()->get();

        $salesCategory = EmployeeCategory::where('employee_id',$id)->get();
        $category = Category::all();

        echo json_encode(array("employee" => $employee, "outlets" => $outlets,"category" => $category,'salesCategory'=>$salesCategory));
    }

    /**
     * Editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'edit_category' => 'required'
        ]);

        $id = $request->edit_employee_id;

        $employee = Employee::find($id);
        //$employee->name = $request->edit_employee_name;
        //$employee->email = $request->edit_employee_email;
        //$employee->phone_number = $request->edit_phone_number;
       // $employee->gender = $request->edit_gender;
       // $employee->dob = $request->edit_dob;
       // $employee->designation_id = $request->edit_designation;

        if($request->edit_password){
             $employee->password = bcrypt($request->edit_password);
        }

        $employee->save();
       // $employee->outlets()->sync($request->edit_outlet);

        $categoryDel = EmployeeCategory::where('employee_id',$id);
        $categoryDel->delete();

        $salescat = $request->edit_category;
       
        foreach($salescat as $row){
            $category = new EmployeeCategory();
            $category->employee_id = $id;
            $category->sales_category_id = $row;
            $category->save();
        }

        $status = " Salesman Updated Successfully";
        return redirect()->back()->with('status', $status);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->id;

        $employee = Employee::find($id);
        $employee->delete();

        print_r(json_encode(array('status' => 'success', 'msg' => 'Salesman Deleted Succesfully','id'=>$id)));
    }
}
