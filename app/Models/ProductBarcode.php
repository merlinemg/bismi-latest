<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductBarcode extends Model
{
    protected $fillable = [
        'barcode_unit', 'barcode_name', 'barcode','product_id',
    ];
}
