<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $fillable = [
        'pricetype_code', 'pricetype_name', 'pricetype_unit','pricetype_msp','product_id','selling_price',
    ];
}
