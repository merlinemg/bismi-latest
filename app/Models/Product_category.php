<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_category extends Model
{	
	public $timestamps = false;
     protected $fillable = [
        'category_code', 'category_name',
    ];
}
