<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeCategory extends Model
{
    protected $fillable = [
        'employee_id', 'sales_category_id',
    ];
}
