<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'category_id', 'subcategory_id', 'thumbnail_image', 'description', 'product_price', 'product_code', 'barcode', 'department', 'package', 'brand', 'shelf_life', 'country_of_orgin', 'selling_price', 'overhide','sales_category_id',
    ];
}
