<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
   protected $fillable = [
        'base_unit', 'base_qty', 'conversion_unit','conversion_qty','conversion_string','product_id',
    ];
}
