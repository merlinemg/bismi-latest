<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id', 'salesman_id','outlet_id','sales_category_id','payment_mode',
    ];
}
