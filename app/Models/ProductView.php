<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductView extends Model
{
	protected $table = 'product_view';
    protected $fillable = [
        'product_id', 'prefix', 'sales_category_id','category_id',
    ];
}
