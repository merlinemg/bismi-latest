<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    public function users() {
        return $this->belongsToMany(User::class);
    }
}
