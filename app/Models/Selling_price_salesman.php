<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Selling_price_salesman extends Model
{
   protected $fillable = [
        'customer_id','product_id','salesman_id','sales_category_id','price',
    ];
}
