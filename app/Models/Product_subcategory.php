<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_subcategory extends Model
{
    public $timestamps = false;
     protected $fillable = [
        'category_id','subcategory_code', 'subcategory_name',
    ];

    protected $casts = [
    	'id' => 'integer',
	];
}
