<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
   protected $fillable = [
        'name','emailid','phone','address','customer_type','customer_outlet_id','customer_currency',
    ];
}
