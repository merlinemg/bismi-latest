<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMode extends Model
{
     protected $fillable = [
        'payment_mode', 'payment_name',
    ];
}
