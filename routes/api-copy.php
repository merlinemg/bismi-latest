<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\EmployeeController@login');

// Route::group(['middleware' => ['auth:api']], function () {
Route::group(['middleware' => ['auth:employee']], function () {

		Route::post('/outlets','API\EmployeeController@outlets');
		Route::post('/outlet/search','API\EmployeeController@outletsSearch');

		Route::post('/profile','API\EmployeeController@profile');
;
		Route::post('/profile/update','API\EmployeeController@profileUpdate');
		Route::post('/profile/changepwd','API\EmployeeController@changePassword');
		

		// FAQ
		Route::post('/faq','API\FaqController@faq');

		//Customer
		Route::post('/customer/add','API\CustomerController@add');
		Route::post('/customer/search','API\CustomerController@customerSearch');
		Route::post('/customer/all','API\CustomerController@getAllCustomer');

		//Sales category
		Route::post('/category/all','API\SalesCategoryController@salesCategoryList');

		//Department
		Route::post('/department/all','API\DepartmentController@departmentList');

		//Payment Mode
		Route::post('/paymentMode','API\PaymentModeController@paymentList');


		//Product
		Route::post('/product/all','API\ProductApiController@getProductByCategory');
		Route::post('/product/ByCategoryID','API\ProductApiController@getProductBySingleCategory');
		Route::post('/product/detail','API\ProductApiController@getProductById');
		Route::post('/product/setprice','API\ProductApiController@setSellingPrice');

		Route::post('/product/order/place','API\OrderController@orderPlace');
		Route::post('/product/order/list','API\OrderController@orderList');

		Route::post('/product/search','API\ProductApiController@getProductSearch');

		Route::post('/product/barcode','API\BarcodeController@barcode');

		Route::post('/product/filter','API\FilterController@getProductFilter');

		Route::post('/vat','API\FilterController@getVAT');

		Route::post('/threshold','API\FilterController@getThreshold');

	});


//

Route::post('/forgotPassword','API\EmployeeController@forgotPassword');



