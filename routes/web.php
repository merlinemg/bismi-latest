<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
  
//    // Artisan::call('config:cache');
// });

Route::group(['middleware' => ['role:super_administrator']], function () {

    /***************** Roles Routes **********************/
    Route::get('roles', 'RoleController@index')->name('role.list');
    Route::post('role/create', 'RoleController@store')->name('role.create');
    Route::post('role/edit', array('as' => 'role.edit','uses' => 'RoleController@edit',));
    Route::post('role/update', array('as' => 'role.update','uses' => 'RoleController@update',));
    Route::post('role/delete', ['as' => 'role.delete', 'uses' => 'RoleController@destroy']);

    Route::post('role/deleteBulk', ['as' => 'role.deleteBulk', 'uses' => 'RoleController@destroyBulk']);
});

    


    

    //Route::get('/users', 'UserController@index')->name('user.list')->middleware (['role:super_administrator','permission:create_user|edit_user|delete_user|view_user|list_user']);

    Route::get('employees', 'EmployeeController@index')->name('employee.list');
    Route::post('employees/create', ['as' => 'employee.create', 'uses' => 'EmployeeController@store']);
    Route::post('employee/edit', array('as' => 'employee.edit','uses' => 'EmployeeController@edit',));
    Route::post('employee/update', array('as' => 'employee.update','uses' => 'EmployeeController@update',));
    Route::post('employee/delete', ['as' => 'employee.delete', 'uses' => 'EmployeeController@destroy']);


    // ***********************
    Route::get('users', 'UserController@index')->name('user.list');
    Route::post('user/create', ['as' => 'user.create', 'uses' => 'UserController@store']);
    Route::post('user/edit', array('as' => 'user.edit','uses' => 'UserController@edit',));
    Route::post('user/update', array('as' => 'user.update','uses' => 'UserController@update',));
    Route::post('user/delete', ['as' => 'user.delete', 'uses' => 'UserController@destroy']);

    Route::post('changepassword', ['as' => 'user.changepwd', 'uses' => 'UserController@changePassword']);

    Route::get('profile', ['as' => 'profile', 'uses' => 'UserController@profile']);
    Route::post('profile/edit', ['as' => 'profile.update', 'uses' => 'UserController@updateProfile']);

    //route has been restricted inside category controller's constructor
    Route::get('category', 'CategoryController@index')->name('category.list');
    Route::post('category/create', ['as' => 'category.create', 'uses' => 'CategoryController@store']);
    Route::post('category/edit', array('as' => 'category.edit','uses' => 'CategoryController@edit',));
    Route::post('category/update', array('as' => 'category.update','uses' => 'CategoryController@update',));
    Route::post('category/delete', ['as' => 'category.delete', 'uses' => 'CategoryController@destroy']);

    Route::get('faq', 'FaqController@index')->name('faq.list');
    Route::post('faq/create', ['as' => 'faq.create', 'uses' => 'FaqController@store']);
    Route::post('faq/edit', array('as' => 'faq.edit','uses' => 'FaqController@edit',));
    Route::post('faq/update', array('as' => 'faq.update','uses' => 'FaqController@update',));
    Route::post('faq/delete', ['as' => 'faq.delete', 'uses' => 'FaqController@destroy']);

    Route::get('product', 'ProductController@index')->name('product.list');
    Route::post('product/edit', array('as' => 'product.edit','uses' => 'ProductController@edit',));
    Route::post('product/update', array('as' => 'product.update','uses' => 'ProductController@update',));
    Route::post('product/setFeatures', array('as' => 'product.setFeatures','uses' => 'ProductController@setFeatures'));
    Route::post('product/setFeaturesBulk', array('as' => 'product.setFeaturesBulk','uses' => 'ProductController@setFeaturesBulk'));

    Route::get('products',['as'=>'product.sort','uses'=>'ProductController@sort']);


    Route::get('product/category', array('as' => 'product.category','uses' => 'ProductController@categoryList'));
    Route::post('category/setFeatures', array('as' => 'category.setFeatures','uses' => 'ProductController@setFeaturesCategory'));
    Route::post('category/setFeaturesBulk', array('as' => 'category.setFeaturesBulk','uses' => 'ProductController@setFeaturesCategoryBulk'));

    Route::get('settings/vat', array('as' => 'settings.vat','uses' => 'SettingsController@index',));
    Route::post('settings/update', array('as' => 'settings.update','uses' => 'SettingsController@update',));

    Route::get('settings/prefix', array('as' => 'settings.prefix','uses' => 'SettingsController@prefix',));
    Route::post('settings/prefixUpdate', array('as' => 'settings.prefixUpdate','uses' => 'SettingsController@prefixUpdate',));
    

    Route::get('settings/threshold', array('as' => 'settings.threshold','uses' => 'SettingsController@threshold',));
    Route::post('settings/thresholdUpdate', array('as' => 'settings.thresholdUpdate','uses' => 'SettingsController@thresholdUpdate',));


    Route::get('order', array('as' => 'order.list','uses' => 'OrderController@index',));
    Route::get('order/{id}/details', array('as' => 'order.detail','uses' => 'OrderController@orderDetails',));

    Route::post('order/report', array('as' => 'order.report','uses' => 'OrderController@export',));

    Route::post('order',['as'=>'order.sort','uses'=>'OrderController@sort']);


    Route::get('/', 'Auth\LoginController@showLoginForm')->name('loginForm');
    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    //save data from erp to our db

    Route::get('/erp/api/erplogin', 'ERPController@erpLogin')->name('erplogin');

    Route::get('/erp/api/getdepartments', 'ERPController@getAllDepartments')->name('getDepartments');

    Route::get('/erp/api/getcategory', 'ERPController@getAllCategory')->name('getCategory');

    Route::get('/erp/api/getoutlets', 'ERPController@getAllOutlets')->name('getOutlets');

    Route::get('/erp/api/getcustomers', 'ERPController@getAllCustomer')->name('getCustomers');

    Route::get('/erp/api/getemployee', 'ERPController@getAllEmployee')->name('getEmployee');

    Route::get('/erp/api/getpaymentMode', 'ERPController@getAllPayment')->name('getPayment');

    Route::get('/erp/api/getproduct', 'ERPController@getAllProduct')->name('getProduct');


    Route::get('/erp/cron', 'ERPController@getAllErpData')->name('getErpData');


   // Route::get('/erp/api/updatePrice', 'ProductController@alterPrice')->name('updatePrice');

    

    //save data from erp to our db

    Route::get('/404', ['as' => 'notfound', 'uses' => 'HomeController@pagenotfound']);

   
    Route::group(['middleware' => 'auth'], function () {

        Route::get('/403', function () {
            return view('errors.forbidden');
        })->name('forbidden');

    });


    Route::get('jquery-loadmore',['as'=>'jquery-loadmore','uses'=>'DemoController@loadMore']);




    

  


