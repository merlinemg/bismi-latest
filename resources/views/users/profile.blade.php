@extends('partials.master')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Profile</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">My Profile</div>

                    <div class="panel-body">
                        <form id="profile-update" class="form-horizontal form-label-left" role="form" method="post" action="{{ route('profile.update') }}" >
                                {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="x_content">
                                    <br />

                                    <div class="form-group">
                                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="name" name="name" value="{{ $user->name}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="short-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email ID<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="email" name="email" value="{{ $user->email}}" readonly="">
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-sm-8 col-xs-8">

                                        <button type="submit" style="float: right" class="btn btn-warning" name="btn-submit">Update </button>

                                    </div>

                                </div>
                            </div>
                        </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_scripts')

<script type="text/javascript">
    $(function() {

        var $form = $("#profile-update"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });
        $form.validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5,
                    letters: true
                },

            },
            messages: {
                name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!"
                },
            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });
    });
</script>

@stop