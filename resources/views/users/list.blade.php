@extends('partials.master')
@section('content')
<div class="right_col" role="main">
<div class="alert alert-success" id="msg" style="display: none;"></div>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Users</h3>
            </div>
            @permission(['create_user','super_permission'])
                <div class="title_right">
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-user-modal"><i class="fa fa-folder-open"></i> Create New User Record</button>
                </div>
            @endpermission
        </div>

        <div class="clearfix"></div>

        @permission(['list_user','super_permission'])

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title">
                    <h2>All Users<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                </div>

                <table id="usertable" class="table table-striped jambo_table  contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl.No</th>
                            <th class="column-title">Name</th>
                            <th class="column-title">Email</th>
                            <th class="column-title">Role</th>
                            @permission(['edit_user','super_permission','delete_user'])
                                <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                            </th>
                             @endpermission

                        </tr>
                    </thead>

                    <tbody>
                        @foreach($users as $user)
                            <tr class="even pointer" id="{{ $user->id }}">

                               <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>
                                <td>
                                    {{ $user->name }}
                                </td>

                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->roles[0]->display_name }}
                                </td>
                                @permission(['edit_user','super_permission','delete_user'])
                                <td class="last">
                                    @permission(['edit_user','super_permission'])
                                        <button type="button" class="btn btn-warning btn-xs" onClick="editUserModal({{ $user->id }});">Edit</button>
                                    @endpermission
                                    @permission(['delete_user','super_permission'])
                                        <button type="button" class="btn btn-danger btn-xs" onClick="deleteUserModal({{ $user->id }});">Delete</button>
                                    @endpermission
                                </td>
                                @endpermission
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
            </div>
        </div>

        @endpermission
    </div>
</div>

<!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete User Record</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->

@include('includes.userModal')
@include('includes.editUserModal')

@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#usertable').DataTable();
</script>

<script type="text/javascript">
    @if (count($errors) > 0)
        $('.add-user-modal').modal('show');
    @endif
</script>

<script type="text/javascript">
    $(function() {

        var $form = $("#user-create"),
        $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });
        $form.validate({
            rules: {
                user_name: {
                    required: true,
                    minlength: 3,
                    letters: true
                },
                user_email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 5
                },
                cpassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                role: {
                    required: true
                },
            },
            messages: {
                user_name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!",
                    letters:"Only letters and spaces are allowed"
                },
                user_email: {
                    required: "Please specify a valid email address",
                    email: "Your email address must be in the format of name@domain.com"
                },
                password: {
                    required: "Please specify password",
                    minlength: "At least {0} characters required!"
                },
                cpassword: {
                    required: "Please specify Confirm password",
                    equalTo: "Please specify Confirm Password Same as Password",
                    minlength: "At least {0} characters required!"
                },
                role: {
                    required: "You must assign a role"
                },
            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });


        // Edit User
        var $form_edit = $("#user-edit"),
        $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form_edit.validate({
            rules: {
                edit_user_name: {
                    required: true,
                    minlength: 3,
                    letters: true
                },
                edit_user_email: {
                    required: true,
                    email: true
                },
                edit_role: {
                    required: true
                }
            },
            messages: {
                edit_user_name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!"
                },
                edit_user_email: {
                    required: "Please specify a valid email address",
                    email: "Your email address must be in the format of name@domain.com"
                },
                edit_role: {
                    required: "You must assign a role"
                }
            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });

    });
</script>

@stop