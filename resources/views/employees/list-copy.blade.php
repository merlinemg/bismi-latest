@extends('partials.master')
@section('content')
<div class="right_col" role="main">
<div class="alert alert-success" style="display: none;"></div>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Salesman</h3>
            </div>
            @permission(['create_employee','super_permission'])
            <!-- <div class="title_right">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-employee-modal"><i class="fa fa-folder-open"></i> Create New Salesman </button>
            </div> -->
            @endpermission
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table id="employeetable" class="table table-striped jambo_table  contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl.No</th>
                            <th class="column-title">Name</th>
                            <th class="column-title">Salesman ID</th>
                            <th class="column-title">Email</th>
                            <th class="column-title">Phone Number</th>
                            <th class="column-title">Gender</th>
                            <th class="column-title">Date of Birth</th>
                            @permission(['edit_employee','super_permission','delete_employee'])
                                <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                            </th>
                            @endpermission
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($employees as $employee)
                            <tr class="even pointer">

                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $employee->name }}
                                </td>

                                <td>
                                    {{ $employee->emp_id }}
                                </td>
                                <td>
                                    {{ $employee->email }}
                                </td>

                                <td>
                                    {{ $employee->phone_number }}
                                </td>

                                <td>
                                    {{ $employee->gender }}
                                </td>

                                <td>
                                    {{ date('d-M-Y', strtotime($employee->dob)) }}
                                </td>

                                <td class=" last">
                                    @permission(['edit_employee','super_permission'])
                                        <button type="button" class="btn btn-warning btn-xs" onClick="editEmployeeModal({{ $employee->id }});">Edit</button>
                                    @endpermission
                                    <!-- @permission(['delete_employee','super_permission'])
                                        <button type="button" class="btn btn-danger btn-xs" onClick="deleteEmployeeModal({{ $employee->id }});">Delete</button>
                                    @endpermission -->
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete Employee</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->

@include('includes.employeeModal')
@include('includes.editEmployeeModal')

@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#employeetable').DataTable();
</script>

<script type="text/javascript">
    @if (count($errors) > 0)
        $('.add-employee-modal').modal('show');
    @endif
</script>

<script type="text/javascript">
    $(function() {

        var $form = $("#employee-create"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });
        $form.validate({
            rules: {
                employee_name: {
                    required: true,
                    minlength: 5,
                    letters: true
                },
                employee_email: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    number: true
                },
                password: {
                    required: true,
                    minlength: 5
                },
                cpassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                outlet: {
                    required: true
                },
                gender: {
                    required: true
                }
            },
            messages: {
                employee_name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!"
                },
                employee_email: {
                    required: "Please specify a valid email address",
                    email: "Your email address must be in the format of name@domain.com"
                },
                phone_number: {
                    required: "Please specify a valid mobile number",
                    number: "Please enter a valid number",
                    minlength: "At least {0} numbers required!"
                },
                password: {
                    required: "Please specify password",
                    minlength: "At least {0} characters required!"
                },
                cpassword: {
                    required: "Please specify Confirm password",
                    equalTo: "Please specify Confirm Password Same as Password",
                    minlength: "At least {0} characters required!"
                },
                outlet: {
                    required: "You must select atleast 1 outlet"
                },
                gender: {
                    required: "You must select your gender"
                }
            },

            errorPlacement: function(error, element) {
                if (element.is(":radio")) {
                    error.prependTo('#gender-error');
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Saved");
                return false;
            }
        });

        // Edit User

        var $form_edit = $("#employee-edit"),
        $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form_edit.validate({
            rules: {
                edit_employee_name: {
                    required: true,
                    minlength: 5,
                    letters: true
                },
                edit_employee_email: {
                    required: true,
                    email: true
                },
                edit_phone_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    number: true
                },
                'edit_category[]': {
                    required: true
                },
                'edit_outlet[]': {
                    required: true
                },
                edit_gender: {
                    required: true
                }
            },
            messages: {
                edit_employee_name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!"
                },
                edit_employee_email: {
                    required: "Please specify a valid email address",
                    email: "Your email address must be in the format of name@domain.com"
                },
                edit_phone_number: {
                    required: "Please specify a valid mobile number",
                    number: "Please enter a valid number",
                    minlength: "At least {0} numbers required!"
                },
                'edit_category[]': {
                    required: "You must select atleast 1 sales category"
                },
                'edit_outlet[]': {
                    required: "You must select atleast 1 outlet"
                },
                edit_gender: {
                    required: "You must select your gender"
                }
            },

            errorPlacement: function(error, element) {
                if (element.is(":radio")) {
                    error.prependTo('#gender-error');
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Saved");
                return false;
            }
        });

    });

 // $(".edit_category").multipleSelect(
 //    {
 //        width: '100%',
 //        selectAll: false,
 //        isOpen: false,
 //        keepOpen: false
    
 //    }
 //    );  

</script>

@stop