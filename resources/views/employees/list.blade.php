@extends('partials.master')
@section('content')

<div class="loader-page"></div>

<div class="right_col" role="main">
<div class="alert alert-success" style="display: none;"></div>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Salesman</h3>
            </div>
            @permission(['create_employee','super_permission'])
            <!-- <div class="title_right">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-employee-modal"><i class="fa fa-folder-open"></i> Create New Salesman </button>
            </div> -->
            @endpermission
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

             <div class="x_panel">
                  <div class="x_title">
                    <h2>All Salesman<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                  </div>
                <table id="employeetable" class="table table-striped jambo_table  contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl.No</th>
                            <th class="column-title">Name</th>
                            <th class="column-title">Salesman ID</th>
                            <th class="column-title">Email</th>
                            <th class="column-title">Phone Number</th>
                            <th class="column-title">Gender</th>
                            <th class="column-title">Date of Birth</th>
                            @permission(['edit_employee','super_permission','delete_employee'])
                                <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                            </th>
                            @endpermission
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($employees as $employee)
                            <tr class="even pointer">

                                 <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>
                                <td>
                                    {{ $employee->name }}
                                </td>

                                <td>
                                    {{ $employee->emp_id }}
                                </td>
                                <td>
                                    {{ $employee->email }}
                                </td>

                                <td>
                                    {{ $employee->phone_number }}
                                </td>

                                <td>
                                    {{ $employee->gender }}
                                </td>

                                <td>
                                    {{ date('d-M-Y', strtotime($employee->dob)) }}
                                </td>

                                <td class=" last">
                                    @permission(['edit_employee','super_permission'])
                                        <button type="button" class="btn btn-warning btn-xs" onClick="editEmployeeModal({{ $employee->id }});">Edit</button>
                                    @endpermission
                                    <!-- @permission(['delete_employee','super_permission'])
                                        <button type="button" class="btn btn-danger btn-xs" onClick="deleteEmployeeModal({{ $employee->id }});">Delete</button>
                                    @endpermission -->
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete Employee</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->

@include('includes.employeeModal')
@include('includes.editEmployeeModal')

@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#employeetable').DataTable();
    $(window).load(function() {
        $(".loader-page").fadeOut("slow");
    });
</script>

<script type="text/javascript">
    @if (count($errors) > 0)
        $('.add-employee-modal').modal('show');
    @endif
</script>

<script type="text/javascript">
    $(function() {

        var $form = $("#employee-create"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });
        $form.validate({
            rules: {
                employee_name: {
                    required: true,
                    minlength: 5,
                    letters: true
                },
                employee_email: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    number: true
                },
                password: {
                    required: true,
                    minlength: 5
                },
                cpassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                outlet: {
                    required: true
                },
                gender: {
                    required: true
                }
            },
            messages: {
                employee_name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!"
                },
                employee_email: {
                    required: "Please specify a valid email address",
                    email: "Your email address must be in the format of name@domain.com"
                },
                phone_number: {
                    required: "Please specify a valid mobile number",
                    number: "Please enter a valid number",
                    minlength: "At least {0} numbers required!"
                },
                password: {
                    required: "Please specify password",
                    minlength: "At least {0} characters required!"
                },
                cpassword: {
                    required: "Please specify Confirm password",
                    equalTo: "Please specify Confirm Password Same as Password",
                    minlength: "At least {0} characters required!"
                },
                outlet: {
                    required: "You must select atleast 1 outlet"
                },
                gender: {
                    required: "You must select your gender"
                }
            },

            errorPlacement: function(error, element) {
                if (element.is(":radio")) {
                    error.prependTo('#gender-error');
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Saved");
                return false;
            }
        });

        // Edit User

        var $form_edit = $("#employee-edit"),
        $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form_edit.validate({
            rules: {
                edit_employee_name: {
                    required: true,
                    minlength: 5,
                    letters: true
                },
                'edit_category[]': {
                    required: true
                }
                
            },
            messages: {
                edit_employee_name: {
                    required: "Please specify your name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!"
                },
                
                'edit_category[]': {
                    required: "You must select atleast 1 sales category"
                }
                
                
               
            },

            errorPlacement: function(error, element) {
                if (element.is(":checkbox")) {
                    error.prependTo('#category_error');
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Saved");
                return false;
            }
        });

    });

$("#bt-editemp").click(function(){
    
     //$("#edit_cpassword-error").html('');
    if($("#edit_password").val()!="" && $("#edit_cpassword").val()==""){
        //alert(2);
        $("#edit_cpassword-error").html("Please specify confirm password")

    }
    else if($("#edit_password").val()!=$("#edit_cpassword").val()){
       // alert(1);
        $("#edit_mismatched-error").html("Password Mismatched...!")
    }
    
    else{
        //alert(3);
        $("#employee-edit").submit();
    }

});


 

</script>

@stop