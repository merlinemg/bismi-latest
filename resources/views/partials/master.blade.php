<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Bismi Whole Sale Point</title>

  <!-- CSS INJECTION -->
  @include('partials/styles')
  <!-- CSS INJECTION -->

</head>

<body class="nav-md">
  <div class="container body">
      <div class="main_container">
          <div class="col-md-3 left_col menu_fixed">
              <div class="left_col scroll-view">
                  <div class="navbar nav_title navLogo" style="border: 0;">
                      <a href="javascript:void(0)" class="site_title">
                        <img src="{{ asset('img/logo.png') }}" />
                      </a>
                  </div>

                  <div class="clearfix"></div>

                  <!-- menu profile quick info -->
                  <div class="profile">
                    <!-- <div class="profile_pic">
                      <img src="img/main_logo.png" alt="" class="img-circle profile_img">
                    </div> -->
                    <div class="profile_info">
                      <h2><span>Welcome,</span> {{ \Auth::user()->name }}</h2>
                    </div>
                  </div>
                  <!-- /menu profile quick info -->


                  <div class="clearfix"></div>

                  <!-- sidebar menu --> <!-- SIDEBAR INJECTION -->
                   @include('partials/sidebar')
                  <!-- /sidebar menu --> <!-- SIDEBAR INJECTION -->

                  <!-- /menu footer buttons -->
                  <div class="sidebar-footer hidden-small">
                      <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                      </a>
                      <!-- <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                      </a>
                      <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                      </a>
                      <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                      </a> -->
                  </div>
                  <!-- /menu footer buttons -->

              </div>
          </div>

          <!-- top navigation --> <!-- HEADER INJECTION -->
          @include('partials/header')
          <!-- /top navigation --> <!-- HEADER INJECTION -->

          <!-- Change Password -->
          @include('includes.changepwd')
          <!-- Change Password -->

          <!-- page content --> <!-- MAIN CONTENT INJECTION -->
          @yield('content')




          <!-- /page content --> <!-- MAIN CONTENT INJECTION -->

          <!-- CSS INJECTION -->
          @include('partials/footer')
          <!-- CSS INJECTION -->

      </div>
  </div>

  <!-- JS INJECTION -->
  @include('partials/scripts')
  @yield('page_scripts')
  <!-- JS INJECTION -->

</body>
</html>