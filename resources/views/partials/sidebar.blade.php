<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li>
                <a href="{{ route('home') }}"><i class="fa fa-home"></i>Dashboard</a>
            </li>
            @role('super_administrator')
            <li>
                <a><i class="fa fa-graduation-cap"></i>Manage Roles<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('role.list') }}">List Roles</a></li>
                    <!-- <li><a data-toggle="modal" data-target=".add-role-modal">Add New Role</a></li> -->
                </ul>
            </li>
            @endrole
            @permission(['*_user','super_permission'])
            <li>
                <a><i class="fa fa-users"></i>Manage Users<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('user.list') }}">List Users</a></li>
                    <!-- <li><a href="#" data-toggle="modal" data-target=".add-user-modal">Add New User</a></li> -->
                </ul>
            </li>
            @endpermission
            @permission(['*_category','super_permission'])
            <li>
                <a><i class="fa fa-bars"></i>Manage Sales Category<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('category.list') }}">List Sales Category</a></li>
                </ul>
            </li>
            @endpermission
            @permission(['*_employee','super_permission'])
            <li>
                <a><i class="fa fa-users"></i>Manage Salesman<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('employee.list') }}">List Salesman</a></li>
                    <!-- <li><a href="#" data-toggle="modal" data-target=".add-user-modal">Add New Employee</a></li> -->
                </ul>
            </li>
            @endpermission
            @permission(['*_product','super_permission'])

           @php $url = \Request::route()->getName(); @endphp

            <li <?php echo ($url=='product.sort')? 'class="active"':''?>>
                <a><i class="fa fa-bars"></i>Manage Product<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" <?php echo ($url=='product.sort')? 'style="display: block;"':''?> >
                    <li <?php echo ($url=='product.sort')? 'class="current-page"':''?> ><a href="{{ route('product.list') }}">List Product</a></li>
                    <li><a href="{{ route('product.category') }}">List Category</a></li>
                    
                </ul>
            </li>
            @endpermission
            @permission(['*_order','super_permission'])
            <li>
                <a><i class="fa fa-question-circle"></i>Manage Orders<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('order.list') }}">Orders</a></li>
                </ul>
            </li>
            @endpermission
           <!--  @permission(['*_faq','super_permission'])
            <li>
                <a><i class="fa fa-question-circle"></i>Manage FAQs<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('faq.list') }}">List FAQs</a></li>
                </ul>
            </li>
            @endpermission -->
            @permission(['*_settings','super_permission'])
            <li>
                <a><i class="fa fa-question-circle"></i>Manage Settings<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('settings.vat') }}">VAT</a></li>
                    <li><a href="{{ route('settings.prefix') }}">PREFIX</a></li>
                    <li><a href="{{ route('settings.threshold') }}">THRESHOLD</a></li>
                </ul>
            </li>
            @endpermission
           
        </ul>
    </div>
</div>