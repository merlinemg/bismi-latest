<!-- Bootstrap -->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
<!-- NProgress -->
<link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="{{ asset('css/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet"/>

<!-- iCheck -->
<link href="{{ asset('css/green.css') }}" rel="stylesheet">

<!-- Switchery -->
<link href="{{ asset('css/switchery.min.css') }}" rel="stylesheet">

<!-- Bootstrap Colorpicker -->
<link href="{{ asset('css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">

<!-- bootstrap-daterangepicker -->
<link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">



<link href="{{ asset('css/multiple-select.css') }}" rel="stylesheet">

<!-- DataTables -->
<link href="{{ asset('css/dataTables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/dataTables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>

<!-- Custom Theme Style -->
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">