<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ asset('img/img.jpg') }}" alt="">{{ \Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <!-- <li><a href="{{ route('profile') }}"> My Profile</a></li> -->

                        <li>
                          <a data-toggle="modal" data-target=".change-password-modal">
                            <span>Change Password</span>
                          </a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    </ul>
                </li>

            </ul>
        </nav>

    </div>
</div>
<div class="container">
  <div class="container-fluid ">

      <div class="row">
          @if (session('status'))
              <div class="alert alert-success" style="margin-left:250px;color:#ffffff; margin-top:100px">
                  {{ session('status') }}
              </div>
          @endif

          @if (session('failed'))
              <div class="alert alert-danger" style="margin-left:250px;color:#ffffff; margin-top:100px">
                  {{ session('failed') }}
              </div>
          @endif

      </div>
  </div>
</div>

