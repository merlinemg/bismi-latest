<!-- jQuery -->

<script src="{{ asset('js/jquery.min.js') }}"></script>

<!-- Bootstrap -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('js/nprogress.js') }}"></script>
<!-- jQuery custom content scroller -->
<script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- iCheck -->
<script src="{{ asset('js/icheck.min.js') }}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset('js/moment.min.js') }}"></script>

<!-- jQuery Smart Wizard -->
<script src="{{ asset('js/jquery.smartWizard.js') }}"></script>

<!-- Bootstrap Colorpicker -->
<script src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>

<!-- Switchery -->
<script src="{{ asset('js/switchery.min.js') }}"></script>

<!-- DateRange Picker -->
<script src="{{ asset('js/daterangepicker.js') }}"></script>

<!-- Bootstrap Multiselect -->
<script src="{{ asset('js/multiselect.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('js/select2.min.js') }}"></script>

<!-- Datatables -->
<script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables/dataTables.bootstrap.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('js/custom.js') }}"></script>

<!-- Jquery Validation Scripts -->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var base_url = "<?php echo URL::to('/'); ?>";
</script>

<script src="{{ asset('js/main.js') }}"></script>

<script>
    $(document).ready(function() {

        $('#wizard').smartWizard({
            selected: 0,
            labelNext: 'Next',
            labelPrevious: 'Previous',
            buttonOrder: ['prev', 'next'],
        });

        $('.buttonNext').addClass('btn btn-warning pull-right');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').css('display', 'none');

        $('.demo2').colorpicker();


        $('#search').multiselect({
            search: {
                left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
                right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            },
            fireSearch: function(value) {
                return value.length > 1;
            }
        });

        $(".select2_single").select2({
            placeholder: "Select a Course",
            allowClear: true,
        });


        $('#date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        //Modal in a Modal
        function modalInModal() {
            $('.modal-in-modal').on('hidden.bs.modal', function() {
                $('body').addClass('modal-open');
            });

            //Keeping modal-open class in body if any modal is open
            $(document).on('hidden.bs.modal', function(event) {
                if ($('.modal:visible').length > 0) {
                    $('body').addClass('modal-open');
                }
            });

        }

        modalInModal();


        $(document).on('click', '.selectStudent', function() {
            alert('Action');
        });


    });
</script>

<script>
    $(document).ready(function() {

        //change text color in td
        tdSwitchColor();
        $('table tr td').on("DOMSubtreeModified", function() {
            tdSwitchColor();
        });

    });


    function tdSwitchColor() {
        $('table tbody').find('td.dueContable').each(function() {
            $this = $(this);
            var growthVal = $(this).text().replace('$', '');
            if (growthVal > 0) {
                //alert( growthVal );
                $this.closest('.dueContableCon').addClass('hasDue');
            }
        });
    }

    $('.change-password-modal,.add-user-modal,.add-role-modal,.add-faq-modal').on('hidden.bs.modal', function () {
        $('.modal-body').find('input').val('');
        $('.modal-body').find('textarea').val('');
        $('input:checkbox').removeAttr('checked');
        $( "label.error" ).hide();

        $( "input.valid" ).css( "color", "#555" );

        
    });

</script>