@extends('partials.master')
@section('content')
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Faq</h3>
            </div>
            @permission(['create_faq','super_permission'])
            <div class="title_right">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-faq-modal"><i class="fa fa-folder-open"></i> Create New Faq </button>
            </div>
            @endpermission
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table id="faqtable" class="table table-striped jambo_table bulk_action contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl No</th>
                            <th class="column-title">Question</th>
                            <th class="column-title">Answer</th>
                            @permission(['edit_faq','super_permission','delete_faq'])
                                <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                            </th>
                            @endpermission
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($faq as $rowfaq)
                            <tr class="even pointer">
                                <td class="">
                                    {{ $loop->index + 1}}
                                </td>

                                <td class="">
                                    {{ $rowfaq->question }}
                                </td>

                                <td class="">
                                    {{ $rowfaq->answer }}
                                </td>


                                <td class=" last">

                                    @permission(['edit_faq','super_permission'])
                                        <a  onClick="editFaqModal({{ $rowfaq->id }});" class="btn btn-warning btn-xs">Edit</a>
                                    @endpermission

                                    @permission(['delete_faq','super_permission'])
                                        <button type="button" class="btn btn-danger btn-xs" onClick="deleteFaqModal({{ $rowfaq->id }});">Delete</button>
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete Faq</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->
@include('includes.faqModal')
@include('includes.editFaqModal')

@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#faqtable').DataTable();
</script>

<script type="text/javascript">
    @if (count($errors) > 0)
        $('.add-faq-modal').modal('show');
    @endif
</script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var $form = $("#faq-create"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                question: {
                    required: true
                },
                answer:{
                    required: true
                }
            },
            messages: {
                question: {
                    required: "Please specify faq question"
                },
                answer: {
                    required: "Please specify faq answer"
                }

            },


            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });

        // edit faq

        var $form = $("#faq-edit"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                edit_question: {
                    required: true
                },
                edit_answer:{
                    required: true
                }
            },
            messages: {
                edit_question: {
                    required: "Please specify faq question"
                },
                edit_answer: {
                    required: "Please specify faq answer"
                }

            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });


    });

</script>

@stop