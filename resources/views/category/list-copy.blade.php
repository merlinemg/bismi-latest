@extends('partials.master')
@section('content')
<div class="right_col" role="main">
<!--  <div class="alert alert-success" style="display: none;"></div> -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Sales Category</h3>
            </div>
            @permission(['create_category','super_permission'])
            <!-- <div class="title_right">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-category-modal"><i class="fa fa-folder-open"></i> Create New Category </button>
            </div> -->
            @endpermission
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table id="categorytable" class="table table-striped jambo_table bulk_action contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl No</th>
                            <th class="column-title">Name</th>
                            <th class="column-title">Description</th>
                            <th class="column-title">Percentage</th>
                            @permission(['edit_category','super_permission'])
                                <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span></th>
                            @endpermission
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($category as $rowcategory)
                            <tr class="even pointer">
                                <td class="">
                                    {{ $loop->index + 1}}
                                </td>

                                <td class="">
                                    {{ $rowcategory->name }}
                                </td>

                                <td class=" ">
                                    {{ $rowcategory->description }}
                                </td>

                                 <td class=" ">
                                    {{ $rowcategory->fixed_percentage }}%
                                </td>

                                @permission(['edit_category','super_permission'])
                                <td class=" last">
                                    <a  onClick="editCategoryModal({{ $rowcategory->id }});" class="btn btn-warning btn-xs">Edit</a>

                                   <!--  <button type="button" class="btn btn-danger btn-xs" onClick="deleteCategoryModal({{ $rowcategory->id }});">Delete</button> -->
                                </td>
                                @endpermission
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete Category</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->
@include('includes.categoryModal')
@include('includes.editCategoryModal')

@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#categorytable').DataTable();
</script>

<script type="text/javascript">
    @if (count($errors) > 0)
        $('.add-category-modal').modal('show');
    @endif
</script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var $form = $("#category-create"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                name: {
                    required: true,
                    letters: true
                },
                percentage:{
                    maxlength: 5,
                    number: true
                },
            },
            messages: {
                name: {
                    required: "Please specify category name (only letters and spaces are allowed)"
                },
                percentage: {
                    number: "Please enter a valid number",
                    maxlength: "Atmost {0} numbers only!"
                },

            },


            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });

        // edit category

        var $form = $("#category-edit"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                edit_name: {
                    required: true,
                    letters: true
                }
            },
            messages: {
                edit_name: {
                    required: "Please specify role name (only letters and spaces are allowed)"
                }

            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });


    });

</script>

@stop