@extends('partials.master')
@section('content')
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <!-- <div class="title_left">
                <h3>Vat Settings</h3>
            </div> -->
            
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">
                    <h2>Vat Settings<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                </div>
                

                <form id="settings-update" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('settings.update') }}" >
                {{ csrf_field() }}
                <div class="modal-body">

                @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Settings Name </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ $settings->name }}" readonly="">

                                         <input type="hidden" class="form-control" id="settings_id" name="settings_id" placeholder="" value="{{ $settings->id }}" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="role_desc" class="control-label col-md-3 col-sm-3 col-xs-12"> Percentage <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="percentage" name="percentage" placeholder="5%" value="{{ $settings->percentage }}" >
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label for="short-name" class="control-label col-md-3 col-sm-3 col-xs-12">Price in AED <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="50AED" value="{{ $settings->amount }}">
                                    </div>
                                </div> -->

                            </div>


                        </div>
                        <button type="submit" class="btn btn-warning" style="float:right;margin-right: 260px;" name="btn-submit">Update</button>
                    </div>
                </div>

               

            </form>

            </div>

            </div>
        </div>
    </div>
</div>


</div>


@endsection

@section('page_scripts')

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var $form = $("#settings-update"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                percentage: {
                    required: true,
                    number: true,
                    maxlength:2
                }
            },
            messages: {
                percentage: {
                    required: "Please specify percentage",
                    number: "Please enter a valid number",
                    maxlength:"Please enter maximum 2 numbers"
                }

            },


            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });

    });

</script>
@stop

