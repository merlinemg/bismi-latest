@extends('partials.master')
@section('content')
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
           <!--  <div class="title_left">
                <h3>Base Price Prefix Settings</h3>
            </div> -->
            
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">
                    <h2>Base Price Prefix Settings</h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                </div>
                

                <form id="settings-update" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('settings.prefixUpdate') }}" >
                {{ csrf_field() }}
                <div class="modal-body">

                @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Settings Name </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ $settings->name }}" readonly="">

                                         <input type="hidden" class="form-control" id="settings_id" name="settings_id" placeholder="" value="{{ $settings->id }}" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="role_desc" class="control-label col-md-3 col-sm-3 col-xs-12"> Value <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="value" name="value" placeholder="" value="{{ $settings->value }}" >
                                    </div>
                                </div>

                            </div>


                        </div>
                        <button type="submit" class="btn btn-warning" style="float:right;margin-right: 260px;" name="btn-submit">Update</button>
                    </div>
                </div>

               

            </form>

            </div>

            </div>
        </div>
    </div>
</div>


</div>


@endsection

@section('page_scripts')

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var $form = $("#settings-update"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                value: {
                    required: true
                }
            },
            messages: {
                value: {
                    required: "Please specify value"
                }

            },


            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });

    });

</script>
@stop

