@extends('partials.master')
@section('content')



<div class="loader-page"></div>

        <!-- page content -->
        <div class="right_col" role="main">


            <div id="msg" class="alert alert-success" style="display: none;"></div>

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>List Product</h3>
              </div>
              
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                    <form method="GET" id="frm_filter" name="frm_filter" action="{{ route('product.sort') }}">

                     {{ csrf_field() }}

                     

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">

                            <div class="form-group">
                              
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label for="middle-name" >Department</label>
                                    <select class="form-control" id="department" name="department" onchange="sortOrder('department');">
                                        <option value="">--Department--</option>
                                        @foreach($department as $rowDepartment)
                                            <option  value="{{ $rowDepartment->id }}" {{ ($department_id==$rowDepartment->id)?'selected':'' }}>{{ $rowDepartment->department_name }} </option>
                                        @endforeach
                                        
                                       
                                    </select>
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label for="middle-name" >Category</label>
                                    <select class="form-control" id="category" name="category" onchange="sortOrder('category');">
                                        <option value="">--Category--</option>
                                        @foreach($category as $rowCat)
                                            <option value="{{ $rowCat->id }}" {{ ($category_id==$rowCat->id)?'selected':'' }}>{{ $rowCat->category_name }} </option>
                                        @endforeach
                                       
                                    </select>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <label for="middle-name" >SubCategory</label>
                                    <select class="form-control" id="subcategory" name="subcategory" onchange="sortOrder('subcategory');">
                                        <option value="">--SubCategory--</option>
                                        @foreach($subcategory as $rowSubCat)
                                            <option value="{{ $rowSubCat->id }}" {{ ($subcategory_id==$rowSubCat->id)?'selected':'' }}>{{ $rowSubCat->subcategory_name }} </option>
                                        @endforeach
                                       
                                    </select>
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label for="middle-name" >Brand</label>
                                    <select class="form-control" id="brand" name="brand" onchange="sortOrder('brand');">
                                        <option value="">--Brand--</option>
                                        @foreach($brandProduct as $rowBrand)
                                            <option value="{{ $rowBrand->brand_name }}" {{ ($brand_id==$rowBrand->brand_name)?'selected':'' }}>{{ $rowBrand->brand_name }} </option>
                                        @endforeach
                                       
                                    </select>
                                </div>


                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label for="middle-name" >Featured Status</label>
                                    <select class="form-control" id="featured_status" name="featured_status" onchange="sortOrder('featured');">
                                        <option value="">--Status--</option>
                                        <option value="1" {{ ($featured_status=='1')?'selected':'' }}>Featured</option>
                                        <option value="2" {{ ($featured_status=='2')?'selected':'' }}>Not Featured</option>
                                       
                                    </select>
                                </div>
                                
                                

                            </div>



                        </div>

                        

                    </form>

                    </div>
                </div>
            </div>
             
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Products<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
            
                    <div class="table-responsive">
                      <table  id="categorytable" class="table table-striped jambo_table bulk_action contraList centerList">
                        <thead>
                          <tr class="headings">
                            <th style="width: 40px;">
                              <input type="checkbox" id="check-all" class="flat">
                            </th>

                            <th class="column-title">Sl No</th>
                            <th class="column-title">Product code</th>
                            <th class="column-title">Product name</th>
                            <!-- <th class="column-title">Sales category</th> -->
                            <th class="column-title">Category</th>
                            <th class="column-title">Sub category</th>
                            <th class="column-title">Department</th>
                            <th class="column-title">Brand</th>
                            <th class="column-title">Featured</th>
                            @permission(['edit_product','super_permission'])
                                <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                                </th>
                            @endpermission
                            <th class="bulk-actions" colspan="11">
                                <div class="dropdown">
                                  <a class="dropdown-toggle" style="color:#fff; font-weight:500;" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="true">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                  <ul id="menu6" class="dropdown-menu animated fadeInDown featured_status" role="menu">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#">Featured</a>
                                        <a role="menuitem" tabindex="1" href="#">Non Featured</a>
                                        
                                    </li>
                                  </ul>
                                </div>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                         @foreach($product as $rowproduct)
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records" id="featured_value[]" value="{{ $rowproduct->product_code }}">
                            </td>
                            <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>
                           
                                <td class="">
                                    {{ $rowproduct->product_code }}
                                </td>

                                <td class="">
                                    {{ $rowproduct->product_name }}
                                </td>
                              <!--   <td class="">
                                    {{ $rowproduct->name }}
                                </td> -->
                                <td class="">
                                    {{ $rowproduct->product_category }}
                                </td>
                                <td class="">
                                    {{ $rowproduct->subcategory_name }}
                                </td>
                                 <td class="">
                                    {{ $rowproduct->department_name }}
                                </td>
                                <td class="">
                                    {{ $rowproduct->brand }}
                                </td>
                                <td>
                                    @if($rowproduct->featured_status=='1')
                                        <input type="button" onclick="setFeatured({{ $rowproduct->product_code }},'0')" name="featured_status" class="btn btn-primary btn-xs" value="Featured">
                                    @else
                                        <input type="button" onclick="setFeatured({{ $rowproduct->product_code }},'1')" name="featured_status" class="btn btn-danger btn-xs" value="Not Featured">
                                    @endif
                                </td>

                                @permission(['edit_product','super_permission'])

                                    <td class=" last">
                                        <a  onClick="editProductModal({{ $rowproduct->product_code }});" class="btn btn-warning btn-xs">Edit</a>
                                    </td>
                                @endpermission
                           
                          </tr>
                          @endforeach
                          
                        </tbody>
                      </table>

                      <div class="dataTables_paginate paging_simple_numbers" id="categorytable_paginate">

                        @php
                            $currentUrl = url()->current();

                        @endphp

                        <input type="hidden" name="current-url" id="current-url" value="{{ $currentUrl }}">


                        @if ($product->hasPages())

                        
               
                            @php 
                             $url = \Request::route()->getName();

                             if($url=='product.sort'){

                                $sortUrl = '&department='.$department_id.'&category='.$category_id.'&subcategory='.$subcategory_id.'&brand='.$brand_id.'&featured_status='.$featured_status;
                             } 
                             else{
                                $sortUrl = '';
                             }
                        @endphp
                      
                      
                            <ul class="pagination">
                                {{-- Previous Page Link --}}
                                @if ($product->onFirstPage())
                                    <li class="disabled"><span>Previous</span></li>
                                @else
                                    <li><a  href="{{ $product->previousPageUrl() .$sortUrl  }}" rel="prev">Previous</a></li>
                                @endif

                                @if($product->currentPage() > 3)
                                    <li class="hidden-xs"><a href="{{ $product->url(1).$sortUrl }}">1</a></li>
                                @endif
                                @if($product->currentPage() > 4)
                                    <li class="disabled hidden-xs"><span>...</span></li>
                                @endif
                                @foreach(range(1, $product->lastPage()) as $i)
                                    @if($i >= $product->currentPage() - 2 && $i <= $product->currentPage() + 2)
                                        @if ($i == $product->currentPage())

                                            <li class="active"><!-- class="active" --><span>{{ $i }}</span></li>
                                        @else
                                            <li><a href="{{ $product->url($i). $sortUrl }}">{{ $i }}</a></li>
                                        @endif
                                    @endif
                                @endforeach
                                @if($product->currentPage() < $product->lastPage() - 3)
                                    <li class="disabled hidden-xs"><span>...</span></li>
                                @endif
                                @if($product->currentPage() < $product->lastPage() - 2)
                                    <li class="hidden-xs"><a href="{{ $product->url($product->lastPage()).$sortUrl }}">{{ $product->lastPage() }}</a></li>
                                @endif

                                {{-- Next Page Link --}}
                                @if ($product->hasMorePages())
                                    <li><a href="{{ $product->nextPageUrl().$sortUrl }}" rel="next">Next</a></li>
                                @else
                                    <li class="disabled"><span>Next</span></li>
                                @endif
                            </ul>
                        @endif

                      </div>

                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<div class="modal fade edit-product-modal" role="dialog" data-backdrop="static" aria-hidden="true">

</div>
        


@endsection

@section('page_scripts')



<script type="text/javascript">

$(window).load(function() {
    $(".loader-page").fadeOut("slow");
});

     $('#categorytable').DataTable( {      
         "searching": true,
         "paging": false, 
         "info": false,         
         "lengthChange":false 
    } );

</script>

<script type="text/javascript">
    $('#producttable').DataTable();

    function setFeatured(id,val){

        // var department = $("#department").val();
        // var category = $("#category").val();
        // var subcategory = $("#subcategory").val();
        // var brand = $("#brand").val();
        // var featured_status = $("#featured_status").val();
        

        if(val=='1'){
            var msg = "Featured Product Set Succesfully";
        }
        else{
             var msg = "Non Featured Product Set Succesfully";
        }

        jQuery.ajax({
        type: 'POST',
        url: base_url + '/product/setFeatures',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id,
            'status': val
        },
        success: function(res) {
            location.reload();
            
            // $('#msg').show();
            // $('.alert-success').html(msg);
            // setTimeout(function() {
            //     $('.alert-success').fadeOut('fast');
            //    location.reload();
            // }, 1000);
        }
    });
    }

    $(document).ready(function() 
    {
       $('ul.featured_status li a').click(function(e) 
       { 
            var type = $(this).text();
            var category_id = [];
            $.each($("input[name='table_records']:checked"), function(){            
                category_id.push($(this).val());
            });

            if(type=='Featured'){
                var status = 1;
                var msg = "Featured Product Set Succesfully";
            }
            else{
                 var status = 0;
                 var msg = "Non Featured Product Set Succesfully";
            }

            jQuery.ajax({
                type: 'POST',
                url: base_url + '/product/setFeaturesBulk',
                dataType: 'json',
                data: {
                    '_token': CSRF_TOKEN,
                    'id': category_id,
                    'status': status
                },
                success: function(res) {
                    location.reload();
                    // $('#msg').show();
                    // $('.alert-success').html(msg);
                   
                    // setTimeout(function() {
                    //     $('.alert-success').fadeOut('fast');
                    //      location.reload();
                    // }, 1000);
                }
            });


            
       });
    });

    function sortOrder(type){
        
        if(type=='department'){
            $('#category').val('');
            $('#subcategory').val('');
            $('#brand').val('');
        }

        if(type=='category'){
            $('#subcategory').val('');
            $('#brand').val('');
        }
        $("#frm_filter").submit();
    }

    var currentURL = $("#current-url").val();

    if(typeof window.history.pushState == 'function') {
        window.history.pushState({}, "Hide", currentURL);
    }

</script>


@stop