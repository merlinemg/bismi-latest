@extends('partials.master')
@section('content')
<div class="right_col" role="main">
<!--  <div class="alert alert-success" style="display: none;"></div> -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Category</h3>
            </div>
           
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table id="categorytable" class="table table-striped jambo_table bulk_action contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl No</th>
                            <th class="column-title">Name</th>
                            <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Featured</span></th>

                        </tr>
                    </thead>

                    <tbody>
                        @foreach($category as $rowcategory)
                            <tr class="even pointer">
                                <td class="">
                                    {{ $loop->index + 1}}
                                </td>

                                <td class="">
                                    {{ $rowcategory->category_name }}
                                </td>

                                <td>
                                    @if($rowcategory->featured_status=='1')
                                        <input type="button" onclick="setFeatured({{ $rowcategory->id }},'0')" name="featured_status" class="btn btn-primary btn-xs" value="Featured">
                                    @else
                                        <input type="button" onclick="setFeatured({{ $rowcategory->id }},'1')" name="featured_status" class="btn btn-danger btn-xs" value="Not Featured">
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#categorytable').DataTable();
</script>

<script type="text/javascript">
    $('#producttable').DataTable();

    function setFeatured(id,val){
        jQuery.ajax({
        type: 'POST',
        url: base_url + '/category/setFeatures',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id,
            'status': val
        },
        success: function(res) {
            location.reload();
        }
    });
    }

</script>


@stop