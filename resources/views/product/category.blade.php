@extends('partials.master')
@section('content')

<div class="loader-page"></div>



        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>List Category</h3>
              </div>
              
            </div>
            
            <div class="clearfix"></div>
             
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Category<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
            
                   <!--  <div class="table-responsive"> -->
                      <table  id="categorytable" class="table table-striped jambo_table bulk_action contraList centerList">
                        <thead>
                          <tr class="headings">
                            <th style="width: 40px;">
                              <input type="checkbox" id="check-all" class="flat">
                            </th>

                            <th class="column-title">Sl No</th>
                            <th class="column-title">Name</th>
                            <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Featured</span></th>

                            </th>
                            <th class="bulk-actions" colspan="11">
                                <div class="dropdown">
                                  <a class="dropdown-toggle" style="color:#fff; font-weight:500;" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="true">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                  <ul id="menu6" class="dropdown-menu animated fadeInDown featured_status" role="menu">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#">Featured</a>
                                        <a role="menuitem" tabindex="1" href="#">Non Featured</a>
                                        
                                    </li>
                                  </ul>
                                </div>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                        @foreach($category as $rowcategory)
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records" id="featured_value[]" value="{{ $rowcategory->id }}">
                            </td>
                            <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>
                            <td class=" ">{{ $rowcategory->category_name }}</td>
                            
                            <td class=" last">@if($rowcategory->featured_status=='1')
                                        <input type="button" onclick="setFeatured({{ $rowcategory->id }},'0')" name="featured_status" class="btn btn-primary btn-xs" value="Featured">
                                    @else
                                        <input type="button" onclick="setFeatured({{ $rowcategory->id }},'1')" name="featured_status" class="btn btn-danger btn-xs" value="Not Featured">
                                    @endif
                                </td>
                                </td>
                           
                          </tr>
                          @endforeach
                          
                        </tbody>
                      </table>
                      
                        
                    <!-- </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        


@endsection

@section('page_scripts')

<script type="text/javascript">

  $(window).load(function() {
      $(".loader-page").fadeOut("slow");
  });
    $('#categorytable').DataTable();
</script>

<script type="text/javascript">
    $('#producttable').DataTable();

    function setFeatured(id,val){
        jQuery.ajax({
        type: 'POST',
        url: base_url + '/category/setFeatures',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id,
            'status': val
        },
        success: function(res) {
            location.reload();
        }
    });
    }

    $(document).ready(function() 
    {
       $('ul.featured_status li a').click(function(e) 
       { 
            var type = $(this).text();
            var category_id = [];
            $.each($("input[name='table_records']:checked"), function(){            
                category_id.push($(this).val());
            });

            if(type=='Featured'){
                var status = 1;
            }
            else{
                 var status = 0;
            }

            jQuery.ajax({
                type: 'POST',
                url: base_url + '/category/setFeaturesBulk',
                dataType: 'json',
                data: {
                    '_token': CSRF_TOKEN,
                    'id': category_id,
                    'status': status
                },
                success: function(res) {
                    location.reload();
                }
            });


            
       });
    });

</script>


@stop