@extends('partials.master')
@section('content')



<div class="loader-page"></div>

        <!-- page content -->
        <div class="right_col" role="main">


            <div id="msg" class="alert alert-success" style="display: none;"></div>

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>List Product</h3>
              </div>
              
            </div>
            
            <div class="clearfix"></div>




            <div id="filterDiv">

                   @include('product.productajax') 

            </div>





          </div>
        </div>
        <!-- /page content -->

<div class="modal fade edit-product-modal" role="dialog" data-backdrop="static" aria-hidden="true">

</div>
        


@endsection

@section('page_scripts')



<script type="text/javascript">

$(window).load(function() {
    $(".loader-page").fadeOut("slow");
});

 $('#categorytable').DataTable( {      
     "searching": false,
     "paging": false, 
     "info": false,         
     "lengthChange":false 
} );

</script>

<script type="text/javascript">
    $('#producttable').DataTable();

    function setFeatured(id,val){

        // var department = $("#department").val();
        // var category = $("#category").val();
        // var subcategory = $("#subcategory").val();
        // var brand = $("#brand").val();
        // var featured_status = $("#featured_status").val();
        

        if(val=='1'){
            var msg = "Featured Product Set Succesfully";
        }
        else{
             var msg = "Non Featured Product Set Succesfully";
        }

        jQuery.ajax({
        type: 'POST',
        url: base_url + '/product/setFeatures',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id,
            'status': val
        },
        success: function(res) {
            location.reload();
            
            // $('#msg').show();
            // $('.alert-success').html(msg);
            // setTimeout(function() {
            //     $('.alert-success').fadeOut('fast');
            //    location.reload();
            // }, 1000);
        }
    });
    }

    $(document).ready(function() 
    {

        test();
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                }else{

                    var start = $('.starts').val();
                     var end = $('.ends').val();
                    // start = formatDate(start);
                    // end = formatDate(end);
                    ajaxSort(page);
                }
            }
        });

       $('ul.featured_status li a').click(function(e) 
       { 
            var type = $(this).text();
            var category_id = [];
            $.each($("input[name='table_records']:checked"), function(){            
                category_id.push($(this).val());
            });

            if(type=='Featured'){
                var status = 1;
                var msg = "Featured Product Set Succesfully";
            }
            else{
                 var status = 0;
                 var msg = "Non Featured Product Set Succesfully";
            }

            jQuery.ajax({
                type: 'POST',
                url: base_url + '/product/setFeaturesBulk',
                dataType: 'json',
                data: {
                    '_token': CSRF_TOKEN,
                    'id': category_id,
                    'status': status
                },
                success: function(res) {
                    location.reload();
                    // $('#msg').show();
                    // $('.alert-success').html(msg);
                   
                    // setTimeout(function() {
                    //     $('.alert-success').fadeOut('fast');
                    //      location.reload();
                    // }, 1000);
                }
            });


            
       });
    });


     $(document).on('click', '.pagination a',function(event)
    {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var myurl = $(this).attr('href');
       var page=$(this).attr('href').split('page=')[1];
       //alert(page);
         // var start = $('.starts').val();
         // var end = $('.ends').val();
     //start = formatDate(start);
     //end = formatDate(end);
     //alert(start);
       ajaxSort(page);
    });



    function sortOrder(type){
        
        if(type=='department'){
            $('#category').val('');
            $('#subcategory').val('');
            $('#brand').val('');
        }

        if(type=='category'){
            $('#subcategory').val('');
            $('#brand').val('');
        }

        if(type=='name'){
            $('#search_text').focus();
        }

         var page=1;
         

       ajaxSort(page);

    }

    function ajaxSort(page){

        var category = $('#category').val();
        var subcategory = $('#subcategory').val();
        var brand = $('#brand').val();
        var department = $('#department').val();
        var featured = $('#featured_status').val();
        var name = $('#search_text').val();

        // if(name==""){
            $(".loader-page").show();
        // }
       

        jQuery.ajax({
            type: 'GET',
            url: base_url + '/products?page='+ page+'&department='+department+'&category='+category+'&subcategory='+subcategory+'&brand='+brand+'&featured_status='+featured+'&search_text='+name,
         
            data:$("#frm_filter").serializeArray(),
            datatype: "html",
            
        })
        .done(function(data)
        {
            console.log(data);
            $(".loader-page").fadeOut("slow");

            $("#filterDiv").empty().html(data);
            test();

            if(name!=""){
                $('#search_text').focus();
                $('#clear_click').show();
                $('#search_text').setCursorToTextEnd();
            }

            document.documentElement.scrollTop = 55;
           // location.hash = page;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
    }

    (function($){
        $.fn.setCursorToTextEnd = function() {
            $initialVal = this.val();
            this.val($initialVal + ' ');
            this.val($initialVal);
        };
    })(jQuery);

     

    var currentURL = $("#current-url").val();

    if(typeof window.history.pushState == 'function') {
        window.history.pushState({}, "Hide", currentURL);
    }


    function test(){


     $('#search_text').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $('#sort_click').click();//Trigger search button click event
        }
    });

    }



    function isNumberKey(evt)
    {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
         return false;

      return true;
    }

    function clearSearch(){
        $("#search_text").val('');
        sortOrder('name');
    }

</script>


@stop