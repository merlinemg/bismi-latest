@extends('partials.master')
@section('content')

<div class="loader-page"></div>
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Orders</h3>
            </div>
           
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row showTop">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Salesman<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                  </div>

            <form method="post" id="frm_export" name="frm_export" action="{{ route('order.report') }}">

             {{ csrf_field() }}

                 <input type="hidden" name="customer_id" value="{{ $customer_id}}">
                 <input type="hidden" name="salesman_id" value="{{ $salesman_id}}">
                 <input type="hidden" name="outlet_id" value="{{ $outlet_id}}">
                 <input type="hidden" name="from_date_order" value="{{ $from_date}}">
                 <input type="hidden" name="to_date_order" value="{{ $to_date}}">
                 <input type="submit" name="" style="float: right" class="btn btn-sm btn-primary" value="EXPORT as CSV">
                 
             </form>

            <form method="post" id="frm_filter" name="frm_filter" action="{{ route('order.sort') }}">

             {{ csrf_field() }}

             

                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">

                    <div class="form-group">
                      
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" id="customer" name="customer" onchange="sortOrder('customer');">
                                <option value="">--Customer--</option>
                                @foreach($customer as $rowCustomer)
                                    <option value="{{ $rowCustomer->id}}" {{ ($customer_id==$rowCustomer->id)?'selected':'' }}>{{ $rowCustomer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" id="salesman" name="salesman" onchange="sortOrder('salesman');">
                                <option value="">--Salesman--</option>
                                @foreach($employee as $rowSalesman)
                                    <option value="{{ $rowSalesman->id}}" {{ ($salesman_id==$rowSalesman->id)?'selected':'' }}>{{ $rowSalesman->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" id="outlet" name="outlet" onchange="sortOrder('outlet');">
                                <option value="">--Outlet--</option>
                                @foreach($outlet as $rowOutlet)
                                    <option value="{{ $rowOutlet->id}}" {{ ($outlet_id==$rowOutlet->id)?'selected':'' }}>{{ $rowOutlet->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>



                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                        <div class="form-group">

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="middle-name" >From Date</label>
                                    <input type="date" name="from_date" id="from_date" class="form-control" placeholder="From Date" value="{{ $from_date }}" onchange="sortOrder('date');">

                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">
                            <label for="middle-name" >To Date</label>
                                    <input type="date" name="to_date" id="to_date" class="form-control" placeholder="To Date" value="{{ $to_date }}" onchange="sortOrder('date');">

                            </div>
                        </div>
                </div>

            </form>

            </div>



            <div class="x_panel">
            

                <table id="ordertable" class="table table-striped jambo_table bulk_action contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl No</th>
                            <th class="column-title">Order ID(Spericorn)</th>
                            <th class="column-title">Order ID(Fact)</th>
                            <th class="column-title">Customer</th>
                            <th class="column-title">Salesman</th>
                            <th class="column-title">Outlet</th>
                            <th class="column-title">Order Date</th>
                            <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                            </th>
                           
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($order as $rowOrder)
                            <tr class="even pointer">
                                 <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>

                                <td class="">
                                    {{ $rowOrder->order_unique_code }}
                                </td>

                                <td class="">
                                    FACT{{ $rowOrder->order_id }}
                                </td>
                                <td class="">
                                    {{ $rowOrder->customer_name }}
                                </td>
                                <td class="">
                                    {{ $rowOrder->employee_name }}
                                </td>
                                <td class="">
                                    {{ $rowOrder->outlet_name }}
                                </td>
                                 <td class="">
                                    {{  date('d-M-Y', strtotime($rowOrder->created_at)) }}
                                </td>

                                <td class=" last">
                                  
                                    <a  href="{{ route('order.detail',$rowOrder->order_unique_code) }}" class="btn btn-primary btn-xs" >VIEW</a>
                                  
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('page_scripts')

<script type="text/javascript">
    var table = $('#ordertable').DataTable();
    $(window).load(function() {
        $(".loader-page").fadeOut("slow");
    });


        table.on('page.dt', function() {
          $('html, body').animate({
            scrollTop: $(".showTop").offset().top
           }, 'slow');
        });





</script>

<script type="text/javascript">
    function sortOrder(type){
        $("#frm_filter").submit();
    }

</script>


@stop