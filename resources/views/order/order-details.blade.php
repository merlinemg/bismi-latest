@extends('partials.master')
@section('content')
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Orders Details - {{ $order->order_unique_code}}</h3>
            </div>
           
        </div>

        <div class="clearfix"></div><br><br>

            
            <form id="order-record" class="form-horizontal form-label-left" role="form" method="POST"  >
                 {{ csrf_field() }}
                <div class="modal-body">

                 

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                           
                                <br />

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Spericorn Order ID </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->order_unique_code}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fact Order ID </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->order_id}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Customer Name </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->customer_name}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Salesman Name </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->employee_name}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Outlet Name </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->outlet_name}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sales Category Name </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->cat_name}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Payment Mode </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ $order->payment_mode}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Discount Amount </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="AED {{ $order->discount_amount}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Vat Amount </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="AED {{ $order->vat_amount}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Total Amount </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="AED {{ $order->order_total_amount}}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Order Date </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="{{ date('d-M-Y', strtotime($order->created_at))}}" readonly="">
                                        </div>
                                    </div>


                                    <table id="ordertable" class="table table-striped jambo_table bulk_action contraList centerList">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">Sl No</th>
                                            <th class="column-title">Product Name</th>
                                            <th class="column-title">Quantity</th>
                                            <th class="column-title">Rate</th>
                                            <th class="column-title">Total Price</th>
                                            <th class="column-title">Package</th>
                                           
                                        </tr>
                                    </thead>

                                    <tbody>
                                       @foreach($product as $row)
                                        <tr class="even pointer">
                                            <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>

                                            <td class="">
                                               {{ $row->product_name }}
                                            </td>

                                            <td class="">
                                                {{ $row->quantity }}
                                            </td>
                                             <td class="">
                                                AED {{ $row->rate }}
                                            </td>
                                             <td class="">
                                                AED {{ $row->total_amount }}
                                            </td>

                                            <td class=" last">
                                              
                                                {{ $row->package }}
                                              
                                            </td>
                                        </tr>
                                    @endforeach 

                                    </tbody>
                                 </table>
                                    
                                   
                                    
                                   
                                   
                                    
                                    


                            
                        </div>
                    </div>
                </div>

                

            </form>


        
    </div>
</div>



@endsection

@section('page_scripts')

<script type="text/javascript">
    //$('#ordertable').DataTable();
</script>





@stop