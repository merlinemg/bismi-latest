<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bismi Whole Sale Point | Login</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
    <!-- Animate.css -->
    <!-- <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet"> -->

    <!-- Custom Theme Style -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  </head>

  <body class="login">

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">

        <div class="loginLogo">
            <img src="{{ asset('img/logo.png') }}">
            <!-- <h1>KIDVERSITY</h1> -->
        </div>


        <div class="animate form login_form">


          <section class="login_content">
            <form role="form" id="signin" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}

              <h1>Login Form</h1>

              @if ($errors->has('email'))
                  <span class="help-block" style="color: red;">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif

              <div>
                 <input id="email" type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Username" required >

              </div>
              <div>
                <input id="password" type="password" name="password" class="form-control" placeholder="Password" required>
              </div>
              <div>
                <!-- <a class="reset_pass pull-left" href="#">Lost your password?</a> -->
                <!-- <a class="btn btn-default" href="">Log in</a> -->

                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-user"></i> Login</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

              </div>
            </form>
          </section>
        </div>

        <!-- <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Reset Password</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <button class="btn btn-primary pull-right submit"><i class="fa fa-user"></i> Register</button>
              </div>

              <div class="clearfix"></div>


            </form>
          </section>
        </div> -->
      </div>

    </div>


    <!-- footer content -->
    <footer>
        <div class="text-center">
            Copyright © 2018 Bismi. All rights reserved.
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->

  </body>
</html>
