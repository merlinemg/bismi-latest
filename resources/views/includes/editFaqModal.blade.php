<div class="modal fade edit-faq-modal" role="dialog" data-backdrop="static" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">New Faq</h3>
            </div>

            <form id="faq-edit" class="form-horizontal form-label-left" role="form" method="post" action="{{ route('faq.update') }}" >
                {{ csrf_field() }}
                <div class="modal-body">

                @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />
                                 <input type="hidden" class="form-control" id="edit_faq_id" name="edit_faq_id" placeholder="">

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Question <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="edit_question" name="edit_question" placeholder="" value="">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="role_desc" class="control-label col-md-3 col-sm-3 col-xs-12"> Answer </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control" id="edit_answer" name="edit_answer" placeholder=""></textarea>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" name="btn-submit">Update</button>

                </div>

            </form>

        </div>
    </div>

</div>