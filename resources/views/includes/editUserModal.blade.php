<div class="modal fade edit-user-modal" role="dialog" data-backdrop="static" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">Edit User Record</h3>
            </div>

            <form id="user-edit" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('user.update')}}"  >
                 {{ csrf_field() }}
                <div class="modal-body">

                 @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />
                                <input type="hidden" class="form-control" id="edit_user_id" name="edit_user_id" placeholder="" value="">

                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> Name <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="edit_user_name" name="edit_user_name" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> Email ID <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="edit_user_email" name="edit_user_email" placeholder="" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Role <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class=" form-control" tabindex="-1" name="edit_role" id="edit_role" >
                                            <option value=""> -- Assign a role -- </option>
                                            @foreach ($roles as $key => $rowRoles)
                                                <option value="{{ $rowRoles['id'] }}">{{ $rowRoles['display_name'] }}</option>
                                            @endforeach
                                        </select>
                                        <div id="roles-error"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" name="btn-submit">Update</button>


                </div>

            </form>


        </div>

    </div>
</div>