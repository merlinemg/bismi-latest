<div class="modal fade add-employee-modal" role="dialog" data-backdrop="static" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">Add New Salesman</h3>
            </div>

            <form id="employee-create" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('employee.create')}}" >
                 {{ csrf_field() }}
                <div class="modal-body">

                 @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Salesman Name <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="employee_name" name="employee_name" placeholder="" value="{{ old('employee_name') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Salesman Email ID <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="employee_email" name="employee_email" placeholder="" value="{{ old('employee_email') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Phone Number <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="" value="{{ old('phone_number') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="radio" id="gender" name="gender" value="male">Male</label>
                                            <label><input type="radio" id="gender" name="gender" value="female">Female</label>
                                            <div id="gender-error"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">DOB </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="date" class="form-control" id="dob" name="dob" value="{{ old('dob') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Designation <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class=" form-control" tabindex="-1" name="designation" id="designation" >
                                                <option value=""> -- Select Designation -- </option>
                                                @foreach ($designations as $designation)
                                                    <option value="{{ $designation['id'] }}">{{ $designation['name'] }}</option>
                                                @endforeach
                                            </select>
                                            <div id="designation-error"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Outlet <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" tabindex="-1" id="outlet" name="outlet[]" multiple="true">
                                                @foreach ($outlets as $key => $rowOutlets)
                                                    <option value="{{ $rowOutlets->id }}"> {{ $rowOutlets->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" name="btn-submit">Create</button>


                </div>

            </form>


        </div>

    </div>

</div>