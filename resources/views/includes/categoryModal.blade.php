<div class="modal fade add-category-modal" role="dialog" data-backdrop="static" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">New Category</h3>
            </div>

            <form id="category-create" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('category.create') }}" >
                {{ csrf_field() }}
                <div class="modal-body">

                @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Category Name <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ old('name') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="role_desc" class="control-label col-md-3 col-sm-3 col-xs-12"> Description </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control" id="description" name="description" placeholder="">{{ old('description') }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="short-name" class="control-label col-md-3 col-sm-3 col-xs-12">Percentage </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="percentage" name="percentage" placeholder="" value="{{ old('percentage') }}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" name="btn-submit">Create</button>

                </div>

            </form>

        </div>
    </div>

</div>