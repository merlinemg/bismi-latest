<div class="modal fade edit-employee-modal" role="dialog" data-backdrop="static" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">Edit Salesman Record</h3>
            </div>

            <form id="employee-edit" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('employee.update')}}"  >
                 {{ csrf_field() }}
                <div class="modal-body">

                 @if (count($errors) > 0)
                  <div class="alert alert-danger">

                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />
                                <input type="hidden" class="form-control" id="edit_employee_id" name="edit_employee_id" placeholder="" value="">

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Salesman Name <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="edit_employee_name" name="edit_employee_name" placeholder="" value="" readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Salesman Code <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="edit_employee_code" name="edit_employee_code" placeholder="" value="" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Salesman Email ID <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="edit_employee_email" name="edit_employee_email" placeholder="" value="" readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Phone Number <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="edit_phone_number" name="edit_phone_number" placeholder="" value="" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="radio" id="edit_gender_male" name="edit_gender" value="male">Male</label>
                                            <label><input type="radio" id="edit_gender_female" name="edit_gender" value="female">Female</label>
                                            <div id="gender-error"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">DOB </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="date" class="form-control" id="edit_dob" name="edit_dob" placeholder="DD-MM-YYYY" value="" readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Password </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" class="form-control" id="edit_password" name="edit_password" placeholder="Password" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" class="form-control" id="edit_cpassword" name="edit_cpassword" placeholder="Confirm Password" >
                                            <label id="edit_cpassword-error" class="error" for="edit_cpassword"></label>
                                            <label id="edit_mismatched-error" class="error" for="edit_mismatched"></label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="salescategory" class="control-label col-md-3 col-sm-3 col-xs-12">Sales Category <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            <label><input type="checkbox" name="edit_category[]" class="edit_category_id"  value="2"> Retail </label> &nbsp;

                                            <label><input type="checkbox" name="edit_category[]" class="edit_category_id"  value="3"> Semi Retail </label> &nbsp;

                                            <label><input type="checkbox" name="edit_category[]" class="edit_category_id"  value="4"> Whole Sale </label> &nbsp;

                                            <label><input type="checkbox" name="edit_category[]" class="edit_category_id"  value="5"> Semi Whole Sale </label> &nbsp;

                                            <!-- @foreach ($category as $key1 => $rowCategory)
                                                <label><input type="checkbox" name="edit_category[]" class="edit_category"  value="{{ $rowCategory->id }}"> {{ $rowCategory->name }} </label> &nbsp;
                                            @endforeach -->
                                            <br><div id="category_error"></div>
                                        </div>
                                    </div>



                                    
                                        

                                       <!--  <select class="form-control edit_category" multiple="true"  id="edit_category[]" name="edit_category[]"  >
                                            @foreach ($category as $key1 => $rowCategory)

                                                <option value="{{ $rowCategory->id }}"> {{ $rowCategory->name }} </option>
                                            @endforeach
                                        </select> -->

                                           
                                       
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit_outlet">Outlet <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class=" form-control edit_outlet" tabindex="-1"  id="edit_outlet" name="edit_outlet[]" multiple="true" disabled="">
                                                @foreach ($outlets as $key => $rowOutlets)
                                                    <option value="{{ $rowOutlets->id }}"> {{ $rowOutlets->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="bt-editemp" class="btn btn-warning" name="btn-submit">Update</button>


                </div>

            </form>


        </div>

    </div>
</div>