<!-- <div class="modal fade edit-product-modal" role="dialog" data-backdrop="static" aria-hidden="true"> -->

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">Edit Product</h3>
            </div>

            <form id="product-edit-price" class="form-horizontal form-label-left" role="form" method="post" action="{{ route('product.update') }}" >
                {{ csrf_field() }}
                <div class="modal-body">
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                 
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />
                                 <input type="hidden" class="form-control" id="edit_product_id" name="edit_product_id" placeholder="" value="{{ $product_edit->id}}">

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Name </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="@if($product_edit!=""){{ $product_edit->product_name  }}@endif" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Code </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $product_edit->product_code}}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Sales Category </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $product_edit->name  }}" readonly="">
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Category </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $product_edit->product_category  }}" readonly="">
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Subcategory </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $product_edit->subcategory_name  }}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Department </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $product_edit->department_name  }}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control" readonly="">{{ $product_edit->description}}</textarea>
                                        
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Package</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $product_edit->package}}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Brand</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $product_edit->brand}}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Shelf Life</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $product_edit->shelf_life}}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Country Of Origin</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $product_edit->country_of_orgin}}"  readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Overhide</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $product_edit->overhide}}" readonly="">
                                    </div>
                                </div>

                                @foreach($product_price as $rowPrice)
                                
                                 <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">{{ $rowPrice->pricetype_unit}} MSP <span style="color: red">(AED)</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" style="width: 100px; display: inline" class="form-control" readonly="" id="base_price_{{ $rowPrice->pricetype_unit}}" name="base_price[{{ $rowPrice->pricetype_unit}}]"  value="{{ $rowPrice->pricetype_msp}}">
                                        <!-- <b>Selling price</b> -->
                                        <input type="text" id="selling_price_{{ $rowPrice->pricetype_unit}}" name="selling_price[{{ $rowPrice->pricetype_unit}}]" style="margin-left:10px;display: inline; width: 150px;" class="form-control"  value="{{ $rowPrice->selling_price}}">

                                        <input type="hidden" id="selling_price_original_{{ $rowPrice->pricetype_unit}}" name="selling_price_original[{{ $rowPrice->pricetype_unit}}]" class="form-control"  value="{{ $rowPrice->selling_price}}">

                                        <input type="hidden" id="price_id_{{ $rowPrice->pricetype_unit}}" name="price_id[{{ $rowPrice->pricetype_unit}}]" class="form-control"  value="{{ $rowPrice->id}}">
                                    </div>
                                </div>


                                @endforeach

                                <div  class="form-group" >
                                     <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                        <div id="loadingDiv" style="display: none;" class="col-md-6 col-sm-6 col-xs-12">
                                            <img src="{{ asset('img/loader.gif') }}" width="150" height="90">
                                            
                                        </div>
                                        <div id="msgDiv"  style="color: rgba(38, 185, 154, 0.88);font-size:16px;display: none;">

                                                Product Price Updated Successfully
                                        </div>
                                </div>

                                

                                


                            </div>
                        </div>
                    </div>
                </div>

                @if(count($product_price)>0)

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="saveProductPrice({{ $product_edit->id}});"  id="test" class="btn btn-warning" name="btn-submit">Update</button>

                </div>

                @endif

            </form>

        </div>
    </div>

<!-- </div> -->

<!-- onclick="saveProductPrice({{ $product_edit->id}});" -->
