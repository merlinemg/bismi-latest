<!-- <div class="modal fade edit-product-modal" role="dialog" data-backdrop="static" aria-hidden="true"> -->

    <div class="modal-dialog modal-lg-c">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">Edit Product</h3>
            </div>

            <form id="product-edit-prices" class="form-horizontal form-label-left" role="form" method="post" action="{{ route('product.update') }}" >
                {{ csrf_field() }}
                <div class="modal-body">
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                    @endif

                    <!-- Form >> -->
                    <div class="row">
                 
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />
                                 <input type="hidden" class="form-control" id="edit_product_id" name="edit_product_id" placeholder="" value="{{ $productDetail->id}}">

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Name </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="@if($productDetail!=""){{ $productDetail->product_name  }}@endif" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Code </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $productDetail->product_code}}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Sales Category </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $productDetail->name  }}" readonly="">
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Category </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $productDetail->product_category  }}" readonly="">
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Subcategory </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $productDetail->subcategory_name  }}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Department </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"  placeholder="Product Name" value="{{ $productDetail->department_name  }}" readonly="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control" readonly="">{{ $productDetail->description}}</textarea>
                                        
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Brand</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control"   value="{{ $productDetail->brand}}" readonly="">
                                    </div>
                                </div>
                                

                                <div class="container-fluid extra_form_con">
                                    <div class="row">

                                    @foreach($product_edit as $rowPrice)
                                        <div class="col-sm-6">
                                            <div class="panel panel-primary">
                                              <div class="panel-heading">{{ $rowPrice->name}}</div>

                                            @php

                                                $temArr = explode('@@,',$rowPrice->bismiPrice);
                                           
                                            @endphp

                                           

                                              <div class="panel-body">

                                               @foreach($temArr as $rowSingle)

                                                    @php

                                                        $temArrSingle = explode(',',$rowSingle);
                                               
                                                    @endphp
                                                    <div class="row form-group">
                                                        <label for="name" class="control-label col-md-4 col-sm-4 col-xs-12">
                                                           {{ $temArrSingle[1]}} <span style="color: red">(AED)</span>
                                                        </label>
                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                           <input type="text"  class="form-control" readonly="" id="base_price_{{ $temArrSingle[1]}}"   value="{{ $temArrSingle[2]}}">
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                                           <input type="text" maxlength="10" onkeypress="return isNumberKey(event)" id="selling_price[]" name="[{{ $temArrSingle[0]}}]"  class="form-control sellingXyz"  value="{{ $temArrSingle[3]}}" autocomplete="off" required="required">
                                                        </div>
                                                    </div>
                                                    
                                                    

                                                @endforeach

                                              </div>
                                               
                                            </div>
                                        </div>

                                     @endforeach   
                                    </div>
                                    
                                </div>


                                <div  class="form-group" >
                                     <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                        <div id="loadingDiv" style="display: none;" class="col-md-6 col-sm-6 col-xs-12">
                                            <img src="{{ asset('img/loader.gif') }}" width="150" height="90">
                                            
                                        </div>
                                       <!--  <div id="msgDiv"  style="color: rgba(38, 185, 154, 0.88);font-size:16px;display: none;">
 -->
                                                

                                            <div id="msgDiv" style="display: none;"  class="alert alert-success alert-dismissible">
                                                  
                                                  <strong>Success!</strong> Product Price Updated Successfully!
                                            </div>
                                        <!-- </div> -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                @if(count($product_edit)>0)

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="saveProductPrice({{ $productDetail->id}});"  id="test" class="btn btn-warning" name="btn-submit">Update</button>

                </div>

                @endif

            </form>

        </div>
    </div>

<!-- </div> -->


