<div class="modal fade add-role-modal" role="dialog" data-backdrop="static" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-custom">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h3 class="modal-title" id="myModalLabel">New Role</h3>
            </div>

            <form id="role-create" class="form-horizontal form-label-left" role="form" method="POST" action="{{ route('role.create') }}" >
                {{ csrf_field() }}
                <div class="modal-body">

                    <!-- Form >> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_content">
                                <br />

                                <div class="form-group">
                                    <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Role Name <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="role_name" name="role_name" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="short-name" class="control-label col-md-3 col-sm-3 col-xs-12">Role Shortname <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="role_shortname" name="role_shortname" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role_desc" class="control-label col-md-3 col-sm-3 col-xs-12">Role Description <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control" id="role_desc" name="role_desc" placeholder=""></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="permissions" class="control-label col-md-3 col-sm-3 col-xs-12">Permissions <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @foreach($permissions as $permission)
                                            <label><input type="checkbox" name="add_permisssion[]" value="{{ $permission->id }}"> {{ $permission->display_name }} </label><br/>
                                        @endforeach
                                        <br><div id="permission_error"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" name="btn-submit">Create</button>

                </div>

            </form>

        </div>
    </div>

</div>