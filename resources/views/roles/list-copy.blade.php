@extends('partials.master')
@section('content')
<div class="right_col" role="main">
 <div class="alert alert-success" style="display: none;"></div>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Roles</h3>
            </div>
            <div class="title_right">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-role-modal"><i class="fa fa-folder-open"></i> Create New Role </button>
            </div>
        </div>

        <div class="clearfix"></div><br><br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table id="roletable" class="table table-striped jambo_table bulk_action contraList centerList">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Sl No</th>
                            <th class="column-title">Role Name</th>
                            <th class="column-title">Role Shortname</th>
                            <th class="column-title">Description</th>
                            <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($roles as $role)
                            <tr class="even pointer">
                                <td class="">
                                    {{ $loop->index + 1}}
                                </td>

                                <td class="">
                                    {{ $role->name }}
                                </td>

                                <td class="">
                                    {{ $role->display_name }}
                                </td>

                                <td class=" ">
                                    {{ $role->description }}
                                </td>

                                <td class="last">
                                    <button type="button" class="btn btn-warning btn-xs" onClick="editRoleModal({{ $role->id }});">Edit</button>

                                    <button type="button" class="btn btn-danger btn-xs" onClick="deleteRoleModal({{ $role->id }});">Delete</button>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete Role</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->

@include('includes.roleModal')
@include('includes.editRoleModal')
@endsection

@section('page_scripts')

<script type="text/javascript">
    $('#roletable').DataTable();
</script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var $form = $("#role-create"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                role_name: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                role_shortname: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                role_desc: {
                    required: true,
                     maxlength: 50
                },
                'add_permisssion[]': {
                    required: true,
                },
            },
            messages: {
                role_name: {
                    required: "Please specify role name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!",
                    letters:"Only letters and spaces are allowed!"
                },
                role_shortname: {
                    required: "Please specify role short name",
                    letters:"Only letters and spaces are allowed!"
                },
                role_desc: {
                    required: "Please specify role description",
                },
                'add_permisssion[]': {
                    required: "Please assign permissions",
                },

            },

            errorPlacement: function(error, element) {
                if ( element.is(":checkbox") ) {
                    error.prependTo( '#permission_error' );
                }
                else {
                    error.insertAfter( element );
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Role Inserted Successfully");
                return false;
            }
        });

        // edit role

        var $form = $("#role-edit"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                edit_role_name: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                edit_role_shortname: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                edit_role_desc: {
                    required: true,
                    maxlength: 50
                },
                'edit_add_permisssion[]': {
                    required: true,
                },
            },
            messages: {
                edit_role_name: {
                    required: "Please specify role name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!",
                    letters:"Only letters and spaces are allowed!"
                },
                edit_role_shortname: {
                    required: "Please specify role short name",
                    letters:"Only letters and spaces are allowed!"
                },
                edit_role_desc: {
                    required: "Please specify role description",
                },
                'edit_add_permisssion[]': {
                    required: "Please assign permissions",
                },

            },

            errorPlacement: function(error, element) {
                if ( element.is(":checkbox") ) {
                    error.prependTo( '#edit_permission_error' );
                }
                else {
                    error.insertAfter( element );
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Role Inserted Successfully");
                return false;
            }
        });


    });

</script>

@stop