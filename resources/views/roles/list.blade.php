@extends('partials.master')
@section('content')




        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>List Roles</h3>
              </div>
              <div class="title_right">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".add-role-modal"><i class="fa fa-folder-open"></i> Create New Role </button>
            </div>
              
            </div>
            
            <div class="clearfix"></div>
             
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Roles<!-- <small>Showing 50 of 150 Results</small> --></h2>
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                        </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content" id="role_div">
            
                   <!--  <div class="table-responsive"> -->
                      <table  id="categorytable" class="table table-striped jambo_table bulk_action contraList centerList">
                        <thead>
                          <tr class="headings">
                            <th style="width: 40px;">
                              <input type="checkbox" id="check-all" class="flat">
                            </th>

                            <th class="column-title">Sl No</th>
                            <th class="column-title">Role Name</th>
                            <th class="column-title">Role Shortname</th>
                            <th class="column-title">Description</th>
                            <th class="column-title no-link last" style="width: 125px;"><span class="nobr">Action</span>

                            </th>
                            <th class="bulk-actions" colspan="11">
                                <div class="dropdown">
                                  <a class="dropdown-toggle" style="color:#fff; font-weight:500;" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="true">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                  <ul id="menu6" class="dropdown-menu animated fadeInDown featured_status" role="menu">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#">Delete</a>
                                        
                                    </li>
                                  </ul>
                                </div>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                         @foreach($roles as $role)
                          <tr class="even pointer" id="role_{{ $role->id }}">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records" id="featured_value[]" value="{{ $role->id }}">
                            </td>
                            <td class="centerName"><a class="link-primary" data-toggle="modal" data-target=".view-contract">{{ $loop->index + 1}}</a></td>
                            <td class="">
                                    {{ $role->name }}
                                </td>

                                <td class="">
                                    {{ $role->display_name }}
                                </td>

                                <td class=" ">
                                    {{ $role->description }}
                                </td>

                                <td class="last">
                                    <button type="button" class="btn btn-warning btn-xs" onClick="editRoleModal({{ $role->id }});">Edit</button>

                                    <button type="button" class="btn btn-danger btn-xs" onClick="deleteRoleModal({{ $role->id }});">Delete</button>
                                </td>
                           
                          </tr>
                          @endforeach
                          
                        </tbody>
                      </table>
                      
                        
                    <!-- </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- Delete Model Box begin-->

<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="color: #636b6f;" >Delete Role</div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <a id="bt-modal-cancel" href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <a id="bt-modal-confirm" class="btn btn-danger btn-ok">Delete</a>

            </div>
        </div>
    </div>
</div>

<!-- Model Box end-->
        

@include('includes.roleModal')
@include('includes.editRoleModal')
@endsection


@section('page_scripts')

<script type="text/javascript">
    $('#roletable').DataTable();
</script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var $form = $("#role-create"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                role_name: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                role_shortname: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                role_desc: {
                    required: true,
                     maxlength: 50
                },
                'add_permisssion[]': {
                    required: true,
                },
            },
            messages: {
                role_name: {
                    required: "Please specify role name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!",
                    letters:"Only letters and spaces are allowed!"
                },
                role_shortname: {
                    required: "Please specify role short name",
                    letters:"Only letters and spaces are allowed!"
                },
                role_desc: {
                    required: "Please specify role description",
                },
                'add_permisssion[]': {
                    required: "Please assign permissions",
                },

            },

            errorPlacement: function(error, element) {
                if ( element.is(":checkbox") ) {
                    error.prependTo( '#permission_error' );
                }
                else {
                    error.insertAfter( element );
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Role Inserted Successfully");
                return false;
            }
        });

        // edit role

        var $form = $("#role-edit"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                edit_role_name: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                edit_role_shortname: {
                    required: true,
                    letters: true,
                    maxlength: 30
                },
                edit_role_desc: {
                    required: true,
                    maxlength: 50
                },
                'edit_add_permisssion[]': {
                    required: true,
                },
            },
            messages: {
                edit_role_name: {
                    required: "Please specify role name (only letters and spaces are allowed)",
                    minlength: "At least {0} characters required!",
                    letters:"Only letters and spaces are allowed!"
                },
                edit_role_shortname: {
                    required: "Please specify role short name",
                    letters:"Only letters and spaces are allowed!"
                },
                edit_role_desc: {
                    required: "Please specify role description",
                },
                'edit_add_permisssion[]': {
                    required: "Please assign permissions",
                },

            },

            errorPlacement: function(error, element) {
                if ( element.is(":checkbox") ) {
                    error.prependTo( '#edit_permission_error' );
                }
                else {
                    error.insertAfter( element );
                }
            },

            submitHandler: function(form) {
                form.submit();
                //alert("Role Inserted Successfully");
                return false;
            }
        });


    });

$(document).ready(function() 
    {
       $('ul.featured_status li a').click(function(e) 
       { 
            var type = $(this).text();
            var role_id = [];
            $.each($("input[name='table_records']:checked"), function(){            
                role_id.push($(this).val());
            });

            var role_id_remove = [];

             $.each($("input[name='table_records']:checked"), function(){            
                role_id_remove.push("#role_"+$(this).val());
            });
             role_id_remove.toString();
             //alert(role_id_remove);

            jQuery.ajax({
                type: 'POST',
                url: base_url + '/role/deleteBulk',
                dataType: 'json',
                data: {
                    '_token': CSRF_TOKEN,
                    'id': role_id
                },
                success: function(res) {
                    //$(role_id_remove).remove();
                    $('.alert-success').show();
                    $('.alert-success').html('Role Deleted Successfully');
                    setTimeout(function() {
                        location.reload();
                        $('.alert-success').fadeOut('fast');
                    }, 2000);
                    
                }
            });


            
       });
    });

</script>

@stop