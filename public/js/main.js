$( document ).ready(function() {
    setTimeout(function() {
       $('.alert-success').fadeOut('fast');
       $('.alert-danger').fadeOut('fast');
    }, 2000);

    var $form = $("#change-pwd"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                curPassword: {
                    required: true,
                    minlength:5
                },
                newPassword: {
                    required: true,
                    minlength:5
                },
                retypePassword: {
                    required: true,
                    minlength:5,
                    equalTo: "#newPassword"
                }
            },
            messages: {
                curPassword: {
                    required: "Please specify current password",
                    minlength: "At least {0} characters required!"
                },
                newPassword: {
                    required: "Please specify new password",
                    minlength: "At least {0} characters required!"
                },
                retypePassword: {
                    required: "Please re-enter password",
                    minlength: "At least {0} characters required!",
                    equalTo: "Both password should match"
                }

            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });


        // $("#test").click(function(){
        //     alert(1);
        // });
});


// USER *******************
function editUserModal(id) {

    jQuery.ajax({
        type: 'POST',
        url: base_url + '/user/edit',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            var jsonUser = JSON.stringify(res['user']);
            var jsonUserdata = JSON.parse(jsonUser);

            var jsonRole = JSON.stringify(res['role']);
            var jsonRoledata = JSON.parse(jsonRole);

            $(".alert-danger").hide();

            $('#edit_user_id').val(jsonUserdata.id);
            $('#edit_user_name').val(jsonUserdata.name);
            $('#edit_user_email').val(jsonUserdata.email);

            $.each(jsonRoledata, function(idx, obj) {
                $("#edit_role option[value='" + obj.id + "']").prop("selected", true);
             });

            $('.edit-user-modal').modal('show');

        }
    });
}

function deleteUserModal(id) {
    var $myModal = jQuery('#my-modal');
    var $dsBody = $myModal.find('div.modal-body');
    var $dsTitle = $myModal.find('div.modal-header');
    var $btConfirm = jQuery('#bt-modal-confirm');
    var $btCancel = jQuery('#bt-modal-cancel');

    $dsBody.html('<h5><center>Are you sure you want to delete this user record ?</center></h5>');
    $dsTitle.html('<h4>Delete User Record</h4>');
    $myModal.modal({
        show: true
    });

    $btConfirm.click(function() {
        deleteUser(id);
    });

    $btCancel.click(function() {
        $dsTitle.html('Warning');
        $dsBody.html('<p>Notice</p>');
        $btConfirm.attr('href', '#').attr('data-dismiss', 'modal');
    });
}

function deleteUser(id) {
    jQuery.ajax({
        type: 'POST',
        url: base_url + '/user/delete',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            //console.log(res);
            if (res.status == 'success') {
                $('#my-modal').modal('hide');
                $("#"+id).remove();
                $('#msg').show();
                $('.alert-success').html('User Deleted Successfully');
                setTimeout(function() {
                    $('.alert-success').fadeOut('fast');
                   // location.reload();
                }, 2000);
            }
        }
    });
}
// end USER ****************


// EMPLOYEE *****************
function editEmployeeModal(id) {

    jQuery.ajax({
        type: 'POST',
        url: base_url + '/employee/edit',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            var json = JSON.stringify(res['employee']);
            var jsondata = JSON.parse(json);

            var jsonOutlet = JSON.stringify(res['outlets']);
            var jsonOutletdata = JSON.parse(jsonOutlet);

            //var jsonCategory = JSON.stringify(res['salesCategory']);

            //var jsonCategorydata = new Array();
           // var jsonCategorydata = JSON.parse(jsonCategory);

            $(".alert-danger").hide();
            $('#employee-edit')[0].reset();

            $('#edit_employee_id').val(jsondata.id);
            $('#edit_employee_name').val(jsondata.name);
            $('#edit_employee_code').val(jsondata.emp_id);
            $('#edit_employee_email').val(jsondata.email);
            $('#edit_phone_number').val(jsondata.phone_number);
            $('#edit_dob').val(jsondata.dob);

            if (jsondata.gender == 'Male') {
                $("#edit_gender_male").attr('checked', 'checked');
            } else {
                $("#edit_gender_female").attr('checked', 'checked');
            }


            $.each(jsonOutletdata, function(idx, obj) {
                $(".edit_outlet option[value='" + obj.id + "']").prop("selected", true);
             });


            var json_permission = JSON.stringify(res['salesCategory']);
            var jsondataPermission = JSON.parse(json_permission);
            var permission_array = new Array();
            $.each(jsondataPermission, function(idx, obj) {
                permission_array.push(obj.sales_category_id);

             });

            // $.each(permission_array, function(i, val){
            //     $("input[value='" + val + "']").prop('checked', true);
            // });



           //  console.log(permission_array);
            $.each(permission_array, function(i, val){
               
                $(".edit_category_id[value='" + val + "']").prop('checked', true);
             
            });

            $('.edit-employee-modal').modal('show');

        }
    });
}

function deleteEmployeeModal(id) {
    var $myModal = jQuery('#my-modal');
    var $dsBody = $myModal.find('div.modal-body');
    var $dsTitle = $myModal.find('div.modal-header');
    var $btConfirm = jQuery('#bt-modal-confirm');
    var $btCancel = jQuery('#bt-modal-cancel');

    $dsBody.html('<p>Are you sure you want to delete employee record ?</p>');
    $dsTitle.html('<p>Delete Employee Record</p>');
    $myModal.modal({
        show: true
    });

    $btConfirm.click(function() {
        deleteEmp(id);
    });

    $btCancel.click(function() {
        $dsTitle.html('Warning');
        $dsBody.html('<p>Notice</p>');
        $btConfirm.attr('href', '#').attr('data-dismiss', 'modal');
    });
}

function deleteEmp(id) {
    jQuery.ajax({
        type: 'POST',
        url: base_url + '/employee/delete',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            console.log(res);
            if (res.status == 'success') {
                $('#my-modal').modal('hide');
                $('.alert-success').show();
                $('.alert-success').html('Employee Record Deleted Successfully');
                setTimeout(function() {
                    location.reload();
                    $('.alert-success').fadeOut('fast');
                }, 2000);
            }
        }
    });
}
// end EMPLOYEE *****************


// ROLE *****************
function editRoleModal(id) {

    jQuery.ajax({
        type: 'POST',
        url: base_url + '/role/edit',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            var json = JSON.stringify(res['role']);
            var jsondata = JSON.parse(json);
            $(".alert-danger").hide();

            $('#role-edit')[0].reset();

            $('#edit_role_id').val(jsondata.id);
            $('#edit_role_name').val(jsondata.name);
            $('#edit_role_shortname').val(jsondata.display_name);
            $('#edit_role_desc').val(jsondata.description);


            var json_permission = JSON.stringify(res['permission']);
            var jsondataPermission = JSON.parse(json_permission);
            var permission_array = new Array();
            $.each(jsondataPermission, function(idx, obj) {
                permission_array.push(obj.id);

             });

            $.each(permission_array, function(i, val){
                $("input[value='" + val + "']").prop('checked', true);
            });

            $('.edit-role-modal').modal('show');

        }
    });
}

function deleteRoleModal(id) {
    var $myModal = jQuery('#my-modal');
    var $dsBody = $myModal.find('div.modal-body');
    var $dsTitle = $myModal.find('div.modal-header');
    var $btConfirm = jQuery('#bt-modal-confirm');
    var $btCancel = jQuery('#bt-modal-cancel');

    $dsBody.html('<h5><center>Are you sure you want to delete this role?</center></h5>');
    $dsTitle.html('<h4>Delete Role</h4>');
    $myModal.modal({
        show: true
    });

    $btConfirm.click(function() {
        deleteRole(id);
    });

    $btCancel.click(function() {
        $dsTitle.html('Warning');
        $dsBody.html('<p>Notice</p>');
        $btConfirm.attr('href', '#').attr('data-dismiss', 'modal');
    });
}

function deleteRole(id) {
    jQuery.ajax({
        type: 'POST',
        url: base_url + '/role/delete',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            if (res.status == 'success') {
                $('#my-modal').modal('hide');
                 $('.alert-success').show();
                 $('.alert-success').html('Role Deleted Successfully');
                setTimeout(function() {
                       $('.alert-success').fadeOut('fast');
                       location.reload();
                    }, 2000);
            }
        }
    });
}
// end ROLE *****************


// Category ********************
function editCategoryModal(id) {

    jQuery.ajax({
        type: 'POST',
        url: base_url + '/category/edit',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            var json = JSON.stringify(res);
            var jsondata = JSON.parse(json);

            $(".alert-danger").hide();

            $('#edit_category_id').val(jsondata.id);
            $('#edit_name').val(jsondata.name);
            $('#edit_description').val(jsondata.description);
            $('#edit_percentage').val(jsondata.fixed_percentage);

            $('.edit-category-modal').modal('show');
        }
    });
}

function deleteCategoryModal(id) {
    var $myModal = jQuery('#my-modal');
    var $dsBody = $myModal.find('div.modal-body');
    var $dsTitle = $myModal.find('div.modal-header');
    var $btConfirm = jQuery('#bt-modal-confirm');
    var $btCancel = jQuery('#bt-modal-cancel');

    $dsBody.html('<p>Are you sure you want to delete category?</p>');
    $dsTitle.html('<p>Delete Category</p>');
    $myModal.modal({
        show: true
    });

    $btConfirm.click(function() {
        deleteCategory(id);
    });

    $btCancel.click(function() {
        $dsTitle.html('Warning');
        $dsBody.html('<p>Notice</p>');
        $btConfirm.attr('href', '#').attr('data-dismiss', 'modal');
    });
}

function deleteCategory(id) {
    jQuery.ajax({
        type: 'POST',
        url: base_url + '/category/delete',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            if (res.status == 'success') {
                $('#my-modal').modal('hide');
                $('.alert-success').show();
                $('.alert-success').html('Category Deleted Successfully..!');
                setTimeout(function() {
                    $('.alert-success').fadeOut('fast');
                    location.reload();
                }, 2000);
            }
        }
    });
}
// end Category ****************


// FAQ ***************************
function editFaqModal(id) {

    jQuery.ajax({
        type: 'POST',
        url: base_url + '/faq/edit',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            var json = JSON.stringify(res);
            var jsondata = JSON.parse(json);

            $(".alert-danger").hide();

            $('#edit_faq_id').val(jsondata.id);
            $('#edit_question').val(jsondata.question);
            $('#edit_answer').val(jsondata.answer);

            $('.edit-faq-modal').modal('show');
        }
    });
}

function deleteFaqModal(id) {
    var $myModal = jQuery('#my-modal');
    var $dsBody = $myModal.find('div.modal-body');
    var $dsTitle = $myModal.find('div.modal-header');
    var $btConfirm = jQuery('#bt-modal-confirm');
    var $btCancel = jQuery('#bt-modal-cancel');

    $dsBody.html('<h5><center>Are you sure you want to delete this faq?</center></h5>');
    $dsTitle.html('<h4>Delete Faq</h4>');
    $myModal.modal({
        show: true
    });

    $btConfirm.click(function() {
        deleteFaq(id);
    });

    $btCancel.click(function() {
        $dsTitle.html('Warning');
        $dsBody.html('<p>Notice</p>');
        $btConfirm.attr('href', '#').attr('data-dismiss', 'modal');
    });
}

function deleteFaq(id) {
    //console.log(id);
    jQuery.ajax({
        type: 'POST',
        url: base_url + '/faq/delete',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
            //console.log(res);
            if (res.status == 'success') {
                $('#my-modal').modal('hide');
                $('.alert-success').show();
                $('.alert-success').html('Faq Deleted Successfully..!');
                
                setTimeout(function() {
                    $('.alert-success').fadeOut('fast');
                    location.reload();
             }, 2000);
            }
        }
    });
}
// end FAQ ***********************

//Product Edit

function validation_product(){
        var $form = $("#product-edit-price"),
            $successMsg = $(".alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
        });

        $form.validate({
            rules: {
                
                'selling_price[CTN]': {
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 2
                },
                'selling_price[OTR]':{
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 2
                },
                'selling_price[PCS]':{
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 2
                }
            },
            messages: {
                'selling_price[CTN]': {
                    required: "Please specify selling price",
                    number: "Numbers only allowed..!",
                    maxlength: "Maximum 10 digits allowed",
                    minlength: "Minimum 2 digits "
                },
                'selling_price[OTR]': {
                    required: "Please specify selling price",
                    number: "Numbers only allowed..!",
                    maxlength: "Maximum 10 digits allowed",
                    minlength: "Minimum 2 digits "
                },
                'selling_price[PCS]': {
                    required: "Please specify selling price",
                    number: "Numbers only allowed..!",
                    maxlength: "Maximum 10 digits allowed",
                    minlength: "Minimum 2 digits "
                }

            },

            submitHandler: function(form) {
                form.submit();
                return false;
            }
        });
    }

function editProductModal(id) {

    // var department = $("#department").val();
    // var category = $("#category").val();
    // var subcategory = $("#subcategory").val();
    // var brand = $("#brand").val();
    // var featured_status = $("#featured_status").val();

    //alert(department);

    jQuery.ajax({
        type: 'POST',
        url: base_url + '/product/edit',
        dataType: 'json',
        data: {
            '_token': CSRF_TOKEN,
            'id': id
        },
        success: function(res) {
           // console.log(res);
           
            $('.edit-product-modal').html(res.html);
            $('.edit-product-modal').modal('show');
            validation_product();

        }
    });
}


function saveProductPrice(id){

    //var chks = document.getElementById('sellingXyz[]');//here selling_price[] is the name of the textbox

    var chks = $(".sellingXyz");
    var flag = true;
    $.each(chks, function(){
        if($(this).val() == ''){
           alert("Please specify amount"); 
           $(this).focus();
           flag = false;
        }
        
    });
    if(flag){
       $("#msgDiv").hide();
        $("#loadingDiv").show();
        jQuery.ajax({
            type: 'POST',
            url: base_url + '/product/update',
            dataType: 'json',
            data: {
                '_token': CSRF_TOKEN,
                'formData' : $('#product-edit-prices').serializeArray()
            },
            success: function(res) {
                //console.log(res);
               
               if(res['status']=="success"){
                 $("#loadingDiv").hide();
                 $("#msgDiv").show();
                 
                 setTimeout(function() {
                   $('#msgDiv').fadeOut('fast');
                }, 3000);
                       }
                

            }
        });
    }
 
    

    
}
